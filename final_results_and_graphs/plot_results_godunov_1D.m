%{
Author:             Jeremy Wang, University of Waterloo
Last Update:        2022-07-26
Description:        Plots results generated from godunov 1D code
%}

clear; clc; close all;
cur_dir = pwd; % gets present working directory

% Visualization parameters
line_width = 1;
line_width_exact = 0.5;
% line_colour_stars = 'r';
line_colour_stars = [255/255 130/255 0/255];
% line_colour_harten = [51/255 150/255 255/255];
line_colour_harten = 'b';
line_colour_roe = [0.6 0.6 0.6];
line_colour_exact = [0 0 0];

%% Test case 1.a: Transcritical RCS shock tube (IG)
trans_rcs_IG_exact = readtable(sprintf("%s/1_transcritical_rcs/n=2048/trans_rcs_IG_exact_t=0.000900_n=2048.xlsx", cur_dir));
trans_rcs_IG_Roe = readtable(sprintf("%s/1_transcritical_rcs/n=256/trans_rcs_IG_Roe_t=0.000900_n=256.xlsx", cur_dir));
trans_rcs_IG_RoeHarten = readtable(sprintf("%s/1_transcritical_rcs/n=256/trans_rcs_IG_Roe-Harten_t=0.000900_n=256.xlsx", cur_dir));
trans_rcs_IG_RoeStARS = readtable(sprintf("%s/1_transcritical_rcs/n=256/trans_rcs_IG_Roe-StARS_t=0.000900_n=256.xlsx", cur_dir));

line_spec_stars = '^';
line_spec_harten = 's';
line_spec_roe = '+';
line_spec_exact = '-';
marker_size = 3;
xlim_min = -1;
xlim_max = 1;
figure(1), set(gcf, 'Position', [0 0 1500 1200]);
   
subplot(2,3,1);
hold on;
plot(trans_rcs_IG_exact.x, trans_rcs_IG_exact.p_r, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS.x, trans_rcs_IG_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_IG_RoeHarten.x, trans_rcs_IG_RoeHarten.p_r, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_Roe.x, trans_rcs_IG_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 

hold off;
ylabel('p_r', 'FontSize', 15);
xlim([xlim_min, xlim_max]);
legend('Exact', 'Roe-StARS','Roe-Harten','Roe');

subplot(2,3,2);
hold on;
plot(trans_rcs_IG_exact.x, trans_rcs_IG_exact.rho, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS.x, trans_rcs_IG_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_IG_RoeHarten.x, trans_rcs_IG_RoeHarten.rho, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_Roe.x, trans_rcs_IG_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('\rho [kg m^{-3}]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,3);
hold on;
plot(trans_rcs_IG_exact.x, trans_rcs_IG_exact.T_r, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS.x, trans_rcs_IG_RoeStARS.T_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_IG_RoeHarten.x, trans_rcs_IG_RoeHarten.T_r, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_Roe.x, trans_rcs_IG_Roe.T_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('T_r', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,4);
hold on;
plot(trans_rcs_IG_exact.x, trans_rcs_IG_exact.u, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS.x, trans_rcs_IG_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_IG_RoeHarten.x, trans_rcs_IG_RoeHarten.u, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_Roe.x, trans_rcs_IG_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('u [m s^{-1}]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,5);
hold on;
plot(trans_rcs_IG_exact.x, trans_rcs_IG_exact.Ma, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS.x, trans_rcs_IG_RoeStARS.Ma, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_IG_RoeHarten.x, trans_rcs_IG_RoeHarten.Ma, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_Roe.x, trans_rcs_IG_Roe.Ma, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('Ma', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,6);
hold on;
plot(trans_rcs_IG_exact.x, trans_rcs_IG_exact.e, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS.x, trans_rcs_IG_RoeStARS.e, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_IG_RoeHarten.x, trans_rcs_IG_RoeHarten.e, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_Roe.x, trans_rcs_IG_Roe.e, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('e [J kg^{-1}]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

%% Test case 1.b: transcritical RCS shock tube (PR)
trans_rcs_PR_exact = readtable(sprintf("%s/1_transcritical_rcs/n=2048/trans_rcs_PR_exact_t=0.000900_n=2048.xlsx", cur_dir));
trans_rcs_PR_Roe = readtable(sprintf("%s/1_transcritical_rcs/n=256/trans_rcs_PR_Roe_t=0.000900_n=256.xlsx", cur_dir));
trans_rcs_PR_RoeHarten = readtable(sprintf("%s/1_transcritical_rcs/n=256/trans_rcs_PR_Roe-Harten_t=0.000900_n=256.xlsx", cur_dir));
trans_rcs_PR_RoeStARS = readtable(sprintf("%s/1_transcritical_rcs/n=256/trans_rcs_PR_Roe-StARS_t=0.000900_n=256.xlsx", cur_dir));

line_spec_stars = '^';
line_spec_harten = 's';
line_spec_roe = '+';
line_spec_exact = '-';
marker_size = 3;
xlim_min = -1;
xlim_max = 1;
figure(2), set(gcf, 'Position', [0 0 1500 1200]);
   
subplot(2,3,1);
hold on;
plot(trans_rcs_PR_exact.x, trans_rcs_PR_exact.p_r, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS.x, trans_rcs_PR_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_PR_RoeHarten.x, trans_rcs_PR_RoeHarten.p_r, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_Roe.x, trans_rcs_PR_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('p_r', 'FontSize', 15);
xlim([xlim_min, xlim_max]);
legend('Exact', 'Roe-StARS','Roe-Harten','Roe');

subplot(2,3,2);
hold on;
plot(trans_rcs_PR_exact.x, trans_rcs_PR_exact.rho, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS.x, trans_rcs_PR_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_PR_RoeHarten.x, trans_rcs_PR_RoeHarten.rho, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_Roe.x, trans_rcs_PR_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('\rho [kg m^{-3}]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,3);
hold on;
plot(trans_rcs_PR_exact.x, trans_rcs_PR_exact.T_r, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS.x, trans_rcs_PR_RoeStARS.T_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_PR_RoeHarten.x, trans_rcs_PR_RoeHarten.T_r, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_Roe.x, trans_rcs_PR_Roe.T_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('T_r', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,4);
hold on;
plot(trans_rcs_PR_exact.x, trans_rcs_PR_exact.u, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS.x, trans_rcs_PR_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_PR_RoeHarten.x, trans_rcs_PR_RoeHarten.u, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_Roe.x, trans_rcs_PR_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('u [m s^{-1}]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,5);
hold on;
plot(trans_rcs_PR_exact.x, trans_rcs_PR_exact.Ma, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS.x, trans_rcs_PR_RoeStARS.Ma, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_PR_RoeHarten.x, trans_rcs_PR_RoeHarten.Ma, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_Roe.x, trans_rcs_PR_Roe.Ma, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('Ma', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,6);
hold on;
plot(trans_rcs_PR_exact.x, trans_rcs_PR_exact.e, line_spec_exact, 'Color', line_colour_exact, 'LineWidth', line_width_exact, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS.x, trans_rcs_PR_RoeStARS.e, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size+2); 
plot(trans_rcs_PR_RoeHarten.x, trans_rcs_PR_RoeHarten.e, line_spec_harten, 'Color', line_colour_harten, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_Roe.x, trans_rcs_PR_Roe.e, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size+1); 
hold off;
ylabel('e [J kg^{-1}]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

%% Test case 2: transcritical RCS shock tube with periodic BCs
trans_rcs_2_IG_Roe_1 = readtable(sprintf("%s/2_transcritical_rcs_periodic/n=256/trans_rcs_2_IG_Roe_t=0.000902_n=256.xlsx", cur_dir));
trans_rcs_2_IG_Roe_2 = readtable(sprintf("%s/2_transcritical_rcs_periodic/n=256/trans_rcs_2_IG_Roe_t=0.002002_n=256.xlsx", cur_dir));
trans_rcs_2_IG_Roe_3 = readtable(sprintf("%s/2_transcritical_rcs_periodic/n=256/trans_rcs_2_IG_Roe_t=0.004006_n=256.xlsx", cur_dir));
trans_rcs_2_IG_Roe_4 = readtable(sprintf("%s/2_transcritical_rcs_periodic/n=256/trans_rcs_2_IG_Roe_t=0.007503_n=256.xlsx", cur_dir));

trans_rcs_2_IG_RoeStARS_1 = readtable(sprintf("%s/2_transcritical_rcs_periodic/n=256/trans_rcs_2_IG_Roe-StARS_t=0.000905_n=256.xlsx", cur_dir));
trans_rcs_2_IG_RoeStARS_2 = readtable(sprintf("%s/2_transcritical_rcs_periodic/n=256/trans_rcs_2_IG_Roe-StARS_t=0.002004_n=256.xlsx", cur_dir));
trans_rcs_2_IG_RoeStARS_3 = readtable(sprintf("%s/2_transcritical_rcs_periodic/n=256/trans_rcs_2_IG_Roe-StARS_t=0.004002_n=256.xlsx", cur_dir));
trans_rcs_2_IG_RoeStARS_4 = readtable(sprintf("%s/2_transcritical_rcs_periodic/n=256/trans_rcs_2_IG_Roe-StARS_t=0.007507_n=256.xlsx", cur_dir));

line_spec_stars = '^';
line_spec_roe = '+';
marker_size = 2.5;
xlim_min = -1;
xlim_max = 1;
figure(3);
set(gcf, 'Position', [0 0 1500 1500]);
   
subplot(3,4,1);
hold on;
plot(trans_rcs_2_IG_RoeStARS_1.x, trans_rcs_2_IG_RoeStARS_1.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_1.x, trans_rcs_2_IG_Roe_1.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('\rho [kg m^{-3}]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);
subtitle('\Delta t = 0.9 ms');

subplot(3,4,2);
hold on;
plot(trans_rcs_2_IG_RoeStARS_2.x, trans_rcs_2_IG_RoeStARS_2.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_2.x, trans_rcs_2_IG_Roe_2.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlim([xlim_min, xlim_max]);
subtitle('\Delta t = 2.0 ms');

subplot(3,4,3);
hold on;
plot(trans_rcs_2_IG_RoeStARS_3.x, trans_rcs_2_IG_RoeStARS_3.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_3.x, trans_rcs_2_IG_Roe_3.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlim([xlim_min, xlim_max]);
subtitle('\Delta t = 4.0 ms');

subplot(3,4,4);
hold on;
plot(trans_rcs_2_IG_RoeStARS_4.x, trans_rcs_2_IG_RoeStARS_4.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_4.x, trans_rcs_2_IG_Roe_4.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlim([xlim_min, xlim_max]);
legend('Roe-StARS','Roe', 'Location', 'northwest');
subtitle('\Delta t = 7.5 ms');

subplot(3,4,5);
hold on;
plot(trans_rcs_2_IG_RoeStARS_1.x, trans_rcs_2_IG_RoeStARS_1.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_1.x, trans_rcs_2_IG_Roe_1.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('u [m s^{-1}]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(3,4,6);
hold on;
plot(trans_rcs_2_IG_RoeStARS_2.x, trans_rcs_2_IG_RoeStARS_2.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_2.x, trans_rcs_2_IG_Roe_2.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,7);
hold on;
plot(trans_rcs_2_IG_RoeStARS_3.x, trans_rcs_2_IG_RoeStARS_3.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_3.x, trans_rcs_2_IG_Roe_3.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,8);
hold on;
plot(trans_rcs_2_IG_RoeStARS_4.x, trans_rcs_2_IG_RoeStARS_4.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_4.x, trans_rcs_2_IG_Roe_4.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,9);
hold on;
plot(trans_rcs_2_IG_RoeStARS_1.x, trans_rcs_2_IG_RoeStARS_1.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_1.x, trans_rcs_2_IG_Roe_1.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('p_r', 'FontSize', 15);
xlim([xlim_min, xlim_max]);
xlabel('x [m]');

subplot(3,4,10);
hold on;
plot(trans_rcs_2_IG_RoeStARS_2.x, trans_rcs_2_IG_RoeStARS_2.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_2.x, trans_rcs_2_IG_Roe_2.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlim([xlim_min, xlim_max]);
xlabel('x [m]');

subplot(3,4,11);
hold on;
plot(trans_rcs_2_IG_RoeStARS_3.x, trans_rcs_2_IG_RoeStARS_3.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_3.x, trans_rcs_2_IG_Roe_3.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlim([xlim_min, xlim_max]);
xlabel('x [m]');

subplot(3,4,12);
hold on;
plot(trans_rcs_2_IG_RoeStARS_4.x, trans_rcs_2_IG_RoeStARS_4.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_2_IG_Roe_4.x, trans_rcs_2_IG_Roe_4.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlim([xlim_min, xlim_max]);
xlabel('x [m]');

%% Test case 3.a: Gradient flow (IG)
grad1dx_IG_Roe = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad1dx_IG_Roe_t=0.000500_n=128.xlsx", cur_dir));
grad1dx_IG_RoeStARS = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad1dx_IG_Roe-StARS_t=0.000500_n=128.xlsx", cur_dir));
grad3dx_IG_Roe = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad3dx_IG_Roe_t=0.000500_n=128.xlsx", cur_dir));
grad3dx_IG_RoeStARS = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad3dx_IG_Roe-StARS_t=0.000500_n=128.xlsx", cur_dir));
grad5dx_IG_Roe = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad5dx_IG_Roe_t=0.000500_n=128.xlsx", cur_dir));
grad5dx_IG_RoeStARS = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad5dx_IG_Roe-StARS_t=0.000500_n=128.xlsx", cur_dir));
grad7dx_IG_Roe = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad7dx_IG_Roe_t=0.000500_n=128.xlsx", cur_dir));
grad7dx_IG_RoeStARS = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad7dx_IG_Roe-StARS_t=0.000500_n=128.xlsx", cur_dir));

line_spec_stars = '^';
line_spec_roe = '+';
marker_size = 4;
xlim_min = -0.1;
xlim_max = 0.1;
figure(4), set(gcf, 'Position', [0 0 1500 1500]);
   
subplot(3,4,1);
subtitle('\Delta L = 7.8 mm');
hold on;
plot(grad1dx_IG_RoeStARS.x, grad1dx_IG_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad1dx_IG_Roe.x, grad1dx_IG_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('\rho [kg m^{-3}]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(3,4,2);
subtitle('\Delta L = 2.34 cm');
hold on;
plot(grad3dx_IG_RoeStARS.x, grad3dx_IG_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad3dx_IG_Roe.x, grad3dx_IG_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,3);
subtitle('\Delta L = 3.91 cm');
hold on;
plot(grad5dx_IG_RoeStARS.x, grad5dx_IG_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad5dx_IG_Roe.x, grad5dx_IG_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,4);
subtitle('\Delta L = 5.47 cm');
hold on;
plot(grad7dx_IG_RoeStARS.x, grad7dx_IG_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad7dx_IG_Roe.x, grad7dx_IG_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);
legend('Roe-StARS','Roe');

subplot(3,4,5);
hold on;
plot(grad1dx_IG_RoeStARS.x, grad1dx_IG_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad1dx_IG_Roe.x, grad1dx_IG_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('u [m s^{-1}]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(3,4,6);
hold on;
plot(grad3dx_IG_RoeStARS.x, grad3dx_IG_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad3dx_IG_Roe.x, grad3dx_IG_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,7);
hold on;
plot(grad5dx_IG_RoeStARS.x, grad5dx_IG_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad5dx_IG_Roe.x, grad5dx_IG_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,8);
hold on;
plot(grad7dx_IG_RoeStARS.x, grad7dx_IG_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad7dx_IG_Roe.x, grad7dx_IG_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,9);
hold on;
plot(grad1dx_IG_RoeStARS.x, grad1dx_IG_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad1dx_IG_Roe.x, grad1dx_IG_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('p_r', 'FontSize', 15);
xlim([xlim_min, xlim_max]);
xlabel('x [m]', 'FontSize', 15);

subplot(3,4,10);
hold on;
plot(grad3dx_IG_RoeStARS.x, grad3dx_IG_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad3dx_IG_Roe.x, grad3dx_IG_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);
xlabel('x [m]', 'FontSize', 15);

subplot(3,4,11);
hold on;
plot(grad5dx_IG_RoeStARS.x, grad5dx_IG_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad5dx_IG_Roe.x, grad5dx_IG_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);
xlabel('x [m]', 'FontSize', 15);

subplot(3,4,12);
hold on;
plot(grad7dx_IG_RoeStARS.x, grad7dx_IG_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad7dx_IG_Roe.x, grad7dx_IG_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);
xlabel('x [m]', 'FontSize', 15);

%% Test case 3.b: Gradient flow (PR)
grad1dx_PR_Roe = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad1dx_PR_Roe_t=0.000500_n=128.xlsx", cur_dir));
grad1dx_PR_RoeStARS = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad1dx_PR_Roe-StARS_t=0.000500_n=128.xlsx", cur_dir));
grad3dx_PR_Roe = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad3dx_PR_Roe_t=0.000500_n=128.xlsx", cur_dir));
grad3dx_PR_RoeStARS = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad3dx_PR_Roe-StARS_t=0.000500_n=128.xlsx", cur_dir));
grad5dx_PR_Roe = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad5dx_PR_Roe_t=0.000500_n=128.xlsx", cur_dir));
grad5dx_PR_RoeStARS = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad5dx_PR_Roe-StARS_t=0.000500_n=128.xlsx", cur_dir));
grad7dx_PR_Roe = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad7dx_PR_Roe_t=0.000500_n=128.xlsx", cur_dir));
grad7dx_PR_RoeStARS = readtable(sprintf("%s/3_gradient_flow/t=0.0005_n=128/grad7dx_PR_Roe-StARS_t=0.000500_n=128.xlsx", cur_dir));

line_spec_stars = '^';
line_spec_roe = '+';
marker_size = 4;
xlim_min = -0.1;
xlim_max = 0.1;
figure(4), set(gcf, 'Position', [0 0 1500 1500]);
   
subplot(3,4,1);
subtitle('\Delta L = 7.8 mm');
hold on;
plot(grad1dx_PR_RoeStARS.x, grad1dx_PR_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad1dx_PR_Roe.x, grad1dx_PR_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('\rho [kg m^{-3}]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(3,4,2);
subtitle('\Delta L = 2.34 cm');
hold on;
plot(grad3dx_PR_RoeStARS.x, grad3dx_PR_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad3dx_PR_Roe.x, grad3dx_PR_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,3);
subtitle('\Delta L = 3.91 cm');
hold on;
plot(grad5dx_PR_RoeStARS.x, grad5dx_PR_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad5dx_PR_Roe.x, grad5dx_PR_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,4);
subtitle('\Delta L = 5.47 cm');
hold on;
plot(grad7dx_PR_RoeStARS.x, grad7dx_PR_RoeStARS.rho, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad7dx_PR_Roe.x, grad7dx_PR_Roe.rho, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);
legend('Roe-StARS','Roe');

subplot(3,4,5);
hold on;
plot(grad1dx_PR_RoeStARS.x, grad1dx_PR_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad1dx_PR_Roe.x, grad1dx_PR_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('u [m s^{-1}]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(3,4,6);
hold on;
plot(grad3dx_PR_RoeStARS.x, grad3dx_PR_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad3dx_PR_Roe.x, grad3dx_PR_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,7);
hold on;
plot(grad5dx_PR_RoeStARS.x, grad5dx_PR_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad5dx_PR_Roe.x, grad5dx_PR_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,8);
hold on;
plot(grad7dx_PR_RoeStARS.x, grad7dx_PR_RoeStARS.u, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad7dx_PR_Roe.x, grad7dx_PR_Roe.u, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);

subplot(3,4,9);
hold on;
plot(grad1dx_PR_RoeStARS.x, grad1dx_PR_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad1dx_PR_Roe.x, grad1dx_PR_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('p_r', 'FontSize', 15);
xlim([xlim_min, xlim_max]);
xlabel('x [m]', 'FontSize', 15);

subplot(3,4,10);
hold on;
plot(grad3dx_PR_RoeStARS.x, grad3dx_PR_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad3dx_PR_Roe.x, grad3dx_PR_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);
xlabel('x [m]', 'FontSize', 15);

subplot(3,4,11);
hold on;
plot(grad5dx_PR_RoeStARS.x, grad5dx_PR_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad5dx_PR_Roe.x, grad5dx_PR_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);
xlabel('x [m]', 'FontSize', 15);

subplot(3,4,12);
hold on;
plot(grad7dx_PR_RoeStARS.x, grad7dx_PR_RoeStARS.p_r, line_spec_stars, 'Color', line_colour_stars, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(grad7dx_PR_Roe.x, grad7dx_PR_Roe.p_r, line_spec_roe, 'Color', line_colour_roe, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
xlim([xlim_min, xlim_max]);
xlabel('x [m]', 'FontSize', 15);

%% Test case 4: 2D Riemann Problem with PR EOS - Roe results
rp_2d_Roe_x = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "X");
rp_2d_Roe_y = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "Y");
rp_2d_Roe_p_r = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "p_r");
rp_2d_Roe_u = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "u");
rp_2d_Roe_w = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "w");
rp_2d_Roe_rho = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "rho");
rp_2d_Roe_e = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "e");
rp_2d_Roe_T_r = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "T_r");

do_show_contour_labels = 0;
xlim_min = -1;
xlim_max = 1;
ylim_min = -1;
ylim_max = 1;
num_level_steps = 30;
figure(6), set(gcf, 'Position', [0 0 1750 928]);

subplot(2,3,1);
contour(rp_2d_Roe_x, rp_2d_Roe_y, rp_2d_Roe_p_r, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_Roe_p_r, [], 'all') - min(rp_2d_Roe_p_r, [], 'all'))/num_level_steps);
ylabel('y [m]');
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'p_r';

subplot(2,3,2);
contour(rp_2d_Roe_x, rp_2d_Roe_y, rp_2d_Roe_rho, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_Roe_rho, [], 'all') - min(rp_2d_Roe_rho, [], 'all'))/num_level_steps); 
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = '\rho [kg/m^3]';

subplot(2,3,3);
contour(rp_2d_Roe_x, rp_2d_Roe_y, rp_2d_Roe_T_r, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_Roe_T_r, [], 'all') - min(rp_2d_Roe_T_r, [], 'all'))/num_level_steps); 
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'T_r';

subplot(2,3,4);
contour(rp_2d_Roe_x, rp_2d_Roe_y, rp_2d_Roe_u, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_Roe_u, [], 'all') - min(rp_2d_Roe_u, [], 'all'))/num_level_steps); 
xlabel('x [m]');
ylabel('y [m]');
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'u [m/s]';

subplot(2,3,5);
contour(rp_2d_Roe_x, rp_2d_Roe_y, rp_2d_Roe_w, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_Roe_w, [], 'all') - min(rp_2d_Roe_w, [], 'all'))/num_level_steps); 
xlabel('x [m]');
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'w [m/s]';

subplot(2,3,6);
contour(rp_2d_Roe_x, rp_2d_Roe_y, rp_2d_Roe_e, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_Roe_e, [], 'all') - min(rp_2d_Roe_e, [], 'all'))/num_level_steps); 
xlabel('x [m]');
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'e [J/kg]';

%% Test case 4: 2D Riemann Problem with PR EOS - Roe-StARS results
rp_2d_RoeStARS_x = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe-StARS_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "X");
rp_2d_RoeStARS_y = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe-StARS_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "Y");
rp_2d_RoeStARS_p_r = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe-StARS_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "p_r");
rp_2d_RoeStARS_u = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe-StARS_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "u");
rp_2d_RoeStARS_w = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe-StARS_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "w");
rp_2d_RoeStARS_rho = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe-StARS_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "rho");
rp_2d_RoeStARS_e = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe-StARS_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "e");
rp_2d_RoeStARS_T_r = xlsread(sprintf("%s/4_2D_Riemann_problem/J21_RCS32_RCS34_J14_PR_Roe-StARS_t=0.000600_ny=128_nx=128.xlsx", cur_dir), "T_r");

do_show_contour_labels = 0;
xlim_min = -1;
xlim_max = 1;
ylim_min = -1;
ylim_max = 1;
num_level_steps = 30;
figure(7), set(gcf, 'Position', [0 0 1750 928]);

subplot(2,3,1);
contour(rp_2d_RoeStARS_x, rp_2d_RoeStARS_y, rp_2d_RoeStARS_p_r, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_RoeStARS_p_r, [], 'all') - min(rp_2d_RoeStARS_p_r, [], 'all'))/num_level_steps);
ylabel('y [m]');
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'p_r';

subplot(2,3,2);
contour(rp_2d_RoeStARS_x, rp_2d_RoeStARS_y, rp_2d_RoeStARS_rho, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_RoeStARS_rho, [], 'all') - min(rp_2d_RoeStARS_rho, [], 'all'))/num_level_steps); 
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = '\rho [kg/m^3]';

subplot(2,3,3);
contour(rp_2d_RoeStARS_x, rp_2d_RoeStARS_y, rp_2d_RoeStARS_T_r, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_RoeStARS_T_r, [], 'all') - min(rp_2d_RoeStARS_T_r, [], 'all'))/num_level_steps); 
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'T_r';

subplot(2,3,4);
contour(rp_2d_RoeStARS_x, rp_2d_RoeStARS_y, rp_2d_RoeStARS_u, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_RoeStARS_u, [], 'all') - min(rp_2d_RoeStARS_u, [], 'all'))/num_level_steps); 
xlabel('x [m]');
ylabel('y [m]');
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'u [m/s]';

subplot(2,3,5);
contour(rp_2d_RoeStARS_x, rp_2d_RoeStARS_y, rp_2d_RoeStARS_w, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_RoeStARS_w, [], 'all') - min(rp_2d_RoeStARS_w, [], 'all'))/num_level_steps); 
xlabel('x [m]');
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'w [m/s]';

subplot(2,3,6);
contour(rp_2d_RoeStARS_x, rp_2d_RoeStARS_y, rp_2d_RoeStARS_e, ...
    'ShowText', do_show_contour_labels, ...
    'LevelStepMode', 'manual', ...
    'LevelStep', (max(rp_2d_RoeStARS_e, [], 'all') - min(rp_2d_RoeStARS_e, [], 'all'))/num_level_steps); 
xlabel('x [m]');
xlim([xlim_min, xlim_max]);
ylim([ylim_min, ylim_max]);
c = colorbar();
% c.Label.String = 'e [J/kg]';