%% HEADER
%% USAGE: 	Set variables in INPUTS and provide the required files, then run and save the desired outputs.

% Author:       Jeremy Wang, University of Waterloo
% Last Update:  2020-11-18
% Description: 
%   @V1
%       - Performs a scaling analysis to determine when transonic
%       rarefactions are present and the error if they are ignored


clear; clc; close all;
fprintf("StARS scaling analysis initiated...\n");

%% NOMENCLATURE, UNIVERSAL CONSTANTS, AND UNITS
% Physical quantities generally follow the format:
% <quantity>_<location>_<calculationmethod>
% e.g. p_1_IG, delta_h_1_miller

% Variables:
% a = constant in the eos [Pa (m^3 kmol^-1)^2 K^0.5]
% b = constant in the eos [m^3 kmol^-1]
% c = speed of sound [m s^-1]
% c_p = specific heat capacity at constant pressure [J kmol^-1 K^-1]
% c_v = specific heat capacity at constant volume [J kmol^-1 K^-1]
% h = enthalpy [J kmol^-1]
% M = molar mass [kg kmol^-1]
% p = pressure [Pa]
% rho = density [kg m^-3]
% T = temperature [K]
% u = speed [m s^-1]
% v = molar volume [m^3 kmol^-1]
% x = position [m]

% Subscripts:
% 'miller' = calculated from Miller et al. (2001) paper on DNS
% 'IG' = calculated using ideal gas law
% 'TP' = calculated using a thermally perfect gas

% Universal constants:
R = 8314.4621; % Universal gas constant [J kmol^-1 K^-1]

%% INPUTS
fprintf("Populating input variables...\n");

filename_properties = 'gas_properties.xlsx'; % file with table of gas properties
gas_name = 'N2gas'; % name of the gas (see filename_properties)
eos = 3; % 0 = IG, 1 = RK, 2 = SRK, 3 = PR
dpdx = -70e6; % pressure gradient
dudx = 300; % flow speed gradient
dx = 4e-3; % cell width

u = 300; % flow speeds for analysis
p_r = 0.5:0.01:2.5; % reduced pressures for analysis
T_r = 0.5:0.01:2.5; % reduced temperatures for analysis

iterations_fsolve_max = 1000;

% Retrieve the gas properties:
properties = table2struct(readtable(filename_properties));
num_gas = find(contains({properties.gas_name}, gas_name)); % retrieve index to the gas properties table
gamma(eos) = properties(num_gas).gamma;      % specific heat ratio
M(eos) = properties(num_gas).M;              % molar mass
T_c(eos) = properties(num_gas).T_c;          % critical temperature
p_c(eos) = properties(num_gas).p_c;          % critical pressure
omega(eos) = properties(num_gas).omega;      % acentric factor
h_ref(eos) = properties(num_gas).h_ref;      % reference enthalpy 
% NOTE: h_ref is never explicitly computed, it only appears during
    % integration and cancels out in the equtaions that need to be solved. 
    % Thus, h_ref is included only for completeness but it does NOT affect 
    % solutions, so a trivial value of zero is used.  
A = str2num(properties(num_gas).c_IG_p_fit_params);
    
%% DEFINITIONS & SYMBOLIC FUNCTIONS
fprintf("Now executing basic definitions and symbolic functions...\n");

% Set / reset the symbolic variables
syms p v T

% Gas constants
R_specific = R / M(eos); % [J kg^-1 K^-1]

% Setting eos parameters based on input selection
switch eos
    case 0 % Ideal Gas Law
        eos_tag = "Ideal and thermally perfect gas";
        a = 0;
        b = 0;
        Theta = 0;
        delta = 0;
        epsilon = 0;
        p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
    case 1 % Redlich Kwong
        eos_tag = "RK with departure functions relative to thermally perfect gas";
        a = 0.4278 * R^2 * T_c(eos)^(2.5) / p_c(eos);
        b = 0.0867 * R * T_c(eos) / p_c(eos);
        Theta = a * (1/T)^0.5;
        delta = b;
        epsilon = 0;
        p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
    case 2 % Soave-Redlich-Kwong
        eos_tag = "SRK with departure functions relative to thermally perfect gas";
        a = 0.42747 * R^2 * T_c(eos)^2 / p_c(eos);
        b = 0.08664 * R * T_c(eos) / p_c(eos);
        Theta = a*(1 + (0.48 + 1.574*omega(eos) - 0.176*omega(eos)^2)*(1-(T/T_c(eos))^0.5))^2;
        delta = b;
        epsilon = 0;
        p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
    case 3 % Peng-Robinson
        eos_tag = "PR with departure functions relative to thermally perfect gas";
        a = 0.45724 * R^2 * T_c(eos)^2 / p_c(eos);
        b = 0.07780 * R * T_c(eos) / p_c(eos);
        Theta = a*(1 + (0.37464 + 1.54226*omega(eos) - 0.2699*omega(eos)^2)*(1-(T/T_c(eos))^0.5))^2;
        delta = 2*b;
        epsilon = -b^2;
        p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
end
% NOTE: since virial eos diverge near criticality and BWR is close to
% virial, we do not compute here.
fprintf("\teos selection: %s...\n", eos_tag);

% Derivatives
dThetadT = diff(Theta, T);
d2ThetadT2 = diff(Theta, T, 2);
exp_dpdT = diff(p, T);
exp_d2pdT2 = diff(p, T, 2);
exp_dpdv = diff(p, v);

% Derivatives manually written out
% exp_dpdT = R/(v-b)-(dThetadT)/(v^2+delta*v+epsilon);
% exp_d2pdT2 = -(d2ThetadT2)/(v^2+delta*v+epsilon);
% exp_dpdv = R*T/(v-b)^2+Theta*(2*v+delta)/(v^2+delta*v+epsilon)^2;

% Integrals
exp_intdpdT = real(int(exp_dpdT, v)); % [J kmol^-1 K^-1]
exp_intvdpdv = real(int(v*exp_dpdv, v)); % [J kmol^-1]
exp_intd2pdT2 = real(int(exp_d2pdT2, v)); % [J kmol^-1 K^-2]

% Integrals manually written out
% exp_intdpdT = R*log(v-b) + dThetadT*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon));
% exp_intvdpdv = -Theta*v/(v^2+delta*v+epsilon) - Theta*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon)) ...
%     - R*T*log(v-b) + R*T*b/(v-b);
% exp_intd2pdT2 = d2ThetadT2*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon));

% Specific heat capacities and enthalpies
T_mat = T; % reset the length of T_mat (this default value is overridden)
for i = 1:length(A) 
    T_mat(i,1) = T^(i-1); 
end

c_p_IG_TP = R*(A*T_mat); % verified
c_v_IG_TP = c_p_IG_TP - R; % verified
c_v = c_v_IG_TP + T*exp_intd2pdT2; % verified
c_p = c_v - T*exp_dpdT^2/exp_dpdv; % verified
delta_h_IG_TP = real((Theta-T*dThetadT)*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon)) + R*T - p*v);
% NOTE: delta_h_IG was taken directly from Maple's computation of
% {-T*exp_intdpdT - exp_intvdpdv}. For some reason, using the following diverges:
% delta_h_IG_TP = -T*exp_intdpdT - exp_intvdpdv;

if eos == 0 % if ideal gas eos selected, delta_h_IG evaluates to a NaN when it should be zero.
    delta_h_IG_TP = 0;
end

h_IG_TP = int(c_p_IG_TP, T) - h_ref(eos);
h = h_IG_TP - delta_h_IG_TP;

% Miller & Bellan (2001) results for Peng-Robinson:
K_1 = (1/(2*sqrt(2)*b))*log( (v+(1-sqrt(2))*b)/(v+(1+sqrt(2))*b) );
delta_h_miller = - p*v + R*T - K_1*(Theta - T*dThetadT);
h_miller = h_IG_TP - delta_h_miller;

% Speed of sound
c = sqrt(-v^2*c_p*exp_dpdv/(M(eos)*c_v));

%% COMPUTING ERRORS FROM OMITTIN THE RAREFACTION, FOR HLLC
fprintf('Computing errors from omitting the rarefaction (for HLLC)...\n');
f = waitbar(0, 'Processing the domain of interest...');
counter = 0;
total_count = length(p_r)*length(T_r)*length(u);

for i = 1:length(p_r)
    for j = 1:length(T_r)
        for k = 1:length(u)
            syms v T
            % Compute p, T
            p_out(i,j,k) = p_r(i)*p_c(eos);
            T_out(i,j,k) = T_r(j)*T_c(eos);
            
            % Compute v, rho by solving the p = p(v,T) cubic
            v_out(i,j,k) = getvfromTP(T_out(i,j,k), p_out(i,j,k), R, double(subs(Theta, T, T_out(i,j,k))), delta, epsilon, b);
            rho_out(i,j,k) = M(eos)/v_out(i,j,k);
            
            % Compute c
            v = v_out(i,j,k); T = T_out(i,j,k);
            c_out(i,j,k) = double(subs(c));
            
            % Store u
            u_out(i,j,k) = u(k);
            
            % Compute Ma_star
            Ma(i,j,k) = abs(u_out(i,j,k) / c_out(i,j,k));
            Ma_star(i,j,k) = abs(u_out(i,j,k) / c_out(i,j,k) - dx/(2*rho_out(i,j,k)*c_out(i,j,k)^2) * dpdx);
            
            % Determine if a transonic rarefaction would occur, and if so, the corresponding error
            if (Ma(i,j,k) < 1) && (Ma_star(i,j,k) > 1)
                isRarefaction(i,j,k) = 1;
                dvv(i,j,k) = abs(sqrt(1 - dx/(2*rho_out(i,j,k)*c_out(i,j,k)^2)*dpdx) - (1 - dx/(2*rho_out(i,j,k)*c_out(i,j,k)^2)*dpdx));
                % dpp(i,j,k) = abs(sqrt(1 + dx/p_out(i,j,k)*dpdx) - (1 + dx/p_out(i,j,k)*dpdx));
                dpp(i,j,k) = abs(sqrt(1 - dx*rho_out(i,j,k)*c_out(i,j,k)/p_out(i,j,k)*dudx) - (1 - dx*rho_out(i,j,k)*c_out(i,j,k)/p_out(i,j,k)*dudx));
                % dpp(i,j,k) = abs(sqrt(1 + dx^2/(2*p_out(i,j,k)*(u_out(i,j,k) - c_out(i,j,k)))*dudx*dpdx) - (1 + dx^2/(2*p_out(i,j,k)*(u_out(i,j,k) - c_out(i,j,k)))*dudx*dpdx));
                duu(i,j,k) = abs(dx/(4*rho_out(i,j,k)*c_out(i,j,k)*u_out(i,j,k))*dpdx);
            else
                isRarefaction(i,j,k) = 0;
                dvv(i,j,k) = 0;
                dpp(i,j,k) = 0;
                duu(i,j,k) = 0;
            end
            counter = counter + 1;
            
            pct_complete = counter/total_count;
            f = waitbar(pct_complete, f, sprintf('Processing the domain of interest... %2.3f %%', pct_complete*100));
        end
    end
end
close(f)

%% PLOTTING THE RESULTS
fprintf('Now plotting results...\n');

% Plot Ma_star == 1
figure; hold on;
[X,Y] = meshgrid(T_r, p_r); % since i <> Y, j <> X, k <> Z
for n = 1:length(u)
    contour(X,Y,Ma_star(:,:,n),[1,1], 'b');
end
for n = 1:length(u)
    contour(X,Y,Ma(:,:,n),[1,1], 'r--');
end
xlabel('T_r');
ylabel('p_r');
legend('Ma = 1','Ma_* = 1');

% Plot % error in v
figure; hold on;
set(gcf,'position',[0,0,1500,400])

subplot(1,3,1);
contourf(X,Y,dvv(:,:,n),[10]);
% pcolor(X,Y,dvv(:,:,n));
shading interp
caxis([0 max(dvv, [], 'all')]);
colorbar;
title('|\Deltav/v|');
xlabel('T_r');
ylabel('p_r');

% Plot % error in p
subplot(1,3,2);
contourf(X,Y,dpp(:,:,n),[10]);
% pcolor(X,Y,dpp(:,:,n));
shading interp
caxis([0 max(dpp, [], 'all')]);
colorbar;
title('|\Deltap/p|');
xlabel('T_r');
ylabel('p_r');

% Plot % error in u
subplot(1,3,3);
contourf(X,Y,duu(:,:,n),[10]);
% pcolor(X,Y,duu(:,:,n));
shading interp
caxis([0 max(duu, [], 'all')]);
colorbar;
title('|\Deltau/u|');
xlabel('T_r');
ylabel('p_r');

% Plot c
figure; hold on;
% contour(X,Y,c_out(:,:,n),[10]);
pcolor(X,Y,c_out(:,:,n));
shading interp
colorbar;
title('c');
xlabel('T_r');
ylabel('p_r');
