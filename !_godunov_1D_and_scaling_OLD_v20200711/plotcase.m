clear; clc; close all;

%% Figure 1
figure; 
set(gcf,'position',[0,0,1500,500])

subcrit_file = 'CASE_subcritical_RCS2.xlsx';
resultsHLLC = readtable(subcrit_file, 'Sheet', 'HLLC');
startHLLC = 1; endHLLC = 101;
resultsSTARS = readtable(subcrit_file, 'Sheet', 'StARS');
startSTARS = 1; endSTARS = 101;
resultsEXACT = readtable(subcrit_file, 'Sheet', 'Exact');
startEXACT = 1; endEXACT = 400;

% Subcrit
subplot(2,3,1); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,2)), '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,2)), 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,2)), 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('u [m s^{-1}]');

subplot(2,3,2); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,3))./1e6, '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,3))./1e6, 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,3))./1e6, 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('p [MPa]');
ylim([0 0.8]);

subplot(2,3,3); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,4)), '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,4)), 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,4)), 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('\rho [kg m^{-3}');
legend('Exact','HLLC','HLLC-StARS');

% Transcrit n = 512
transcrit_file = 'CASE2A-mod_n512.xlsx';
resultsHLLC = readtable(transcrit_file, 'Sheet', 'HLLC');
startHLLC = 106; endHLLC = 232;
resultsSTARS = readtable(transcrit_file, 'Sheet', 'StARS');
startSTARS = 106; endSTARS = 232;
resultsEXACT = readtable(transcrit_file, 'Sheet', 'Exact');
startEXACT = 85; endEXACT = 290;

subplot(2,3,4); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,2)), '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,3)), 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,3)), 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('u [m s^{-1}]');
ylim([0 350]);

subplot(2,3,5); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,3))./1e6, '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,4))./1e6, 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,4))./1e6, 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('p [MPa]');
ylim([0 11]);

% y = table2array(resultsHLLC(startHLLC:endHLLC,2));
y = movmean(table2array(resultsHLLC(startHLLC:endHLLC,2)), 10);
y = movmean(y, 10);
y = movmean(y, 5);
y = movmean(y, 3);

subplot(2,3,6); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,4)), '-', 'Color', [150 150 150]./255);
% plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,2)), 'b-', 'MarkerSize', 2);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),y, 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,2)), 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('\rho [kg m^{-3}');

%% Figure 2
figure; 
set(gcf,'position',[0,0,1500,500])

% Transcrit n = 256
transcrit_file = 'CASE2A-mod_n256.xlsx';
resultsHLLC = readtable(transcrit_file, 'Sheet', 'HLLC');
startHLLC = 1; endHLLC = 67;
resultsSTARS = readtable(transcrit_file, 'Sheet', 'StARS');
startSTARS = 1; endSTARS = 67;
resultsEXACT = readtable(transcrit_file, 'Sheet', 'Exact');
startEXACT = 85; endEXACT = 290;

subplot(2,3,1); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,2)), '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,3)), 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,3)), 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('u [m s^{-1}]');
xlim([-0.2 0.3]);
ylim([0 350]);

subplot(2,3,2); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,3))./1e6, '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,4))./1e6, 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,4))./1e6, 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('p [MPa]');
xlim([-0.2 0.3]);
ylim([0 11]);

y = table2array(resultsHLLC(startHLLC:endHLLC,2));
y = movmean(table2array(resultsHLLC(startHLLC:endHLLC,2)), 10);
% y = movmean(y, 10);
% y = movmean(y, 5);
% y = movmean(y, 3);

subplot(2,3,3); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,4)), '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),y, 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,2)), 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('\rho [kg m^{-3}');
xlim([-0.2 0.3]);

% Transcrit n = 128
transcrit_file = 'CASE2A-mod_n128.xlsx';
resultsHLLC = readtable(transcrit_file, 'Sheet', 'HLLC');
startHLLC = 1; endHLLC = 32;
resultsSTARS = readtable(transcrit_file, 'Sheet', 'StARS');
startSTARS = 1; endSTARS = 32;
resultsEXACT = readtable(transcrit_file, 'Sheet', 'Exact');
startEXACT = 85; endEXACT = 290;

subplot(2,3,4); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,2)), '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,3)), 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,3)), 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('u [m s^{-1}]');
xlim([-0.2 0.3]);
ylim([0 350]);

subplot(2,3,5); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,3))./1e6, '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),table2array(resultsHLLC(startHLLC:endHLLC,4))./1e6, 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,4))./1e6, 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('p [MPa]');
xlim([-0.2 0.3]);
ylim([0 11]);

% y = table2array(resultsHLLC(startHLLC:endHLLC,2));
y = movmean(table2array(resultsHLLC(startHLLC:endHLLC,2)), 10);
y = movmean(y, 10);
y = movmean(y, 5);
y = movmean(y, 3);

subplot(2,3,6); hold on;
plot(table2array(resultsEXACT(startEXACT:endEXACT,1)),table2array(resultsEXACT(startEXACT:endEXACT,4)), '-', 'Color', [150 150 150]./255);
plot(table2array(resultsHLLC(startHLLC:endHLLC,1)),y, 'b-', 'MarkerSize', 2);
plot(table2array(resultsSTARS(startSTARS:endSTARS,1)),table2array(resultsSTARS(startSTARS:endSTARS,2)), 'r-', 'MarkerSize', 2);
xlabel('x [m]');
ylabel('\rho [kg m^{-3}');
xlim([-0.2 0.3]);