function [gammaS, e0s] = getGammaSandE0S(V, eos)
%% IPR: possibly change input parameters based on called functions
% getGammaSandE0S Compute effective specific heat ratio and effective
% reference energy for the double-flux model.

rho = V(1,:);
p = V(3,:);
T = getTfromPandRho(p, rho, eos);
e = getEnergyfromTandRho(T, rho, eos);

sos = getSos(V, eos);

gammaS = sos.^2.*rho./p; % Eq. (12)
e0s = e - p./rho./(gammaS-1); % Eq. (16)

end

