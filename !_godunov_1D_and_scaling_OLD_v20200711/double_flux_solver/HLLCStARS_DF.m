function [f_HLLC] = SCRS_DF(V_L, V_R, gammaS, e0S)
% SCRS_DF Compute SCRS flux with double-flux model.
global dt

sign = @(s) (s>=0) - (s<=0);

% Left
rho_L = V_L(1,:);
u_L = V_L(2,:);
p_L = V_L(3,:);
a_L = sqrt(gammaS.*p_L./rho_L);
E_L = p_L./(gammaS-1) + rho_L.*e0S + 0.5*rho_L.*u_L.^2; % Eq. (21)

% Right
rho_R = V_R(1,:);
u_R = V_R(2,:);
p_R = V_R(3,:);
a_R = sqrt(gammaS.*p_R./rho_R);
E_R = p_R./(gammaS-1) + rho_R.*e0S + 0.5*rho_R.*u_R.^2; % Eq. (21)

s_L = u_L - a_L;
s_R = u_R + a_R;

W_L = [rho_L; rho_L.*u_L; E_L];
W_R = [rho_R; rho_R.*u_R; E_R];

f_L = [rho_L.*u_L; rho_L.*(u_L).^2+p_L; u_L.*(E_L+p_L)];
f_R = [rho_R.*u_R; rho_R.*(u_R).^2+p_R; u_R.*(E_R+p_R)];

sM = (p_L - p_R - rho_L.*u_L.*(s_L-u_L) + rho_R.*u_R.*(s_R-u_R))./(rho_R.*(s_R-u_R)-rho_L.*(s_L-u_L));
pM = rho_R.*(u_R-s_R).*(u_R-sM) + p_R;

WML = [(s_L-u_L)./(s_L-sM).*rho_L;
       ((s_L-u_L).*rho_L.*u_L + (pM-p_L))./(s_L-sM);
       ((s_L-u_L).*E_L - p_L.*u_L + pM.*sM)./(s_L-sM)];

WMR = [(s_R-u_R)./(s_R-sM).*rho_R;
       ((s_R-u_R).*rho_R.*u_R + (pM-p_R))./(s_R-sM);
       ((s_R-u_R).*E_R - p_R.*u_R + pM.*sM)./(s_R-sM)];

s_minus = min(0,s_L);
s_plus = max(0,s_R);

f_HLLC = bsxfun(@times,(1+sign(sM))/2,(f_L + bsxfun(@times,s_minus,(WML - W_L)))) + bsxfun(@times,(1-sign(sM))/2,(f_R + bsxfun(@times,s_plus,(WMR - W_R))));

for i = 1:length(f_HLLC)
    a_sL(i) = sqrt(gammaS(i)*pM(i)/(rho_L(i)*(s_L(i) - u_L(i))/(s_L(i) - sM(i)))); % Eq 10.39 from Toro (2009)
    a_sR(i) = sqrt(gammaS(i)*pM(i)/(rho_R(i)*(s_R(i) - u_R(i))/(s_R(i) - sM(i)))); % Eq 10.39 from Toro (2009)
    if u_L(i) - a_L(i) < 0 && sM(i) - a_sL(i) > 0 
        v_L = 28/rho_L(i);
        v_star_IG_L = 28/(rho_L(i) + (pM(i) - p_L(i))/a_L(i)^2);
        
        A_2L = -0.5*log(v_L*v_star_IG_L) + 0.5*log(v_L/v_star_IG_L)*(u_L(i) - a_L(i) + sM(i) - a_sL(i))/(u_L(i) - a_L(i) - sM(i) + a_sL(i));
        A_4L = -0.5*log(p_L(i)*pM(i)) + 0.5*log(p_L(i)/pM(i))*(u_L(i) - a_L(i) + sM(i) - a_sL(i))/(u_L(i) - a_L(i) - sM(i) + a_sL(i));
        A_5L = 0.5*(u_L(i) + sM(i)) - 0.5*(u_L(i) - a_L(i) + sM(i) - a_sL(i))*(u_L(i) - sM(i))/(u_L(i) - a_L(i) - sM(i) + a_sL(i));
        
        u = A_5L;
        p = exp(-A_4L);
        rho = 28/exp(-A_2L);
        
%         rho = rho_L(i)*(2/(gammaS(i) + 1) + (gammaS(i) - 1)/((gammaS(i)+1)*a_L(i))*u_L(i))^(2/(gammaS(i)-1));
%         u = 2/(gammaS(i) + 1) * (a_L(i) + (gammaS(i) - 1)/2*u_L(i));
%         p = p_L(i)*(2/(gammaS(i) + 1) + (gammaS(i) - 1)/((gammaS(i)+1)*a_L(i))*u_L(i))^(2*gammaS(i)/(gammaS(i)-1));
%         
        f_HLLC(1,i) = rho*u;
        f_HLLC(2,i) = rho*u^2 + p;
        f_HLLC(3,i) = u*(p./(gammaS(i)-1) + rho*e0S(i) + 0.5*rho*u^2 + p);
    elseif u_R(i) + a_R(i) > 0 && sM(i) + a_sR(i) < 0
        v_R = 28/rho_R(i);
        v_star_IG_R = 28/(rho_R(i) + (pM(i) - p_R(i))/a_R(i)^2);
        
        A_2R = -0.5*log(v_R*v_star_IG_R) + 0.5*log(v_R/v_star_IG_R)*(u_R(i) + a_R(i) + sM(i) + a_sR(i))/(u_R(i) + a_R(i) - sM(i) - a_sR(i));
        A_4R = -0.5*log(p_R(i)*pM(i)) + 0.5*log(p_R(i)/pM(i))*(u_R(i) + a_R(i) + sM(i) + a_sR(i))/(u_R(i) + a_R(i) - sM(i) - a_sR(i));
        A_5R = 0.5*(u_R(i) + sM(i)) - 0.5*(u_R(i) + a_R(i) + sM(i) + a_sR(i))*(u_R(i) - sM(i))/(u_R(i) + a_R(i) - sM(i) - a_sR(i));
        
        u = A_5R;
        p = exp(-A_4R);
        rho = 28/exp(-A_2R);
        
%         rho = rho_R(i)*(2/(gammaS(i) + 1) - (gammaS(i) - 1)/((gammaS(i)+1)*a_R(i))*u_R(i))^(2/(gammaS(i)-1));
%         u = 2/(gammaS(i) + 1) * (-a_R(i) + (gammaS(i) - 1)/2*u_R(i));
%         p = p_R(i)*(2/(gammaS(i) + 1) - (gammaS(i) - 1)/((gammaS(i)+1)*a_R(i))*u_R(i))^(2*gammaS(i)/(gammaS(i)-1));
%         
        f_HLLC(1,i) = rho*u;
        f_HLLC(2,i) = rho*u^2 + p;
        f_HLLC(3,i) = u*(p./(gammaS(i)-1) + rho*e0S(i) + 0.5*rho*u^2 + p);
    end
end

