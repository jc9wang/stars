% Jeremy Wang, University of Waterloo

% Solves for molar specific volume using the generic form of the cubic equation of state, as
% given in Eq. (1) of Wang & Hickey (2020): Analytical solutions to shock
% and expansion waves for non-ideal equations of state, Phys. Fluids, 
% Vol. 32, 086105

function [v] = getvfromTP(T, p, R, Theta, delta, epsilon, b)
    a0 = -b*epsilon*p - b*Theta - epsilon*R*T;
    a1 = -b*delta*p + epsilon*p - delta*R*T + Theta;
    a2 = -b*p + delta*p - R*T;
    a3 = p;
    
    % Uncomment to check: r = roots([a3 a3 a1 a0]);
    
    % only one real root
    l = a2./a3;
    m = a1./a3;
    n = a0./a3;
    A = 1/3*(3*m-l.^2);
    B = 1/27*(2*l.^3 - 9*l.*m + 27*n);
    D = A.^3/27 + B.^2/4;
    M = sign(-B/2+sqrt(D)).*(abs(-B/2+sqrt(D))).^(1/3);
    N = sign(-B/2-sqrt(D)).*(abs(-B/2-sqrt(D))).^(1/3);
    v = M + N - l/3;
end