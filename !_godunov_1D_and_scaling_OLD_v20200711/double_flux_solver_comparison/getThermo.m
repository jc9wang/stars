function [a, b, R, dadT, d2adT2] = getThermo(T, eos)
%% IPR: 13 uses PR acentric factor (alpha), 7-11 is N2 crit & characteristics, 15-20 seems PR-specific calc
% getThermo Compute necessary thermodyamic parameters for the cubic
% equation of state (N2 property is assumed).

% N2
MW = 28.0134e-3;
Tc = 126.19;
pc  = 3.3958e+6;
rhoc = 313.3;
omega  = 0.03720;

if eos == 0 %% PR
    c = 0.37464 + 1.54226*omega - 0.2699*omega^2;
    R = 8.314462; % JEREMY CHANGE
    a = 0.45724*(R*Tc)^2/pc*(1+c*(1-sqrt(T/Tc))).^2;
    b = 0.07780*R*Tc/pc;
    G = c*sqrt(T/Tc)./(1+c*(1-sqrt(T/Tc)));
    dadT = -1./T.*a.*G; 
    d2adT2 = 0.457236*R^2./T/2*c*(1+c)*Tc/pc.*sqrt(Tc./T); 
    R = 8.314462/MW; % JEREMY CHANGE
    
elseif eos == 1 %% RK
    R = 8.314462; % JEREMY CHANGE
    a = ((0.4278*(R^2)*(Tc^2.5))./pc)./(T.^0.5);
    b = (0.0867*R*Tc)/pc;
    dadT = -(a/2).*(1./(T.^1.5)); 
    d2adT2 = ((3.*a)./4).*(1./(T.^2.5)); 
    R = 8.314462/MW; % JEREMY CHANGE
    
elseif eos == 2 %% SRK
    c = 0.48 + 1.574*omega - 0.176*omega^2; 
    R = 8.314462; % JEREMY CHANGE
    Tr = sqrt(T/Tc);
    a = ((0.42747*(R*Tc)^2)/pc)*(1+c*(1-Tr)).^2;
    b = (0.08664*R*Tc)/pc;
    dadT = ((-c*((0.42747*(R*Tc)^2)/pc))./(Tc.^0.5))*((1-c*(Tr-1))./(T.^0.5)); 
    d2adT2 = (c*(c+1)*((0.42747*(R*Tc)^2)/pc))./(2*(c^0.5)*(T.^1.5)); 
    R = 8.314462/MW; % JEREMY CHANGE
    
end

