%% HEADER
%% USAGE: 	Set variables in INPUTS, then run and save the desired outputs.
%% 			Note: currently set up to handle rarefaction-contact-shock only.
%%			Note: this code is modified from a reference implementation of double-flux by Ma et al. (2017)

%% Init

clear all, close all, clc;

tic % start timer

global Nx dx x dt eos solver

%% INPUTS
   
% CFL number & EOS choice
CFL = 0.1;
eos = 0; % 0 = PR, 1 = RK, 2 = SRK
solver = 1; % 0 = HLLC, 1 = StARS

% Grids (periodic)
a = -1;
b = 1;
Nx = 256;
dx = (b-a)/Nx;
x = a+dx/2:dx:b-dx/2; % cell center location

% Plotting parameters
show_start = 1;
show_end = Nx;

% Initial conditions (shock tube)
eta = 1e-10;
rhoL = 290; TL = 126;
rhoR = 7.4; TR = 100;
u0 = zeros(1,Nx);

rho0 = ones(1,Nx); rho0(1,1:Nx/2) = rhoL; rho0(1,Nx/2+1:Nx) = rhoR;
T0 = ones(1,Nx); T0(1,1:Nx/2) = TL; T0(1,Nx/2+1:Nx) = TR;
p0 = ones(1,Nx); p0(1,1:Nx/2) = getPfromTandRho(TL,rhoL,eos); p0(1,Nx/2+1:Nx) = getPfromTandRho(TR,rhoR,eos);

primitives0 = [rho0;u0;p0]; % [rho, u, p]
conservatives0 = primitivesToConservatives(primitives0, eos); % [rho, rho*u, E]
W = conservatives0;
V = primitives0;
printInfo(W,V,0,0,0); % print information

% Simulation time
t_final = 5e-4; % simulation end time
t_sim = 0; % simulation time
step = 0; % time step
CHECK = 100; % check interval

%% Solve

poi = Nx/2;
residuals = zeros(6, 1);
while (t_sim < t_final)

    %%%%%%%%%%%%%%%%
    % Calculate dt %
    %%%%%%%%%%%%%%%%

    sos = getSos(V, eos); % speed of sound
    u = V(2,:);
    dt = CFL*dx/max(max(max(u,u+sos),u-sos));
    if (t_sim + dt > t_final) 
        dt = t_final - t_sim;
    end % last time step dt
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Time Advancement (Algorithm 1) %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Store gammaS and e0S
    [gammaS, e0S] = getGammaSandE0S(V, eos);

    % Time marching with double-flux model (RK3)
    k1 = W + dt*getRHS_DF(W, V, gammaS, e0S, solver);
    fprintf("k1 at NX/2:\t%f\t%f\t%f\n", k1(:,Nx/2));
    fprintf("k1 at NX/2 + 1:\t%f\t%f\t%f\n", k1(:,Nx/2 + 1));
    
    Vk1 = updatePrimitives_DF(k1, gammaS, e0S);
    fprintf("Vk1 at NX/2:\t%f\t%f\t%f\n", Vk1(:,Nx/2));
    fprintf("Vk1 at NX/2 + 1:\t%f\t%f\t%f\n", Vk1(:,Nx/2 + 1));
    
    k2 = 3/4*W + 1/4*k1 + 1/4*dt*getRHS_DF(k1,Vk1,gammaS,e0S, solver);
    Vk2 = updatePrimitives_DF(k2, gammaS, e0S);
    
    W = 1/3*W + 2/3*k2 + 2/3*dt*getRHS_DF(k2, Vk2, gammaS, e0S, solver);
    V = updatePrimitives_DF(W, gammaS, e0S);
    
    % Update conservatives
    W = primitivesToConservatives(V, eos);
    e = (W(3,:) - 0.5*V(1,:).*u.^2)./V(1,:);
    T = getTfromPandRho(V(3,:), V(1,:), eos);
    
    %%%%%%%%%%%%%%%%%%
    % Check Solution %
    %%%%%%%%%%%%%%%%%%
    
    % Update simulation time and step
    t_sim = t_sim + dt;
    step = step + 1;
    
    % Compute residual for Xn - X(n-1);
    if step == 1
        residuals(1, step) = V(1, poi);
        residuals(2, step) = V(2, poi);
        residuals(3, step) = V(3, poi);
        residuals(4, step) = W(3, poi);
        residuals(5, step) = e(poi);
        residuals(6, step) = T(poi);
    else
        residuals(1, step) = V(1, poi);
        residuals(1, step-1) = residuals(1, step-1) - V(1, poi);
        residuals(2, step) = V(2, poi);
        residuals(2, step-1) = residuals(2, step-1) - V(2, poi);
        residuals(3, step) = V(3, poi);
        residuals(3, step-1) = residuals(3, step-1) - V(3, poi);
        residuals(4, step) = W(3, poi);
        residuals(4, step-1) = residuals(4, step-1) - W(3, poi);
        residuals(5, step) = e(poi);
        residuals(5, step-1) = residuals(5, step-1) - e(poi);
        residuals(6, step) = T(poi);
        residuals(6, step-1) = residuals(6, step-1) - T(poi);
    end
    
    % Print information
    if (mod(step,CHECK) == 0 || t_sim >= t_final)
        printInfo(W,V,step,t_sim,dt);
    end
    % disp(t_sim)
    
    % plotSolnInit(W(:,show_start:show_end),V(:,show_start:show_end),x(show_start:show_end),t_final,conservatives0(:,show_start:show_end),primitives0(:,show_start:show_end), residuals, eos);
    % pause(0.01);
                
end

toc % end timer

%% Plot

% plotSolnInit(W,V,x,t_final,conservatives0,primitives0, residuals, eos);
plotSolnInit(W(:,show_start:show_end),V(:,show_start:show_end),x(show_start:show_end),t_final,conservatives0(:,show_start:show_end),primitives0(:,show_start:show_end), residuals, eos);
writematrix([x(show_start:show_end).',V(:,show_start:show_end).'], sprintf('DF_%d.xlsx', solver))
