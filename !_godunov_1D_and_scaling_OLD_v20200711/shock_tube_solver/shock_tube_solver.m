%% HEADER

% Author:       Jeremy Wang, University of Waterloo
% Last Update:  2020-11-18
% Description: 
%   @V1
%       - Solves the shock tube problem based on Wang & Hickey (2020): 
%       Analytical solutions to shock and expansion waves for non-ideal
%       equations of state, Physics of Fluids, 32, 086105


clear; clc; close all;
fprintf("Shock tube calculation initiated...\n");

%% NOMENCLATURE, UNIVERSAL CONSTANTS, AND UNITS
% Physical quantities generally follow the format:
% <quantity>_<location>_<calculationmethod>
% e.g. p_1_IG, delta_h_1_miller

% Variables:
% a = constant in the eos [Pa (m^3 kmol^-1)^2 K^0.5]
% b = constant in the eos [m^3 kmol^-1]
% c = speed of sound [m s^-1]
% c_p = specific heat capacity at constant pressure [J kmol^-1 K^-1]
% c_v = specific heat capacity at constant volume [J kmol^-1 K^-1]
% h = enthalpy [J kmol^-1]
% M = molar mass [kg kmol^-1]
% p = pressure [Pa]
% rho = density [kg m^-3]
% T = temperature [K]
% u = speed [m s^-1]
% v = molar volume [m^3 kmol^-1]

% Subscripts:
% 'miller' = calculated from Miller et al. (2001) paper on DNS
% 'IG' = calculated using ideal gas law
% 'TP' = calculated using a thermally perfect gas

% Universal constants:
R = 8314.4621; % Universal gas constant [J kmol^-1 K^-1]


%% INPUTS
fprintf("Reading spreadsheets with runs and gas properties data...\n");

% Choose name of input file:
filename_runs = 'exact_test_cases.xlsx';
num_run = 5; % choose which row of the runs to compute
filename_properties = 'gas_properties.xlsx';

% Choose solution params
iterations_fsolve_max = 10000;
tol = 1e-8;
URV = 0.4; % specific volume underrelaxation factor

% Display params
show_fsolve = 1;
x_min = -0.5;
x_max = 0.5;
dx = (x_max-x_min)/1000;
x_vec = x_min:dx:x_max;

% TBD

%% RUN BATCH ANALYSIS
% Convert inputs to structs
properties = table2struct(readtable(filename_properties));
runs = table2struct(readtable(filename_runs));

fprintf("\n-------------------------------------------------------------\n");
fprintf("-------------------------------------------------------------\n\n");
for eos = 1:3
    fprintf("Now processing Run #%d of %d...\n", num_run, length(runs));

    %% PROCESSING INPUTS
    % Retrieve the gas properties:
    gas_name = runs(num_run).gas_name; % get the current gas name
    num_gas = find(contains({properties.gas_name}, gas_name)); % retrieve index to the gas properties table

    gamma(eos) = properties(num_gas).gamma;      % specific heat ratio
    M(eos) = properties(num_gas).M;              % molar mass
    T_c(eos) = properties(num_gas).T_c;          % critical temperature
    p_c(eos) = properties(num_gas).p_c;          % critical pressure
    omega(eos) = properties(num_gas).omega;      % acentric factor
    h_ref(eos) = properties(num_gas).h_ref;      % reference enthalpy 

    % NOTE: h_ref is never explicitly computed, it only appears during
        % integration and cancels out in the equtaions that need to be solved. 
        % Thus, h_ref is included only for completeness but it does NOT affect 
        % solutions, so a trivial value of zero is used.  

    % Retrieve fit parameters for thermally perfect c_IG_p:
    A = str2num(properties(num_gas).c_IG_p_fit_params);

    % Retrieve time and boundary conditions for the current run:
    t(eos) = runs(num_run).t;
    rho_L(eos) = runs(num_run).rho_L;
    T_L(eos) = runs(num_run).T_L;
    u_L(eos) = runs(num_run).u_L;
    rho_R(eos) = runs(num_run).rho_R;
    T_R(eos) = runs(num_run).T_R;
    u_R(eos) = runs(num_run).u_R;


    %% DEFINITIONS & SYMBOLIC FUNCTIONS
    fprintf("Now executing basic definitions and symbolic functions...\n");

    % Set / reset the symbolic variables
    syms p v T

    % Gas constants
    R_specific = R / M(eos); % [J kg^-1 K^-1]

    % Setting eos parameters based on input selection
    switch eos
        case 0 % Ideal Gas Law
            eos_tag = "Ideal and thermally perfect gas";
            a = 0;
            b = 0;
            Theta = 0;
            delta = 0;
            epsilon = 0;
            p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
        case 1 % Redlich Kwong
            eos_tag = "RK with departure functions relative to thermally perfect gas";
            a = 0.4278 * R^2 * T_c(eos)^(2.5) / p_c(eos);
            b = 0.0867 * R * T_c(eos) / p_c(eos);
            Theta = a * (1/T)^0.5;
            delta = b;
            epsilon = 0;
            p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
        case 2 % Soave-Redlich-Kwong
            eos_tag = "SRK with departure functions relative to thermally perfect gas";
            a = 0.42747 * R^2 * T_c(eos)^2 / p_c(eos);
            b = 0.08664 * R * T_c(eos) / p_c(eos);
            Theta = a*(1 + (0.48 + 1.574*omega(eos) - 0.176*omega(eos)^2)*(1-(T/T_c(eos))^0.5))^2;
            delta = b;
            epsilon = 0;
            p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
        case 3 % Peng-Robinson
            eos_tag = "PR with departure functions relative to thermally perfect gas";
            a = 0.45724 * R^2 * T_c(eos)^2 / p_c(eos);
            b = 0.07780 * R * T_c(eos) / p_c(eos);
            Theta = a*(1 + (0.37464 + 1.54226*omega(eos) - 0.2699*omega(eos)^2)*(1-(T/T_c(eos))^0.5))^2;
            delta = 2*b;
            epsilon = -b^2;
            p = R*T/(v-b)-Theta/(v^2+delta*v+epsilon);
    end
    % NOTE: since virial eos diverge near criticality and BWR is close to
    % virial, we do not compute here.
    fprintf("\teos selection: %s...\n", eos_tag);

    % Derivatives
    dThetadT = diff(Theta, T);
    d2ThetadT2 = diff(Theta, T, 2);
    exp_dpdT = diff(p, T);
    exp_d2pdT2 = diff(p, T, 2);
    exp_dpdv = diff(p, v);

    % Derivatives manually written out
    % exp_dpdT = R/(v-b)-(dThetadT)/(v^2+delta*v+epsilon);
    % exp_d2pdT2 = -(d2ThetadT2)/(v^2+delta*v+epsilon);
    % exp_dpdv = R*T/(v-b)^2+Theta*(2*v+delta)/(v^2+delta*v+epsilon)^2;

    % Integrals
    exp_intdpdT = real(int(exp_dpdT, v)); % [J kmol^-1 K^-1]
    exp_intvdpdv = real(int(v*exp_dpdv, v)); % [J kmol^-1]
    exp_intd2pdT2 = real(int(exp_d2pdT2, v)); % [J kmol^-1 K^-2]

    % Integrals manually written out
    % exp_intdpdT = R*log(v-b) + dThetadT*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon));
    % exp_intvdpdv = -Theta*v/(v^2+delta*v+epsilon) - Theta*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon)) ...
    %     - R*T*log(v-b) + R*T*b/(v-b);
    % exp_intd2pdT2 = d2ThetadT2*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon));

    % Specific heat capacities and enthalpies
    T_mat = T; % reset the length of T_mat (this default value is overridden)
    for i = 1:length(A) 
        T_mat(i,1) = T^(i-1); 
    end

    c_p_IG_TP = R*(A*T_mat); % verified
    c_v_IG_TP = c_p_IG_TP - R; % verified
    c_v = c_v_IG_TP + T*exp_intd2pdT2; % verified
    c_p = c_v - T*exp_dpdT^2/exp_dpdv; % verified
    delta_h_IG_TP = real((Theta-T*dThetadT)*log((sqrt(delta^2-4*epsilon)+2*v+delta)/(sqrt(delta^2-4*epsilon)-2*v-delta))/(sqrt(delta^2-4*epsilon)) + R*T - p*v);
    % NOTE: delta_h_IG was taken directly from Maple's computation of
    % {-T*exp_intdpdT - exp_intvdpdv}. For some reason, using the following diverges:
    % delta_h_IG_TP = -T*exp_intdpdT - exp_intvdpdv;

    if eos == 0 % if ideal gas eos selected, delta_h_IG evaluates to a NaN when it should be zero.
        delta_h_IG_TP = 0;
    end

    h_IG_TP = int(c_p_IG_TP, T) - h_ref(eos);
    h = h_IG_TP - delta_h_IG_TP;

    % Miller & Bellan (2001) results for Peng-Robinson:
    K_1 = (1/(2*sqrt(2)*b))*log( (v+(1-sqrt(2))*b)/(v+(1+sqrt(2))*b) );
    delta_h_miller = - p*v + R*T - K_1*(Theta - T*dThetadT);
    h_miller = h_IG_TP - delta_h_miller;

    % Speed of sound
    c = sqrt(-v^2*c_p*exp_dpdv/(M(eos)*c_v));


    %% COMPUTATIONS
    fprintf("Now performing computations...\n");

    %% COMPUTE REMAINING INITIAL CONDITIONS (IDEAL GAS)
    % -------------------------------------------------------------------------
    fprintf("\tComputing remaining initial conditions (ideal gas)...\n");

    % Left
    T = T_L(eos); 
    v_L(eos) = M(eos)/rho_L(eos);
    v = v_L(eos);
    p_L_IG(eos) = rho_L(eos)*R_specific*T_L(eos);
    c_L_IG_TP(eos) = sqrt(gamma(eos)*R_specific*T_L(eos));
    h_L_IG_TP(eos) = double(subs(h_IG_TP));

    % Right
    T = T_R(eos); 
    v_R(eos) = M(eos)/rho_R(eos);
    v = v_R(eos);
    p_R_IG(eos) = rho_R(eos)*R_specific*T_R(eos);
    c_R_IG_TP(eos) = sqrt(gamma(eos)*R_specific*T_R(eos));
    h_R_IG_TP(eos) = double(subs(h_IG_TP));

    
    %% COMPUTE REMAINING INITIAL CONDITIONS (REAL GAS)
    % -------------------------------------------------------------------------
    fprintf("\tComputing remaining initial conditions (real gas)...\n");

    % Left
    T = T_L(eos); 
    v = v_L(eos);
    p_L(eos) = double(subs(p));
    c_L(eos) = double(subs(c));
    h_L(eos) = double(subs(h));

    % Right
    T = T_R(eos); 
    v = v_R(eos);
    p_R(eos) = double(subs(p));
    c_R(eos) = double(subs(c));
    h_R(eos) = double(subs(h));


    %% COMPUTE SHOCK TUBE SOLUTION (IDEAL GAS)
    % -----------------------------------------------------------------
    fprintf("\tComputing shock tube solutions (ideal gas)...\n");

    i = 1;
    u_star_IG_L = 9999;
    u_star_IG_R = 0;
    p_star_IG = (c_R_IG_TP(eos)*rho_R(eos)*p_L_IG(eos) ...
        + c_L_IG_TP(eos)*rho_L(eos)*p_R_IG(eos) ...
        + c_R_IG_TP(eos)*rho_R(eos)*c_L_IG_TP(eos)*rho_L(eos)*(u_L(eos) - u_R(eos))) ...
        / (c_L_IG_TP(eos)*rho_L(eos) + c_R_IG_TP(eos)*rho_R(eos)); % Guess star-state pressure using Toro Eq 9.28
    
    while abs(u_star_IG_L(end) - u_star_IG_R(end)) > tol
        % Update pressure
        if i == 2 % first iterate, use mean Lagrangian wavespeed 
            if u_star_IG_L(i-1) == u_L(eos) 
                C_L_bar = rho_L(eos)*c_L_IG_TP(eos);
            else
                C_L_bar = abs(p_star_IG(i-1) - p_L_IG(eos))/abs(u_star_IG_L(i-1) - u_L(eos));
            end
            if u_star_IG_R(i-1) == u_R(eos)
                C_R_bar = rho_R(eos)*c_R_IG_TP(eos);
            else
                C_R_bar = abs(p_star_IG(i-1) - p_R_IG(eos))/abs(u_star_IG_R(i-1) - u_R(eos));
            end
            p_star_IG(i) = (C_R_bar*p_L_IG(eos) ...
                + C_L_bar*p_R_IG(eos) ...
                + C_R_bar*C_L_bar*(u_L(eos) - u_R(eos))) ...
                / (C_L_bar + C_R_bar);
        elseif i >= 3 % use secant method
            p_star_IG(i) = p_star_IG(i-1) - (u_star_IG_L(i-1) - u_star_IG_R(i-1))*(p_star_IG(i-1) - p_star_IG(i-2))/...
                (u_star_IG_L(i-1) - u_star_IG_R(i-1) - u_star_IG_L(i-2) + u_star_IG_R(i-2));
        end
        
        % Check left wave:
        syms u v T
        if p_star_IG(i) < p_L_IG(eos) % rarefaction
            % Use polytropic relations
            v_star_IG_L = v_L(eos)*(p_L_IG(eos)/p_star_IG(i))^(1/gamma(eos));
            T_star_IG_L = T_L(eos)*(p_star_IG(i)/p_L_IG(eos))^((gamma(eos)-1)/gamma(eos));
            c_star_IG_L = sqrt(gamma(eos)*R_specific*T_star_IG_L);
            u_star_IG_L(i) = 2/(gamma(eos) - 1)*(c_L_IG_TP(eos) - c_star_IG_L);
        else % shock
            % Solve CME equation for density
            eqn_energy = h_L_IG_TP(eos)/M(eos) + (1/2)*(M(eos)/(v*rho_L(eos)))*(p_star_IG(i) - p_L_IG(eos))/(M(eos)/v - rho_L(eos)) ...
                - h/M(eos) - (1/2)*(rho_L(eos)*v/M(eos))*(p_star_IG(i) - p_L_IG(eos))/(M(eos)/v - rho_L(eos));
            eqn_eos = p_star_IG(i) - R*T/v;

            mat_ns = [eqn_energy; eqn_eos];
            f = matlabFunction(mat_ns); f_str = func2str(f);
            f_str = strrep(f_str, '@(T,v)', '@(x)'); f_str = strrep(f_str, 'T', 'x(1)'); f_str = strrep(f_str, 'v', 'x(2)');
            f = str2func(f_str);

            init = [0.5*(T_L(eos) + T_R(eos)); ...
                0.5*(v_L(eos) + v_R(eos))];
            if show_fsolve
                S = fsolve(f, init, optimoptions('fsolve', 'Display', 'iter', 'MaxFunctionEvaluations', iterations_fsolve_max));
            else
                S = fsolve(f, init, optimoptions('fsolve', 'MaxFunctionEvaluations', iterations_fsolve_max));
            end
            v_star_IG_L = S(2); T_star_IG_L = S(1);
            
            % Find speed of sound
            c_star_IG_L = sqrt(gamma(eos)*R_specific*T_star_IG_L);
            
            % Solve left shock speed
            W_L = u_L(eos) - sqrt((M(eos)/(v_star_IG_L*rho_L(eos)))*(p_star_IG(i) - p_L_IG(eos))/(M(eos)/v_star_IG_L - rho_L(eos)));

            % Solve for u_star_L
            u_star_IG_L(i) = W_L + sqrt((rho_L(eos)*v_star_IG_L/M(eos))*(p_star_IG(i) - p_L_IG(eos))/(M(eos)/v_star_IG_L - rho_L(eos)));
        end

        % Check right wave type:
        syms u v T
        if p_star_IG(i) < p_R_IG(eos) % rarefaction
            % Use polytropic relations
            v_star_IG_R = v_R(eos)*(p_R_IG(eos)/p_star_IG(i))^(1/gamma(eos));
            T_star_IG_R = T_R(eos)*(p_star_IG(i)/p_R_IG(eos))^((gamma(eos)-1)/gamma(eos));
            c_star_IG_R = sqrt(gamma(eos)*R_specific*T_star_IG_R);
            u_star_IG_R(i) = 2/(gamma(eos) - 1)*(c_star_IG_R - c_R_IG_TP(eos));
        else % shock
            % Solve CME equation for density
            eqn_energy = h_R_IG_TP(eos)/M(eos) + (1/2)*(M(eos)/(v*rho_R(eos)))*(p_star_IG(i) - p_R_IG(eos))/(M(eos)/v - rho_R(eos)) ...
                - h/M(eos) - (1/2)*(rho_R(eos)*v/M(eos))*(p_star_IG(i) - p_R_IG(eos))/(M(eos)/v - rho_R(eos));
            eqn_eos = p_star_IG(i) - R*T/v;
            mat_ns = [eqn_energy; eqn_eos];
            f = matlabFunction(mat_ns); f_str = func2str(f);
            f_str = strrep(f_str, '@(T,v)', '@(x)'); f_str = strrep(f_str, 'T', 'x(1)'); f_str = strrep(f_str, 'v', 'x(2)');
            f = str2func(f_str);

            init = [0.5*(T_L(eos) + T_R(eos)); ...
                0.5*(v_L(eos) + v_R(eos))];
            if show_fsolve
                S = fsolve(f, init, optimoptions('fsolve', 'Display', 'iter', 'MaxFunctionEvaluations', iterations_fsolve_max));
            else
                S = fsolve(f, init, optimoptions('fsolve', 'MaxFunctionEvaluations', iterations_fsolve_max));
            end
            v_star_IG_R = S(2); T_star_IG_R = S(1);

            % Find speed of sound
            c_star_IG_R = sqrt(gamma(eos)*R_specific*T_star_IG_R);
            
            % Solve right shock speed
            W_R = u_R(eos) + sqrt((M(eos)/(v_star_IG_R*rho_R(eos)))*(p_star_IG(i) - p_R_IG(eos))/(M(eos)/v_star_IG_R - rho_R(eos)));

            % Solve for u_star_R
            u_star_IG_R(i) = W_R - sqrt((rho_R(eos)*v_star_IG_R/M(eos))*(p_star_IG(i) - p_R_IG(eos))/(M(eos)/v_star_IG_R - rho_R(eos)));
        end
        
        fprintf("IDEAL GAS\ti = %d\tp_star = %f\tu_star_L = %f\tu_star_R = %f...\n", i, p_star_IG(i), u_star_IG_L(i), u_star_IG_R(i));
        i = i + 1;
    end
    
    % Compute the final star-state pressure and velocity
    p_star_IG_final(eos) = p_star_IG(end);
    u_star_IG_final(eos) = 0.5*(u_star_IG_L(end) + u_star_IG_R(end));
    v_star_IG_L_final(eos) = v_star_IG_L;
    T_star_IG_L_final(eos) = T_star_IG_L;
    v_star_IG_R_final(eos) = v_star_IG_R;
    T_star_IG_R_final(eos) = T_star_IG_R;
    
    % Compute and export solutions as functions of (x,t)
    for j = 1:length(x_vec)
        if x_vec(j) < (u_L(eos) - c_L_IG_TP(eos))*t(eos) % Left constant region
            u_IG(j) = u_L(eos);
            p_IG(j) = p_L_IG(eos);
            rho_IG(j) = M(eos)/v_L(eos);
            T_IG(j) = T_L(eos);
        elseif x_vec(j) >= (u_L(eos) - c_L_IG_TP(eos))*t(eos) && x_vec(j) < (u_star_IG_final(eos) - c_star_IG_L)*t(eos) % Expansion
            u_IG(j) = (2/(gamma(eos) + 1)) * (c_L_IG_TP(eos) + x_vec(j)/t(eos));
            p_IG(j) = p_L_IG(eos) * (1 - 0.5*(gamma(eos) - 1)*u_IG(j)/c_L_IG_TP(eos))^(2*gamma(eos)/(gamma(eos)-1));
            rho_IG(j) = (M(eos)/v_L(eos)) * (1 - 0.5*(gamma(eos) - 1)*u_IG(j)/c_L_IG_TP(eos))^(2/(gamma(eos)-1));
            T_IG(j) = T_L(eos) * (1 - 0.5*(gamma(eos) - 1)*u_IG(j)/c_L_IG_TP(eos))^2;
        elseif x_vec(j) >= (u_star_IG_final(eos) - c_star_IG_L)*t(eos) && x_vec(j) < u_star_IG_final(eos)*t(eos) % Left star state
            u_IG(j) = u_star_IG_final(eos);
            p_IG(j) = p_star_IG_final(eos);
            rho_IG(j) = M(eos)/v_star_IG_L_final(eos);
            T_IG(j) = T_star_IG_L_final(eos);
        elseif x_vec(j) >= u_star_IG_final(eos)*t(eos) && x_vec(j) < W_R*t(eos) % Right star state
            u_IG(j) = u_star_IG_final(eos);
            p_IG(j) = p_star_IG_final(eos);
            rho_IG(j) = M(eos)/v_star_IG_R_final(eos);
            T_IG(j) = T_star_IG_R_final(eos);
        elseif x_vec(j) >= W_R*t(eos) % Right constant region
            u_IG(j) = u_R(eos);
            p_IG(j) = p_R_IG(eos);
            rho_IG(j) = M(eos)/v_R(eos);
            T_IG(j) = T_R(eos);
        end
    end
    
    writematrix([x_vec.', u_IG.', p_IG.', rho_IG.', T_IG.'], sprintf('IG_%d.xlsx', eos));
    
    % CHECKED TO HERE
    
    %% COMPUTE SHOCK TUBE SOLUTION (REAL GAS)
    % -----------------------------------------------------------------
    fprintf("\tComputing shock tube solutions (real gas)...\n");
    
    i = 1;
    u_star_L = 9999;
    u_star_R = 0;
    p_star = (c_R(eos)*rho_R(eos)*p_L(eos) ...
        + c_L(eos)*rho_L(eos)*p_R(eos) ...
        + c_R(eos)*rho_R(eos)*c_L(eos)*rho_L(eos)*(u_L(eos) - u_R(eos))) ...
        / (c_L(eos)*rho_L(eos) + c_R(eos)*rho_R(eos)); % Guess star-state pressure using Toro Eq 9.28 (Colella and Glaz???)
    v_star_L = M(eos)/(rho_L(eos) + (p_star(i) - p_L(eos))/(c_L(eos)^2));
    v_star_R = M(eos)/(rho_R(eos) + (p_star(i) - p_R(eos))/(c_R(eos)^2));
    
    while abs(u_star_L(end) - u_star_R(end)) > tol
        % Update pressure and molar specific volume
        if i == 2 % first iterate, use mean Lagrangian wavespeed 
            if u_star_L(i-1) == u_L(eos) 
                C_L_bar = rho_L(eos)*c_L(eos);
            else
                C_L_bar = abs(p_star(i-1) - p_L(eos))/abs(u_star_L(i-1) - u_L(eos));
            end
            if u_star_R(i-1) == u_R(eos)
                C_R_bar = rho_R(eos)*c_R(eos);
            else
                C_R_bar = abs(p_star(i-1) - p_R(eos))/abs(u_star_R(i-1) - u_R(eos));
            end
            p_star(i) = (C_R_bar*p_L(eos) ...
                + C_L_bar*p_R(eos) ...
                + C_R_bar*C_L_bar*(u_L(eos) - u_R(eos))) ...
                / (C_L_bar + C_R_bar);
            v_star_L(i) = M(eos)/(rho_L(eos) + (p_star(i) - p_L(eos))/(c_L(eos)^2));
            v_star_R(i) = M(eos)/(rho_R(eos) + (p_star(i) - p_R(eos))/(c_R(eos)^2));
        elseif i >= 3 % use secant method
            p_star(i) = p_star(i-1) - (u_star_L(i-1) - u_star_R(i-1))*(p_star(i-1) - p_star(i-2))/...
                (u_star_L(i-1) - u_star_R(i-1) - u_star_L(i-2) + u_star_R(i-2));
            v_star_L(i) = v_star_L(i-1) - URV*(u_star_L(i-1) - u_star_R(i-1))*(v_star_L(i-1) - v_star_L(i-2))/...
                (u_star_L(i-1) - u_star_R(i-1) - u_star_L(i-2) + u_star_R(i-2));
            v_star_R(i) = v_star_R(i-1) - URV*(u_star_L(i-1) - u_star_R(i-1))*(v_star_R(i-1) - v_star_R(i-2))/...
                (u_star_L(i-1) - u_star_R(i-1) - u_star_L(i-2) + u_star_R(i-2));
        end
        
        % Check left wave:
        syms u v T A_1L A_2L A_3L A_4L A_5L A_6L
        if p_star(i) < p_L(eos) % rarefaction
            eqn_p_L = p_L(eos) - exp(A_3L*(c_L(eos) - u_L(eos)) - A_4L);
            eqn_u_L = u_L(eos) - A_5L - A_6L*(u_L(eos) - c_L(eos));
            eqn_v_L = v_L(eos) - exp(A_1L*(u_L(eos) - c_L(eos)) - A_2L);
            eqn_p_star_L = p_star(i) - exp(A_3L*(subs(c, v, v_star_L(i)) - u) - A_4L);
            eqn_u_star_L = u - A_5L - A_6L*(u - subs(c, v, v_star_L(i)));
            eqn_v_star_L = v_star_L(i) - exp(A_1L*(u - subs(c, v, v_star_L(i))) - A_2L);
            eqn_p_star_L2 = p_star(i) - subs(p, v, v_star_L(i));
            eqn_J_plus_L = p_star(i) + (M(eos)/v_star_L(i))*subs(c, v, v_star_L(i))*u - p_L(eos) - (M(eos)/v_L(eos))*c_L(eos)*u_L(eos);
            mat_ns = [eqn_p_L; eqn_u_L; eqn_v_L; eqn_p_star_L; eqn_u_star_L; eqn_v_star_L; eqn_p_star_L2; eqn_J_plus_L];
            
            f = matlabFunction(mat_ns); f_str = func2str(f);
            f_str = strrep(f_str, '@(A_1L,A_2L,A_3L,A_4L,A_5L,A_6L,T,u)', '@(x)'); 
            f_str = strrep(f_str, 'A_1L', 'x(1)'); 
            f_str = strrep(f_str, 'A_2L', 'x(2)');
            f_str = strrep(f_str, 'A_3L', 'x(3)'); 
            f_str = strrep(f_str, 'A_4L', 'x(4)');
            f_str = strrep(f_str, 'A_5L', 'x(5)'); 
            f_str = strrep(f_str, 'A_6L', 'x(6)');
            f_str = strrep(f_str, 'T', 'x(7)'); 
            f_str = strrep(f_str, 'u', 'x(8)');
            f = str2func(f_str);
            
            init = [1/(u_L(eos) - c_L(eos) - u_star_IG_final(eos) + c_star_IG_L) * log(v_L(eos)/v_star_IG_L); ...
                -0.5*log(v_L(eos)*v_star_IG_L) + 0.5*log(v_L(eos)/v_star_IG_L)*(u_L(eos) - c_L(eos) + u_star_IG_final(eos) - c_star_IG_L)/(u_L(eos) - c_L(eos) - u_star_IG_final(eos) + c_star_IG_L); ...
                -1/(u_L(eos) - c_L(eos) - u_star_IG_final(eos) + c_star_IG_L) * log(p_L(eos)/p_star(i)); ...
                -0.5*log(p_L(eos)*p_star(i)) + 0.5*log(p_L(eos)/p_star(i))*(u_L(eos) - c_L(eos) + u_star_IG_final(eos) - c_star_IG_L)/(u_L(eos) - c_L(eos) - u_star_IG_final(eos) + c_star_IG_L); ...
                0.5*(u_L(eos) + u_star_IG_final(eos)) - 0.5*(u_L(eos) - c_L(eos) + u_star_IG_final(eos) - c_star_IG_L)*(u_L(eos) - u_star_IG_final(eos))/(u_L(eos) - c_L(eos) - u_star_IG_final(eos) + c_star_IG_L); ...
                (u_L(eos) - u_star_IG_final(eos))/(u_L(eos) - c_L(eos) - u_star_IG_final(eos) + c_star_IG_L);
                T_star_IG_L; ...
                u_star_IG_final(eos)];
            
            if show_fsolve
                S = fsolve(f, init, optimoptions('fsolve', 'Display', 'iter', 'MaxFunctionEvaluations', iterations_fsolve_max));
            else
                S = fsolve(f, init, optimoptions('fsolve', 'MaxFunctionEvaluations', iterations_fsolve_max));
            end
            S = real(S);
            A_1L = S(1); 
            A_2L = S(2);
            A_3L = S(3);
            A_4L = S(4);
            A_5L = S(5);
            A_6L = S(6);
            T_star_L = S(7);
            u_star_L(i) = S(8);
            
            c_star_L = double(subs(c, [v, T], [v_star_L(i), T_star_L]));
        else % shock
            % Solve CME equation for density
            eqn_energy = h_L(eos)/M(eos) + (1/2)*(M(eos)/(v*rho_L(eos)))*(p_star(i) - p_L(eos))/(M(eos)/v - rho_L(eos)) ...
                - h/M(eos) - (1/2)*(rho_L(eos)*v/M(eos))*(p_star(i) - p_L(eos))/(M(eos)/v - rho_L(eos));
            eqn_eos = p_star(i) - p;

            mat_ns = [eqn_energy; eqn_eos];
            f = matlabFunction(mat_ns); f_str = func2str(f);
            f_str = strrep(f_str, '@(T,v)', '@(x)'); f_str = strrep(f_str, 'T', 'x(1)'); f_str = strrep(f_str, 'v', 'x(2)');
            f = str2func(f_str);

            init = [0.5*(T_L(eos) + T_R(eos)); ...
                0.5*(v_L(eos) + v_R(eos))];
            if show_fsolve
                S = fsolve(f, init, optimoptions('fsolve', 'Display', 'iter', 'MaxFunctionEvaluations', iterations_fsolve_max));
            else
                S = fsolve(f, init, optimoptions('fsolve', 'MaxFunctionEvaluations', iterations_fsolve_max));
            end
            v_star_L(i) = S(2); T_star_L = S(1);

            % Solve left shock speed
            W_L = u_L(eos) - sqrt((M(eos)/(v_star_L(i)*rho_L(eos)))*(p_star(i) - p_L(eos))/(M(eos)/v_star_L(i) - rho_L(eos)));

            % Solve for u_star_L
            u_star_L(i) = W_L + sqrt((rho_L(eos)*v_star_L(i)/M(eos))*(p_star(i) - p_L(eos))/(M(eos)/v_star_L(i) - rho_L(eos)));
        end
        % Check right wave type:
        syms u v T A_1_R A_2_R A_3_R A_4_R A_5_R A_6_R
        if p_star(i) < p_R(eos) % rarefaction
%             v_star_R = v_star_IG_R; % FLAG change update rule
%             eqn_p_R = p_R(eos) - exp(A_3_R*(u_R(eos) - c_R(eos)) - A_4_R);
%             eqn_u_R = u_R(eos) - A_5_R + A_6_R*(c_R(eos) - u_R(eos));
%             eqn_v_R = v_R(eos) - exp(A_1_R*(c_R(eos) - u_R(eos)) - A_2_R);
%             eqn_p_star_R = p_star(i) - exp(A_3_R*(u - subs(c, v, v_star_R)) - A_4_R);
%             eqn_u_star_R = u - A_5_R - A_6_R*(subs(c, v, v_star_R) - u);
%             eqn_v_star_R = v_star_R - exp(A_1_R*(subs(c, v, v_star_R) - u) - A_2_R);
% %             eqn_p_R = p_R(eos) - exp(A_3_R*(c_R(eos) - u_R(eos)) - A_4_R);
% %             eqn_u_R = u_R(eos) - A_5_R + A_6_R*(u_R(eos) - c_R(eos));
% %             eqn_v_R = v_R(eos) - exp(A_1_R*(u_R(eos) - c_R(eos)) - A_2_R);
% %             eqn_p_star_R = p_star(i) - exp(A_3_R*(subs(c, v, v_star_R) - u) - A_4_R);
% %             eqn_u_star_R = u - A_5_R - A_6_R*(u - subs(c, v, v_star_R));
% %             eqn_v_star_R = v_star_R - exp(A_1_R*(u - subs(c, v, v_star_R)) - A_2_R);
%             
%             mat_ns = [eqn_p_R; eqn_u_R; eqn_v_R; eqn_p_star_R; eqn_u_star_R; eqn_v_star_R];
%             f = matlabFunction(mat_ns); f_str = func2str(f);
%             f_str = strrep(f_str, '@(A_1_R,A_2_R,A_3_R,A_4_R,A_5_R,A_6_R,T,u)', '@(x)'); 
%             f_str = strrep(f_str, 'A_1_R', 'x(1)'); 
%             f_str = strrep(f_str, 'A_2_R', 'x(2)');
%             f_str = strrep(f_str, 'A_3_R', 'x(3)'); 
%             f_str = strrep(f_str, 'A_4_R', 'x(4)');
%             f_str = strrep(f_str, 'A_5_R', 'x(5)'); 
%             f_str = strrep(f_str, 'A_6_R', 'x(6)');
%             f_str = strrep(f_str, 'T', 'x(7)'); 
%             f_str = strrep(f_str, 'u', 'x(8)');
%             f = str2func(f_str);
%             
%             init = [-1/(u_R(eos) - c_R(eos) - u_star_IG_final(eos) + c_star_IG_R) * log(v_R(eos)/v_star_R); ...
%                 -0.5*log(v_R(eos)*v_star_R) + 0.5*log(v_R(eos)/v_star_R)*(u_R(eos) - c_R(eos) + u_star_IG_final(eos) - c_star_IG_R)/(u_R(eos) - c_R(eos) - u_star_IG_final(eos) + c_star_IG_R); ...
%                 1/(u_R(eos) - c_R(eos) - u_star_IG_final(eos) + c_star_IG_R) * log(p_R(eos)/p_star(i)); ...
%                 -0.5*log(p_R(eos)*p_star(i)) + 0.5*log(p_R(eos)/p_star(i))*(u_R(eos) - c_R(eos) + u_star_IG_final(eos) - c_star_IG_R)/(u_R(eos) - c_R(eos) - u_star_IG_final(eos) + c_star_IG_R); ...
%                 0.5*(u_R(eos) + u_star_IG_final(eos)) - 0.5*(u_R(eos) - c_R(eos) + u_star_IG_final(eos) - c_star_IG_R)*(u_R(eos) - u_star_IG_final(eos))/(u_R(eos) - c_R(eos) - u_star_IG_final(eos) + c_star_IG_R); ...
%                 -(u_R(eos) - u_star_IG_final(eos))/(u_R(eos) - c_R(eos) - u_star_IG_final(eos) + c_star_IG_R);
%                 T_star_IG_R; ...
%                 u_star_IG_final(eos)];
%             
%             if show_fsolve
%                 S = fsolve(f, init, optimoptions('fsolve', 'Display', 'iter', 'MaxFunctionEvaluations', iterations_fsolve_max));
%             else
%                 S = fsolve(f, init, optimoptions('fsolve', 'MaxFunctionEvaluations', iterations_fsolve_max));
%             end
%             
%             A_1_R = S(1); 
%             A_2_R = S(2);
%             A_3_R = S(3);
%             A_4_R = S(4);
%             A_5_R = S(5);
%             A_6_R = S(6);
%             T_star_R = S(7);
%             u_star_R(i) = S(8);
        else % shock
            % Solve CME equation for density
            eqn_energy = h_R(eos)/M(eos) + (1/2)*(M(eos)/(v*rho_R(eos)))*(p_star(i) - p_R(eos))/(M(eos)/v - rho_R(eos)) ...
                - h/M(eos) - (1/2)*(rho_R(eos)*v/M(eos))*(p_star(i) - p_R(eos))/(M(eos)/v - rho_R(eos));
            eqn_eos = p_star(i) - p;
            mat_ns = [eqn_energy; eqn_eos];
            f = matlabFunction(mat_ns); f_str = func2str(f);
            f_str = strrep(f_str, '@(T,v)', '@(x)'); f_str = strrep(f_str, 'T', 'x(1)'); f_str = strrep(f_str, 'v', 'x(2)');
            f = str2func(f_str);

            init = [0.5*(T_L(eos) + T_R(eos)); ...
                0.5*(v_L(eos) + v_R(eos))];
            if show_fsolve
                S = fsolve(f, init, optimoptions('fsolve', 'Display', 'iter', 'MaxFunctionEvaluations', iterations_fsolve_max));
            else
                S = fsolve(f, init, optimoptions('fsolve', 'MaxFunctionEvaluations', iterations_fsolve_max));
            end
            v_star_R(i) = S(2); T_star_R = S(1);

            % Solve right shock speed
            W_R = u_R(eos) + sqrt((M(eos)/(v_star_R(i)*rho_R(eos)))*(p_star(i) - p_R(eos))/(M(eos)/v_star_R(i) - rho_R(eos)));

            % Solve for u_star_R
            u_star_R(i) = W_R - sqrt((rho_R(eos)*v_star_R(i)/M(eos))*(p_star(i) - p_R(eos))/(M(eos)/v_star_R(i) - rho_R(eos)));
        end 
        fprintf("REAL GAS\ti = %d\tp_star = %f\tu_star_L = %f\tu_star_R = %f...\n", i, p_star(i), u_star_L(i), u_star_R(i));
        
%         % Update pressure and molar specific volume
%         if i == 1 % first iterate, use mean Lagrangian wavespeed 
%             if u_star_L(i) == u_L(eos) 
%                 C_L_bar = rho_L(eos)*c_L(eos);
%             else
%                 C_L_bar = abs(p_star(i) - p_L(eos))/abs(u_star_L(i) - u_L(eos));
%             end
%             if u_star_R(i) == u_R(eos)
%                 C_R_bar = rho_R(eos)*c_R(eos);
%             else
%                 C_R_bar = abs(p_star(i) - p_R(eos))/abs(u_star_R(i) - u_R(eos));
%             end
%             p_star(i+1) = (C_R_bar*p_L(eos) ...
%                 + C_L_bar*p_R(eos) ...
%                 + C_R_bar*C_L_bar*(u_L(eos) - u_R(eos))) ...
%                 / (C_L_bar + C_R_bar);
%             v_star_L(i+1) = M(eos)/(rho_L(eos) + (p_star(i+1) - p_L(eos))/(c_L(eos)^2));
%             v_star_R(i+1) = M(eos)/(rho_R(eos) + (p_star(i+1) - p_R(eos))/(c_R(eos)^2));
%         else % use secant method
%             p_star(i+1) = p_star(i) - (u_star_L(i) - u_star_R(i))*(p_star(i) - p_star(i-1))/...
%                 (u_star_L(i) - u_star_R(i) - u_star_L(i-1) + u_star_R(i-1));
%             v_star_L(i+1) = v_star_L(i) - URV*(u_star_L(i) - u_star_R(i))*(v_star_L(i) - v_star_L(i-1))/...
%                 (u_star_L(i) - u_star_R(i) - u_star_L(i-1) + u_star_R(i-1));
%             v_star_R(i+1) = v_star_R(i) - URV*(u_star_L(i) - u_star_R(i))*(v_star_R(i) - v_star_R(i-1))/...
%                 (u_star_L(i) - u_star_R(i) - u_star_L(i-1) + u_star_R(i-1));
%         end
        i = i + 1;
    end
    
    % Compute the final star-state pressure and velocity
    p_star_final(eos) = p_star(end);
    u_star_final(eos) = 0.5*(u_star_L(end) + u_star_R(end));
    v_star_L_final(eos) = v_star_L(end);
    T_star_L_final(eos) = T_star_L;
    v_star_R_final(eos) = v_star_R(end);
    T_star_R_final(eos) = T_star_R;
    
    % Compute and export solutions as functions of (x,t)
    for j = 1:length(x_vec)
        if x_vec(j) < (u_L(eos) - c_L(eos))*t(eos) % Left constant region
            u_RG(j) = u_L(eos);
            p_RG(j) = p_L(eos);
            rho_RG(j) = M(eos)/v_L(eos);
            T_RG(j) = T_L(eos);
        elseif x_vec(j) >= (u_L(eos) - c_L(eos))*t(eos) && x_vec(j) < (u_star_final(eos) - c_star_L)*t(eos) % Expansion
            u_RG(j) = A_5L + A_6L*x_vec(j)/t(eos);
            p_RG(j) = exp(-A_3L*x_vec(j)/t(eos) - A_4L);
            rho_RG(j) = M(eos)/exp(A_1L*x_vec(j)/t(eos) - A_2L);
            f = matlabFunction(p_RG(j) - subs(p, v, M(eos)/rho_RG(j)));
            T_RG(j) = fsolve(f, 1, optimoptions('fsolve', 'Display', 'off', 'MaxFunctionEvaluations', iterations_fsolve_max));
        elseif x_vec(j) >= (u_star_final(eos) - c_star_L)*t(eos) && x_vec(j) < u_star_final(eos)*t(eos) % Left star state
            u_RG(j) = u_star_final(eos);
            p_RG(j) = p_star_final(eos);
            rho_RG(j) = M(eos)/v_star_L_final(eos);
            T_RG(j) = T_star_L_final(eos);
        elseif x_vec(j) >= u_star_final(eos)*t(eos) && x_vec(j) < W_R*t(eos) % Right star state
            u_RG(j) = u_star_final(eos);
            p_RG(j) = p_star_final(eos);
            rho_RG(j) = M(eos)/v_star_R_final(eos);
            T_RG(j) = T_star_R_final(eos);
        elseif x_vec(j) >= W_R*t(eos) % Right constant region
            u_RG(j) = u_R(eos);
            p_RG(j) = p_R(eos);
            rho_RG(j) = M(eos)/v_R(eos);
            T_RG(j) = T_R(eos);
        end
    end
    
    writematrix([x_vec.', u_RG.', p_RG.', rho_RG.', T_RG.'], sprintf('RG_%d.xlsx', eos));
    
    figure; hold on;
    subplot(2,2,1);
    plot(x_vec, p_IG, x_vec, p_RG);
    subplot(2,2,2);
    plot(x_vec, u_IG, x_vec, u_RG);
    subplot(2,2,3);
    plot(x_vec, rho_IG, x_vec, rho_RG);
    subplot(2,2,4);
    plot(x_vec, T_IG, x_vec, T_RG);
end

%% OUTPUTS - COMMAND WINDOW
fprintf("\nNow outputting command window results...\n");

figure; hold on;