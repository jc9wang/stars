function [RHS] = getRHS_DF(W, V, gammaS, e0S, solver_num)
% getRHS_DF Compute RHS vector using the double-flux model. First-order
% reconstruction and HLLC Riemann solver are used.

global Nx dx

% First-order reconstruction - periodic boundaries
fa_L = [V(:,Nx),V(:,1:Nx)];
fa_R = [V(:,1:Nx),V(:,1)];

if solver_num == 0     % HLLC flux
    flux_L = HLLC_DF(fa_L(:,1:Nx),fa_R(:,1:Nx),gammaS,e0S);
    flux_R = HLLC_DF(fa_L(:,2:Nx+1),fa_R(:,2:Nx+1),gammaS,e0S);
elseif solver_num == 1     % SCRS Flux
    flux_L = HLLCStARS_DF(fa_L(:,1:Nx),fa_R(:,1:Nx),gammaS,e0S);
    flux_R = HLLCStARS_DF(fa_L(:,2:Nx+1),fa_R(:,2:Nx+1),gammaS,e0S);
end

% disp(flux_R);
% disp(flux_L);
% Calculate RHS
RHS = -(flux_R-flux_L)/dx;
fprintf("flux_L at NX/2:\t%f\t%f\t%f\n", flux_L(:,Nx/2));
fprintf("flux_L at NX/2 + 1:\t%f\t%f\t%f\n", flux_L(:,Nx/2 + 1));
fprintf("flux_R at NX/2:\t%f\t%f\t%f\n", flux_R(:,Nx/2));
fprintf("flux_R at NX/2 + 1:\t%f\t%f\t%f\n", flux_R(:,Nx/2 + 1));
fprintf("RHS at NX/2:\t%f\t%f\t%f\n", RHS(:,Nx/2));
fprintf("RHS at NX/2 + 1:\t%f\t%f\t%f\n", RHS(:,Nx/2 + 1));
end

