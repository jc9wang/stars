function [e] = getEnergyfromTandRho(T, rho, eos)
% getEnergyfromTandRho Compute specific internal energy given temperature
% and density. NASA polynomial for N2 is used.

MW = 28.0134e-3;
an = [3.531005280E+00,-1.236609870E-04,-5.029994370E-07,2.435306120E-09,...
     -1.408812350E-12,-1.046976280E+03,2.967474680E+00];

[a,b,R,dadT,d2adT2] = getThermo(T, eos);

if eos == 0 %% PR
    delta = 2*b;
    epsilon = -(b^2);   

elseif eos == 1 %% RK
    delta = b;
    epsilon = 0; 
    
elseif eos == 2 %% SRK
    delta = b;
    epsilon = 0;
end

h_ideal = R*(an(1)*T + an(2)*T.^2 + an(3)*T.^3 + an(4)*T.^4 + an(5)*T.^5);
R = 8.314462;
v = 1000*MW./rho;

pre_dPdT = -(2*dadT.*atanh((delta+2*v)./(sqrt(delta^2-4*epsilon))))/(sqrt(delta^2-4*epsilon));
pre_dPdT = real(pre_dPdT);
dPdT = (R*log(v - b)) + pre_dPdT;
pre_VdPdV = (2*a.*atanh((delta+2*v)./(sqrt(delta^2-4*epsilon))))/(sqrt(delta^2-4*epsilon));
pre_VdPdV = real(pre_VdPdV);
VdPdV = ((b*R*T)./(v-b)) - R*T.*log(v-b) + pre_VdPdV - ((a.*v)./(v.^2 + delta*v + epsilon));
delta_h = -T.*dPdT - VdPdV;

p = (R*T)/(v-b) - a./(v.^2 + delta*v + epsilon);
e = h_ideal - delta_h - p.*v;
e = e./MW;

% v = 1./rho;
% K1 = (1/sqrt(8)/b)*log((v+(1-sqrt(2))*b)./(v+(1+sqrt(2))*b)); 
% dep = (a - T.*dadT).*K1;
% 
% e = h_ideal - R*T + dep;

end

