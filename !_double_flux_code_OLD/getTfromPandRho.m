function [T] = getTfromPandRho(p, rho, eos)
%% IPR: add eos func arg to self and called
% getTfromPandRho Compute temperature given pressure and density.

CRIT = 1.0e-6;

v = 1./rho;
T = 300*ones(1,length(rho)); % initial guess; NOTE: WATCH OUT
p_n = getPfromTandRho(T, rho, eos);
diff = p_n - p;

if eos == 0
    while (max(abs(diff)) > CRIT)
        [a,b,R,dadT,d2adT2] = getThermo(T, eos);
        dpdT = (R./(v-b)) - (1./(v.^2+2*v*b-b^2).*dadT);
        T = T - diff./dpdT;
        p_n = getPfromTandRho(T, rho, eos);
        diff = p_n - p;
    end

elseif eos == 1 || eos == 2
    while (max(abs(diff)) > CRIT)
        [a,b,R,dadT,d2adT2] = getThermo(T, eos);
        dpdT = (R./(v-b)) - (1./(v.*(v+b)).*dadT);
        T = T - diff./dpdT;
        p_n = getPfromTandRho(T, rho, eos);
        diff = p_n - p;
    end
end

