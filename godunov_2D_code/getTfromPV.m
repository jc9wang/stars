%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2022-07-24
Description:    Calculates temperature of a pure gas given v, T, some gas properties, and choice of Equation of State (EOS).
%}

function [T_out] = getTfromPV(p, v)
%{
Inputs:         p = matrix of pressures [Pa]
                v = matrix of molar specific volumes [m^3 mol^-1]
Outputs:        T = matrix of temperatures [K]
%}
    global a b delta epsilon omega R T_c type_eos
    crit_conv = 1.0e-6; % convergence criterion

    % Configure EOS
    syms T
    switch type_eos
        case "IG"
            T_out = p.*v./R;
            return;
        case "RK"
            dThetadT = -0.5*a*T.^-1.5;
        case "SRK"
            dThetadT = -a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T./T_c).^0.5)).*(0.48 + ...
                1.574*omega - 0.176*omega^2)./(T.*T_c).^0.5; 
        case "PR"
            dThetadT = -a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T./T_c).^0.5)).*(0.37464 + ...
                1.54226*omega - 0.2699*omega^2)./(T.*T_c).^0.5;
    end

    % Use the Newton-Raphson method to find T numerically
    T_cur = 1;
    p_cur = getPfromVT(v, T_cur);
    diff = p_cur - p;
    
    max_iters = 100;
    while (max(abs(diff), [], 'all') > crit_conv) && max_iters > 1
        dpdT = R./(v - b) - double(subs(dThetadT, T_cur))./(delta.*v + v.^2 + epsilon);
        T_cur = T_cur - diff./dpdT;
        p_cur = getPfromVT(v, T_cur);
        diff = p_cur - p;
        max_iters = max_iters - 1;
    end

    T_out = T_cur;
end