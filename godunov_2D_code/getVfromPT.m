%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-09-17
Description:    Calculates molar volume of a pure gas given p, T, some gas properties, and choice of Equation of State (EOS).
%}

function [v] = getVfromPT(p, T)
%{
Inputs:         p = vector of pressures [Pa]
                T = vector of temperatures [K]
Outputs:        v = vector of molar specific volumes [m^3 kmol^-1] 
%}
    
    global a b delta epsilon omega R T_c type_eos

    switch type_eos
        case "IG"
            Theta = 0;
        case "RK"
            Theta = a*(1./T).^0.5;
        case "SRK"
            Theta = a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T./T_c).^0.5)).^2;
        case "PR"
            Theta = a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T./T_c).^0.5)).^2;
    end

    % Solve using Cardano's formula, taking the solution with only 1 distinct real root
    a3 = p;
    a2 = -R.*T - p.*b + p.*delta;
    a1 = -R.*T.*delta - p.*b.*delta + p.*epsilon + Theta;
    a0 = -R.*T.*epsilon - p.*b.*epsilon - Theta.*b;

    L = a2./a3;
    M = a1./a3;
    N = a0./a3;
    A = (3*M - L.^2)/3;
    B = (2*L.^3 - 9*L.*M + 27*N)/27;
    C = A.^3/27 + B.^2/4;

    P = sign(-B/2 + sqrt(C)).*(abs(-B/2 + sqrt(C))).^(1/3);
    Q = sign(-B/2 - sqrt(C)).*(abs(-B/2 - sqrt(C))).^(1/3);

    v = P + Q - L/3;
end