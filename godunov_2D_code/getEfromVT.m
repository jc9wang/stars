%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2022-07-24
Description:    Computes internal energy for a pure gas given choice of EOS and known variables.
%}

function [e] = getEfromVT(v, T)
%{
Inputs:         v = matrix of molar specific volumes [m^3 kmol^-1]
                T = matrix of temperatures [K]
Outputs:        e = matrix of internal energies [J kmol^-1]
%}

    h = getHfromVT(v, T);
    p = getPfromVT(v, T);
    e = h - p.*v;
end