%{
Author:             Jeremy Wang, University of Waterloo
Last Update:        2022-07-25
Description:        - 1D first-order Godunov scheme with modular thermodynamics and Riemann solver     
                    - Physical quantities generally follow the format: <quantity>_<location/time/conditions>_<calculationmethod>, for example:
                        c = specific heat capacity [J kmol^-1 K^-1]
                        h = enthalpy [J kmol^-1]
                        p = pressure [Pa]
                        rho = density [kg m^-3]
                        sos = speed of sound [m s^-1]
                        T = temperature [K]                      
                        u = speed [m s^-1]
                        v = molar volume [m^3 kmol^-1]
%}

clear; clc; close all;
global A a b delta dx dy epsilon gamma getTfromERho_fit M n_cells_x n_cells_y omega p_c R T_c type_bc type_eos type_riemann

%% Configure the problem
fprintf("Configuring problem...\n");

% Load fluid properties
type_fluid = "N2gas";
filename_properties = 'gas_properties.xlsx';
properties = table2struct(readtable(filename_properties));
idx_gas = find(contains({properties.gas_name}, type_fluid));
A = str2num(properties(idx_gas).c_IG_p_fit_params); % vector of polynomial fit parameters for ideal-gas thermally perfect c_p
gamma = properties(idx_gas).gamma; % ideal-gas specific heat ratio [unitless]
M = properties(idx_gas).M; % molar mass [kg kmol^-1]
omega = properties(idx_gas).omega; % acentric factor [unitless]
p_c = properties(idx_gas).p_c; % critical pressure [Pa]
T_c = properties(idx_gas).T_c; % critical temperature [Pa]
R = 8314.4621; % Universal gas constant [J kmol^-1 K^-1]

% Select the name of the test to simulate
test_name = "J21_RCS32_RCS34_J14";

% Configure EOS and EOS parameters based on Abbott's generic cubic equation (1979)
type_eos = "PR"; % {IG, RK, SRK, PR}
switch type_eos
    case "IG"
        a = 0;
        b = 0;
        delta = 0;
        epsilon = 0;
    case "RK"
        a = 0.4278*R^2*T_c^(2.5)/p_c;
        b = 0.0867*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "SRK"
        a = 0.42747*R^2*T_c^2/p_c;
        b = 0.08664*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "PR"
        a = 0.45724*R^2*T_c^2/p_c;
        b = 0.07780*R*T_c/p_c;
        delta = 2*b;
        epsilon = -b^2;
end

% Configure Riemann solver
type_riemann = "Roe-StARS"; % {exact, HLLC, HLLC-StARS, Roe, Roe-StARS, Roe-Harten}

% Instantiate pressure, density (and molar volume), temperature, flow speed, and simulation time parameters
% --------------------------------------------------------------------------------------------------
[p, rho, v, T, u, w, t_final, CFL, n_cells_x, n_cells_y, dx, dy, x, y, type_bc, xlim_min, xlim_max, ylim_min, ylim_max] = selectTest(test_name);
[X,Y] = meshgrid(x,y);

% Instantiate internal energy, primitives, and conservatives
% --------------------------------------------------------------------------------------------------
e = getEfromVT(v, T);
W_prim(:,:,1) = rho; 
W_prim(:,:,2) = u;
W_prim(:,:,3) = w;
W_prim(:,:,4) = p; % primitives [rho, u, w, p]
W_cons(:,:,1) = rho;
W_cons(:,:,2) = rho.*u;
W_cons(:,:,3) = rho.*w;
W_cons(:,:,4) = e./v + (1/2)*rho.*(u.^2 + w.^2); % conservatives [rho, rho*u, rho*w, total energy per unit volume]

% Save initial conditions
p_0 = p; 
rho_0 = rho; 
v_0 = v; 
T_0 = T; 
u_0 = u; 
w_0 = w; 
e_0 = e; 
E_0 = W_cons(:,:,4);

% Instantiate visualization parameters
% --------------------------------------------------------------------------------------------------
do_show_contour_labels = 0; 
do_surf_override = 0;
idx_viz = 1; % visualization index
idx_viz_int = 1; % visualization interval
line_width = 1;
marker_size = 2;
line_colour = [0 0 1];
line_spec = 'd-';
num_level_steps = 30; % number of level steps in contour plots

% Optional: load non-ideal gas curve fits of T(e, rho) and E(p, rho) for simplicity
% --------------------------------------------------------------------------------------------------
type_solve_getTfromEV = 'fit'; % {'fsolve', 'Newton', 'fit'} - applies to non-ideal EOS only
if type_solve_getTfromEV
    load('getTfromERho_fit_n=3000_fit=poly55.mat', 'getTfromERho_fit');
end

%% Solver
fprintf("Starting solver...\n");
t_sim = 0;

while t_sim < t_final
    fprintf("Progress (t_sim/t_final) = %d %% \n", 100*t_sim/t_final);
    tic

    % Solve for dt
    sos = getSOS(v, T); % speed of sound
    sos_max_x = max([max(u + sos, [], 'all'), max(u - sos, [], 'all')]);
    sos_max_y = max([max(w + sos, [], 'all'), max(w - sos, [], 'all')]);
    dt = min(CFL*dx/sos_max_x, CFL*dy/sos_max_y);
    if (t_sim + dt > t_final) % scale final time step accordingly
        dt = t_final - t_sim;
    end

    % Apply Godunov 1D flux model with forward Euler time marching
    W_cons = W_cons + dt*getFluxes(W_prim, W_cons(:,:,4), T);

    % Update all primitives using the fully updated conservatives
    W_prim(:,:,1) = W_cons(:,:,1);
    W_prim(:,:,2) = W_cons(:,:,2) ./ W_cons(:,:,1);
    W_prim(:,:,3) = W_cons(:,:,3) ./ W_cons(:,:,1);
    rho = W_prim(:,:,1);
    v = M ./ rho;
    u = W_prim(:,:,2);
    w = W_prim(:,:,3);
    e = (W_cons(:,:,4) - (1/2)*rho.*(u.^2 + w.^2)) .* v;
    T = getTfromEV(e, v, type_solve_getTfromEV);
    p = getPfromVT(v, T);
    W_prim(:,:,4) = p;

    t(idx_viz) = t_sim;
    
    % Visualize results at "idx_step" intervals of "interval_viz"
    if (mod(idx_viz, idx_viz_int) == 0 || t_sim + dt >= t_final)
        figure(1), set(gcf, 'Position', [0 0 1500 500]);
           
        subplot(2,3,1);
        if ~do_surf_override
            contour(X, Y, p./p_c, ...
                'ShowText', do_show_contour_labels, ...
                'LevelStepMode', 'manual', ...
                'LevelStep', (max(p./p_c, [], 'all') - min(p./p_c, [], 'all'))/num_level_steps); 
        else
            surf(X, Y, p./p_c,'LineStyle','none');
        end
        xlabel('x [m]');
        ylabel('y [m]');
        xlim([xlim_min, xlim_max]);
        ylim([xlim_min, xlim_max]);
        c = colorbar();
        c.Label.String = 'p_r';
        
        subplot(2,3,2);
        if ~do_surf_override
            contour(X, Y, rho, ...
                'ShowText', do_show_contour_labels, ...
                'LevelStepMode', 'manual', ...
                'LevelStep', (max(rho, [], 'all') - min(rho, [], 'all'))/num_level_steps); 
        else
            surf(X, Y, rho,'LineStyle','none');
        end
        xlabel('x [m]');
        ylabel('y [m]');
        xlim([xlim_min, xlim_max]);
        ylim([xlim_min, xlim_max]);
        c = colorbar();
        c.Label.String = '\rho [kg/m^3]';

        subplot(2,3,3);
        if ~do_surf_override
            contour(X, Y, T./T_c, ...
                'ShowText', do_show_contour_labels, ...
                'LevelStepMode', 'manual', ...
                'LevelStep', (max(T./T_c, [], 'all') - min(T./T_c, [], 'all'))/num_level_steps); 
        else
            surf(X, Y, T./T_c,'LineStyle','none');
        end
        xlabel('x [m]');
        ylabel('y [m]');
        xlim([xlim_min, xlim_max]);
        ylim([xlim_min, xlim_max]);
        c = colorbar();
        c.Label.String = 'T_r';

        subplot(2,3,4);
        if ~do_surf_override
            contour(X, Y, u, ...
                'ShowText', do_show_contour_labels, ...
                'LevelStepMode', 'manual', ...
                'LevelStep', (max(u, [], 'all') - min(u, [], 'all'))/num_level_steps); 
        else
            surf(X, Y, u,'LineStyle','none');
        end
        xlabel('x [m]');
        ylabel('y [m]');
        xlim([xlim_min, xlim_max]);
        ylim([xlim_min, xlim_max]);
        c = colorbar();
        c.Label.String = 'u [m/s]';

        subplot(2,3,5);
        if ~do_surf_override
            contour(X, Y, w, ...
                'ShowText', do_show_contour_labels, ...
                'LevelStepMode', 'manual', ...
                'LevelStep', (max(w, [], 'all') - min(w, [], 'all'))/num_level_steps); 
        else
            surf(X, Y, w,'LineStyle','none');
        end
        xlabel('x [m]');
        ylabel('y [m]');
        xlim([xlim_min, xlim_max]);
        ylim([xlim_min, xlim_max]);
        c = colorbar();
        c.Label.String = 'w [m/s]';

        subplot(2,3,6);
        if ~do_surf_override
            contour(X, Y, e./M, ...
                'ShowText', do_show_contour_labels, ...
                'LevelStepMode', 'manual', ...
                'LevelStep', (max(e./M, [], 'all') - min(e./M, [], 'all'))/num_level_steps); 
        else
            surf(X, Y, e./M,'LineStyle','none');
        end
        xlabel('x [m]');
        ylabel('y [m]');
        xlim([xlim_min, xlim_max]);
        ylim([xlim_min, xlim_max]);
        c = colorbar();
        c.Label.String = 'e [J/kg]';
    end

    % Update time and step index
    t_sim = t_sim + dt;
    idx_viz = idx_viz + 1;
    toc
end

%% Save results
out_file = sprintf("%s_%s_%s_t=%f_ny=%d_nx=%d.xlsx", test_name, type_eos, type_riemann, t_final, n_cells_y, n_cells_x);

writematrix(X, out_file, 'Sheet', "X", 'Range', 'A1');
writematrix(Y, out_file, 'Sheet', "Y", 'Range', 'A1');
writematrix(Y, out_file, 'Sheet', "Y", 'Range', 'A1');
writematrix(p/p_c, out_file, 'Sheet', "p_r", 'Range', 'A1');
writematrix(rho, out_file, 'Sheet', "rho", 'Range', 'A1');
writematrix(T/T_c, out_file, 'Sheet', "T_r", 'Range', 'A1');
writematrix(u, out_file, 'Sheet', "u", 'Range', 'A1');
writematrix(w, out_file, 'Sheet', "w", 'Range', 'A1');
writematrix(e/M, out_file, 'Sheet', "e", 'Range', 'A1');
