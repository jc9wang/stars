%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2022-07-25
Description:    Calculates intercell fluxes using the dimensionally split operator approach
%}

function fluxes = getFluxes(W_prim, E, T)
%{
Inputs:         W_prim = 3D matrix (n_cells_y x n_cells_x x 4) of primitive variables [kg m^-3; m s^-2; m s^-2; Pa]
                E = 2D matrix (n_cells_y x n_cells_x) of volume-specific total energies [J m^-3]
                T = 2D matrix (n_cells_y x n_cells_x) of temperatures [K]
Outputs:        fluxes = matrix (n_cells_y x n_cells_x) of intercell fluxes [kg m^-3 s^-1 ; kg m^-2 s^-2, J m^-2 s^-2] 
%}
    global dx dy n_cells_x n_cells_y type_bc type_riemann 

    switch type_bc % first-order reconstruction
        case "transparent" % boundaries transmit waves, i.e. far-field conditions - see Toro (2009) §6.3.3
            W_prim_x_1 = [W_prim(:,1,:), W_prim(:,1:n_cells_x,:)];
            W_prim_x_2 = [W_prim(:,1:n_cells_x,:), W_prim(:,n_cells_x,:)];
            W_prim_y_1 = [W_prim(1,:,:); W_prim(1:n_cells_y,:,:)];
            W_prim_y_2 = [W_prim(1:n_cells_y,:,:); W_prim(n_cells_y,:,:)];
            E_x_1 = [E(:,1), E(:,1:n_cells_x)];
            E_x_2 = [E(:,1:n_cells_x), E(:,n_cells_x)];
            E_y_1 = [E(1,:); E(1:n_cells_y,:)];
            E_y_2 = [E(1:n_cells_y,:); E(n_cells_y,:)];
            T_x_1 = [T(:,1), T(:,1:n_cells_x)];
            T_x_2 = [T(:,1:n_cells_x), T(:,n_cells_x)];
            T_y_1 = [T(1,:); T(1:n_cells_y,:)];
            T_y_2 = [T(1:n_cells_y,:); T(n_cells_y,:)];
        case "reflective" % boundaries reflect, i.e. wall - see Toro (2009) §6.3.3
            W_prim_x_1 = [W_prim(:,1,:), W_prim(:,1:n_cells_x,:)];
            W_prim_x_1(:,1,2) = -W_prim_x_1(:,1,2);
            W_prim_x_2 = [W_prim(:,1:n_cells_x,:), W_prim(:,n_cells_x,:)];
            W_prim_x_2(:,end,2) = -W_prim_x_2(:,end,2);
            W_prim_y_1 = [W_prim(1,:,:); W_prim(1:n_cells_y,:,:)];
            W_prim_y_1(1,:,3) = -W_prim_y_1(1,:,3);
            W_prim_y_2 = [W_prim(1:n_cells_y,:,:); W_prim(n_cells_y,:,:)];
            W_prim_y_2(end,:,3) = -W_prim_y_2(end,:,3);  
            E_x_1 = [E(:,1), E(:,1:n_cells_x)];
            E_x_2 = [E(:,1:n_cells_x), E(:,n_cells_x)];
            E_y_1 = [E(1,:); E(1:n_cells_y,:)];
            E_y_2 = [E(1:n_cells_y,:); E(n_cells_y,:)];
            T_x_1 = [T(:,1), T(:,1:n_cells_x)];
            T_x_2 = [T(:,1:n_cells_x), T(:,n_cells_x)];
            T_y_1 = [T(1,:); T(1:n_cells_y,:)];
            T_y_2 = [T(1:n_cells_y,:); T(n_cells_y,:)];
        case "periodic" % boundary fluxes pass over from one end to the other
            W_prim_x_1 = [W_prim(:,n_cells_x,:), W_prim(:,1:n_cells_x,:)];
            W_prim_x_2 = [W_prim(:,1:n_cells_x,:), W_prim(:,1,:)];
            W_prim_y_1 = [W_prim(n_cells_y,:,:); W_prim(1:n_cells_y,:,:)];
            W_prim_y_2 = [W_prim(1:n_cells_y,:,:); W_prim(1,:,:)];
            E_x_1 = [E(:,n_cells_x), E(:,1:n_cells_x)];
            E_x_2 = [E(:,1:n_cells_x), E(:,1)];
            E_y_1 = [E(n_cells_y,:); E(1:n_cells_y, :)];
            E_y_2 = [E(1:n_cells_y, :); E(1,:);];
            T_x_1 = [T(:,n_cells_x), T(:,1:n_cells_x)];
            T_x_2 = [T(:,1:n_cells_x), T(:,1)];
            T_y_1 = [T(n_cells_y,:); T(1:n_cells_y, :)];
            T_y_2 = [T(1:n_cells_y, :); T(1,:);];
        otherwise
            error('Error: type_bc was invalid. Please confirm your selection is valid.');
    end

    % Rearrange y-axis variables to re-use the Roe solver w/ no modifications
    W_prim_y_1(:,:,[2,3]) = W_prim_y_1(:,:,[3,2]);
    W_prim_y_1 = permute(W_prim_y_1, [2,1,3]);
    W_prim_y_2(:,:,[2,3]) = W_prim_y_2(:,:,[3,2]);
    W_prim_y_2 = permute(W_prim_y_2, [2,1,3]);
    E_y_1 = permute(E_y_1, [2,1]);
    E_y_2 = permute(E_y_2, [2,1]);
    T_y_1 = permute(T_y_1, [2,1]);
    T_y_2 = permute(T_y_2, [2,1]);

    switch type_riemann % solve with the Riemann solver
        case "Roe"
            flux_x_1 = Roe(W_prim_x_1(:,1:n_cells_x,:), W_prim_x_2(:,1:n_cells_x,:), E_x_1(:,1:n_cells_x), E_x_2(:,1:n_cells_x), T_x_1(:,1:n_cells_x), T_x_2(:,1:n_cells_x));
            flux_x_2 = Roe(W_prim_x_1(:,2:n_cells_x+1,:), W_prim_x_2(:,2:n_cells_x+1,:), E_x_1(:,2:n_cells_x+1), E_x_2(:,2:n_cells_x+1), T_x_1(:,2:n_cells_x+1), T_x_2(:,2:n_cells_x+1));
            flux_y_1 = Roe(W_prim_y_1(:,1:n_cells_y,:), W_prim_y_2(:,1:n_cells_y,:), E_y_1(:,1:n_cells_y), E_y_2(:,1:n_cells_y), T_y_1(:,1:n_cells_y), T_y_2(:,1:n_cells_y));
            flux_y_2 = Roe(W_prim_y_1(:,2:n_cells_y+1,:), W_prim_y_2(:,2:n_cells_y+1,:), E_y_1(:,2:n_cells_y+1), E_y_2(:,2:n_cells_y+1), T_y_1(:,2:n_cells_y+1), T_y_2(:,2:n_cells_y+1));
        case "Roe-StARS"
            flux_x_1 = Roe_StARS(W_prim_x_1(:,1:n_cells_x,:), W_prim_x_2(:,1:n_cells_x,:), E_x_1(:,1:n_cells_x), E_x_2(:,1:n_cells_x), T_x_1(:,1:n_cells_x), T_x_2(:,1:n_cells_x));
            flux_x_2 = Roe_StARS(W_prim_x_1(:,2:n_cells_x+1,:), W_prim_x_2(:,2:n_cells_x+1,:), E_x_1(:,2:n_cells_x+1), E_x_2(:,2:n_cells_x+1), T_x_1(:,2:n_cells_x+1), T_x_2(:,2:n_cells_x+1));
            flux_y_1 = Roe_StARS(W_prim_y_1(:,1:n_cells_y,:), W_prim_y_2(:,1:n_cells_y,:), E_y_1(:,1:n_cells_y), E_y_2(:,1:n_cells_y), T_y_1(:,1:n_cells_y), T_y_2(:,1:n_cells_y));
            flux_y_2 = Roe_StARS(W_prim_y_1(:,2:n_cells_y+1,:), W_prim_y_2(:,2:n_cells_y+1,:), E_y_1(:,2:n_cells_y+1), E_y_2(:,2:n_cells_y+1), T_y_1(:,2:n_cells_y+1), T_y_2(:,2:n_cells_y+1));
        case "Roe-Harten"
            flux_x_1 = Roe_Harten(W_prim_x_1(:,1:n_cells_x,:), W_prim_x_2(:,1:n_cells_x,:), E_x_1(:,1:n_cells_x), E_x_2(:,1:n_cells_x), T_x_1(:,1:n_cells_x), T_x_2(:,1:n_cells_x));
            flux_x_2 = Roe_Harten(W_prim_x_1(:,2:n_cells_x+1,:), W_prim_x_2(:,2:n_cells_x+1,:), E_x_1(:,2:n_cells_x+1), E_x_2(:,2:n_cells_x+1), T_x_1(:,2:n_cells_x+1), T_x_2(:,2:n_cells_x+1));
            flux_y_1 = Roe_Harten(W_prim_y_1(:,1:n_cells_y,:), W_prim_y_2(:,1:n_cells_y,:), E_y_1(:,1:n_cells_y), E_y_2(:,1:n_cells_y), T_y_1(:,1:n_cells_y), T_y_2(:,1:n_cells_y));
            flux_y_2 = Roe_Harten(W_prim_y_1(:,2:n_cells_y+1,:), W_prim_y_2(:,2:n_cells_y+1,:), E_y_1(:,2:n_cells_y+1), E_y_2(:,2:n_cells_y+1), T_y_1(:,2:n_cells_y+1), T_y_2(:,2:n_cells_y+1));
    end

    % Switch flux dimensions and components back to the correct order for the scheme
    flux_y_1(:,:,[2,3]) = flux_y_1(:,:,[3,2]); 
    flux_y_1 = permute(flux_y_1, [2,1,3]);
    flux_y_2(:,:,[2,3]) = flux_y_2(:,:,[3,2]);
    flux_y_2 = permute(flux_y_2, [2,1,3]);

    fluxes = -(flux_x_2 - flux_x_1)/dx - (flux_y_2 - flux_y_1)/dy;
end