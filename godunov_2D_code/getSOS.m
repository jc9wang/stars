%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2022-07-24
Description:    Computes speed of sound for a pure gas given choice of EOS and known variables.
%}

function [sos] = getSOS(v, T)
%{
Inputs:         v = matrix of molar specific volumes [m^3 mol^-1]
                T = matrix of temperatures [K]
Outputs:        sos = matrix of speeds of sound [m s^-1]
%}
    global A a b delta epsilon M omega R T_c type_eos

    % Configure EOS
    switch type_eos
        case "IG"
            Theta = 0;
            dThetadT = 0;
            d2ThetadT2 = 0;
        case "RK"
            Theta = a*(1./T).^0.5;
            dThetadT = -0.5*a*T.^-1.5;
            d2ThetadT2 = -0.25*a./(T.^2.5) + a./(T.^2.5);
        case "SRK"
            Theta = a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T./T_c).^0.5)).^2;
            dThetadT = -a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T./T_c).^0.5)).*(0.48 + ...
                1.574*omega - 0.176*omega^2)./(T.*T_c).^0.5; 
            d2ThetadT2 = 0.5*a*(0.48 + 1.574*omega - 0.176*omega^2)^2./(T.*T_c) + ...
                0.5*a*(1 + (0.48 + 1.574*omega - 0.176*omega^2).*(1 - (T./T_c).^0.5)).*(0.48 + 1.574*omega - 0.176*omega^2)./(T.^1.5.*T_c.^0.5);
        case "PR"
            Theta = a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T./T_c).^0.5)).^2;
            dThetadT = -a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T./T_c).^0.5)).*(0.37464 + ...
                1.54226*omega - 0.2699*omega^2)./(T.*T_c).^0.5;
            d2ThetadT2 = 0.5*a*(0.37464 + 1.54226*omega - 0.2699*omega^2)^2./(T.*T_c) + ...
                0.5*a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2).*(1 - (T./T_c).^0.5)).*(0.37464 + 1.54226*omega - 0.2699*omega^2)./(T.^1.5.*T_c.^0.5);
    end
    
    % Calculate the ideal-gas thermally perfect c_p and c_v using polynomial approximation
    syms x
    for i = 1:length(A) 
        T_mat(i,1) = x^(i-1); 
    end
    c_p_IG_TP = double(subs(R*(A*T_mat), T));
    c_v_IG_TP = c_p_IG_TP - R;

    % Calculate real-gas c_p and c_v
    dpdv = -R*T./(v - b).^2 + Theta.*(2*v + delta)./(delta*v + v.^2 + epsilon).^2;
    dpdT = R./(v - b) - dThetadT./(delta.*v + v.^2 + epsilon);
    if type_eos == "IG"
        intd2pdT2 = 0;
    else
        intd2pdT2 = real(2*d2ThetadT2.*atanh((2.*v + delta)./(delta^2 - 4*epsilon).^0.5)./(delta^2 - 4*epsilon).^0.5);
    end
    c_v = c_v_IG_TP + T.*intd2pdT2;
    c_p = c_v - T.*dpdT.^2./dpdv;

    % Calculate speed of sound
    sos = (-v.^2.*c_p.*dpdv./(M.*c_v)).^0.5;
end