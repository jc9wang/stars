%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2022-07-25
Description:    Calculates the Roe flux with the expansion wave restored exactly for real gases
%}

function flux_Roe_StARS = Roe_StARS(W_prim_L, W_prim_R, E_L, E_R, T_L, T_R)
%{
Inputs:         W_prim = 3D matrix (n_cells_y x n_cells_x x 4) of primitive variables [kg m^-3; m s^-2; m s^-2; Pa]
                E = 2D matrix of volume-specific total energies [J m^-3]
                T = 2D matrix of temperatures [K]
Outputs:        fluxes = 3D matrix (n_cells_y x n_cells_x x 4) matrix of intercell fluxes [kg m^-3 s^-1 ; kg m^-2 s^-2; kg m^-2 s^-2; J m^-2 s^-2] 
%}
    global M 

    % Left region "L" conditions
    rho_L = W_prim_L(:,:,1);
    v_L = M ./ rho_L;
    u_L = W_prim_L(:,:,2); % u_L is whichever velocity parallel to the direction of the current sweep
    w_L = W_prim_L(:,:,3); % w_L is whichever velocity perpendicular to the direction the current sweep
    p_L = W_prim_L(:,:,4);
    H_L = (E_L + p_L)./rho_L;
    sos_L = getSOS(v_L, T_L);
    flux_L(:,:,1) = rho_L.*u_L;   
    flux_L(:,:,2) = rho_L.*(u_L).^2 + p_L;
    flux_L(:,:,3) = rho_L.*u_L.*w_L;
    flux_L(:,:,4) = u_L.*(E_L + p_L);

    % Right region "R" conditions
    rho_R = W_prim_R(:,:,1);
    v_R = M ./ rho_R;
    u_R = W_prim_R(:,:,2);
    w_R = W_prim_R(:,:,3);
    p_R = W_prim_R(:,:,4);
    H_R = (E_R + p_R)./rho_R;
    sos_R = getSOS(v_R, T_R);
    flux_R(:,:,1) = rho_R.*u_R; 
    flux_R(:,:,2) = rho_R.*(u_R).^2 + p_R; 
    flux_R(:,:,3) = rho_R.*u_R.*w_R; 
    flux_R(:,:,4) = u_R.*(E_R + p_R);

    % Compute Roe averages
    rho_avg = sqrt(rho_L).*sqrt(rho_R);
    u_avg = (sqrt(rho_L).*u_L + sqrt(rho_R).*u_R)./(sqrt(rho_L) + sqrt(rho_R));
    w_avg = (sqrt(rho_L).*w_L + sqrt(rho_R).*w_R)./(sqrt(rho_L) + sqrt(rho_R));
    H_avg = (sqrt(rho_L).*H_L + sqrt(rho_R).*H_R)./(sqrt(rho_L) + sqrt(rho_R));
    p_avg = (sqrt(rho_L).*p_L + sqrt(rho_R).*p_R)./(sqrt(rho_L) + sqrt(rho_R));
    v_avg = (sqrt(rho_L).*v_L + sqrt(rho_R).*v_R)./(sqrt(rho_L) + sqrt(rho_R));
    T_avg = (sqrt(rho_L).*T_L + sqrt(rho_R).*T_R)./(sqrt(rho_L) + sqrt(rho_R));
    sos_avg = getSOS(v_avg, T_avg);

    flux_Roe_StARS = zeros(length(W_prim_L(:,1,1)), length(W_prim_L(1,:,1)), 4); % n_cells_y x n_cells_x x 4
    for i = 1:length(flux_Roe_StARS(:,1,1)) % i corresponds to y
        for j = 1:length(flux_Roe_StARS(1,:,1)) % j corresponds to x
            % Compute the wavespeeds / eigenvalues
            lambda = abs([u_avg(i,j) - sos_avg(i,j); 
                u_avg(i,j);
                u_avg(i,j);
                u_avg(i,j) + sos_avg(i,j)]);
        
            % Compute the right eigenvectors
            K = [1, 1, 0, 1; 
                u_avg(i,j) - sos_avg(i,j), u_avg(i,j), 0, u_avg(i,j) + sos_avg(i,j); 
                w_avg(i,j), w_avg(i,j), 1, w_avg(i,j);
                H_avg(i,j) - u_avg(i,j)*sos_avg(i,j), u_avg(i,j)^2/2, w_avg(i,j), H_avg(i,j) + u_avg(i,j)*sos_avg(i,j)];
        
            % Compute the differences in primitive variables
            d_rho = rho_R(i,j) - rho_L(i,j);
            d_u = u_R(i,j) - u_L(i,j);
            d_w = w_R(i,j) - w_L(i,j);
            d_p = p_R(i,j) - p_L(i,j);
            
            % Compute the wave strengths
            dV = [(d_p - rho_avg(i,j)*sos_avg(i,j)*d_u)/(2*sos_avg(i,j)^2);
                -(d_p/sos_avg(i,j)^2 - d_rho); 
                rho_avg(i,j)*d_w;
                (d_p + rho_avg(i,j)*sos_avg(i,j)*d_u)/(2*sos_avg(i,j)^2)];
    
            % Compute the Roe flux
            flux_Roe_StARS(i,j,:) = (squeeze(flux_L(i,j,:) + flux_R(i,j,:)) - K*(lambda.*dV))/2;

            D_lambda = max([0, u_avg(i,j) - sos_avg(i,j) - (u_L(i,j) - sos_L(i,j))]); % entropy violation bounding term, see Harten & Hyman Eqs. 2.14a and A.10b
            if (lambda(1) < D_lambda) % Left expansion wave - see Wang & Hickey (2020)
                A_2_LEW = -0.5*log(v_L(i,j).*v_avg(i,j)) + 0.5*log(v_L(i,j)./v_avg(i,j)).*(u_L(i,j) - sos_L(i,j) + u_avg(i,j) - sos_avg(i,j))./(u_L(i,j) - sos_L(i,j) - u_avg(i,j) + sos_avg(i,j));
                v_LEW = exp(-A_2_LEW);        
                rho_LEW = M./v_LEW;
                A_4_LEW = -0.5*log(p_L(i,j).*p_avg(i,j)) + 0.5*log(p_L(i,j)./p_avg(i,j)).*(u_L(i,j) - sos_L(i,j) + u_avg(i,j) - sos_avg(i,j))./(u_L(i,j) - sos_L(i,j) - u_avg(i,j) + sos_avg(i,j));
                p_LEW = exp(-A_4_LEW);
                A_5_LEW = 0.5*(u_L(i,j) + u_avg(i,j)) - 0.5*(u_L(i,j) - sos_L(i,j) + u_avg(i,j) - sos_avg(i,j)).*(u_L(i,j) - u_avg(i,j))./(u_L(i,j) - sos_L(i,j) - u_avg(i,j) + sos_avg(i,j));
                u_LEW = A_5_LEW;
                T_LEW = getTfromPV(p_LEW, v_LEW);
                e_LEW = getEfromVT(v_LEW, T_LEW);
    
                flux_Roe_StARS(i,j,:) = [rho_LEW.*u_LEW; 
                    rho_LEW.*u_LEW.^2 + p_LEW;
                    flux_Roe_StARS(i,j,3);
                    u_LEW.*(e_LEW./v_LEW + (1/2)*rho_LEW.*(u_LEW.^2 + w_avg(i,j).^2) + p_LEW)];
                fprintf("LEW...\n");
                continue;
            end
            D_lambda = max([0, u_R(i) + sos_R(i) - (u_avg(i) + sos_avg(i))]); % entropy violation bounding term, see Harten & Hyman Eqs. 2.14a and A.10b
            if (lambda(4) < D_lambda) % Right expansion wave 
                A_2_REW = -0.5*log(v_R(i,j).*v_avg(i,j)) + 0.5*log(v_R(i,j)./v_avg(i,j)).*(u_R(i,j) + sos_R(i,j) + u_avg(i,j) + sos_avg(i,j))./(u_R(i,j) + sos_R(i,j) - u_avg(i,j) - sos_avg(i,j));
                v_REW = exp(-A_2_REW);
                rho_REW = M./v_REW;
                A_4_REW = -0.5*log(p_R(i,j).*p_avg(i,j)) + 0.5*log(p_R(i,j)./p_avg(i,j)).*(u_R(i,j) + sos_R(i,j) + u_avg(i,j) + sos_avg(i,j))./(u_R(i,j) + sos_R(i,j) - u_avg(i,j) - sos_avg(i,j));
                p_REW = exp(-A_4_REW);
                A_5_REW = 0.5*(u_R(i,j) + u_avg(i,j)) - 0.5*(u_R(i,j) + sos_R(i,j) + u_avg(i,j) + sos_avg(i,j)).*(u_R(i,j) - u_avg(i,j))./(u_R(i,j) + sos_R(i,j) - u_avg(i,j) - sos_avg(i,j));
                u_REW = A_5_REW;
                T_REW = getTfromPV(p_REW, v_REW);
                e_REW = getEfromVT(v_REW, T_REW);
                
                flux_Roe_StARS(i,j,:) = [rho_REW.*u_REW;
                    rho_REW.*u_REW.^2 + p_REW;
                    flux_Roe_StARS(i,j,3);
                    u_REW.*(e_REW./v_REW + (1/2)*rho_REW.*(u_REW.^2 + w_avg(i,j).^2) + p_REW)];
                fprintf("REW...\n");
                continue;
            end
        end
    end
end