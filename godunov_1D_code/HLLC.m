%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-09-22
Description:    Calculates the HLLC flux
%}

function flux_HLLC = HLLC(W_prim_L, W_prim_R, E_L, E_R, T_L, T_R)
%{
Inputs:         W_prim = 3 x n_cells matrix of primitive variables [kg m^-3; m s^-2; Pa]
                E = vector of volume-specific total energies [J m^-3]
                T = vector of temperatures [K]
Outputs:        flux = vector of intercell fluxes [kg m^-3 s^-1 ; kg m^-2 s^-2, J m^-2 s^-2] 
%}
    global M 
    
    % Left region "L" conditions
    rho_L = W_prim_L(1,:);
    v_L = M ./ rho_L;
    u_L = W_prim_L(2,:);
    p_L = W_prim_L(3,:);
    sos_L = getSOS(v_L, T_L);
    s_L = u_L - sos_L;
    W_cons_L = [rho_L; rho_L.*u_L; E_L];
    flux_L = [rho_L.*u_L; rho_L.*(u_L).^2 + p_L; u_L.*(E_L + p_L)];

    % Right region "R" conditions
    rho_R = W_prim_R(1,:);
    v_R = M ./ rho_R;
    u_R = W_prim_R(2,:);
    p_R = W_prim_R(3,:);
    sos_R = getSOS(v_R, T_R);
    s_R = u_R + sos_R;
    W_cons_R = [rho_R; rho_R.*u_R; E_R];
    flux_R = [rho_R.*u_R; rho_R.*(u_R).^2 + p_R; u_R.*(E_R + p_R)];

    % Star region "S" conditions
    s_S = (p_L - p_R - rho_L.*u_L.*(s_L-u_L) + rho_R.*u_R.*(s_R-u_R))./(rho_R.*(s_R-u_R)-rho_L.*(s_L-u_L));
    rho_avg = 0.5*(rho_L + rho_R);
    sos_avg = 0.5*(sos_L + sos_R);
    p_pvrs = 0.5*(p_L + p_R) - 0.5*(u_R - u_L).*rho_avg.*sos_avg;
    p_S = max(0, p_pvrs);
    % p_S = rho_R.*(u_R - s_R).*(u_R - s_S) + p_R;

    % Left-star "SL" conditions
    rho_sL = rho_L.*(s_L - u_L)./(s_L - s_S);
    v_sL = M./rho_sL;
    T_sL = getTfromPV(p_S, v_sL);
    sos_sL = getSOS(v_sL, T_sL);
    W_cons_SL = [(s_L-u_L)./(s_L-s_S).*rho_L;
           ((s_L-u_L).*rho_L.*u_L + (p_S-p_L))./(s_L-s_S);
           ((s_L-u_L).*E_L - p_L.*u_L + p_S.*s_S)./(s_L-s_S)];

    % Right-star "SR" conditions
    rho_sR = rho_R.*(s_R - u_R)./(s_R - s_S);
    v_sR = M./rho_sR;
    T_sR = getTfromPV(p_S, v_sR);
    sos_sR = getSOS(v_sR, T_sR);
    W_cons_SR = [(s_R-u_R)./(s_R-s_S).*rho_R;
           ((s_R-u_R).*rho_R.*u_R + (p_S-p_R))./(s_R-s_S);
           ((s_R-u_R).*E_R - p_R.*u_R + p_S.*s_S)./(s_R-s_S)];
    
    % Check wavespeeds to select correct flux
    flux_HLLC = zeros(3, length(W_prim_L));
    for i=1:length(W_prim_L)
        if s_L(i) >= 0
            flux_HLLC(:,i) = flux_L(:,i);
        elseif s_L(i) < 0 && s_S(i) >= 0
            flux_HLLC(:,i) = flux_L(:,i) + s_L(i)*(W_cons_SL(:,i) - W_cons_L(:,i));
        elseif s_S(i) < 0 && s_R(i) >= 0
            flux_HLLC(:,i) = flux_R(:,i) + s_R(i)*(W_cons_SR(:,i) - W_cons_R(:,i));
        else
            flux_HLLC(:,i) = flux_R(:,i);
        end
    end
end