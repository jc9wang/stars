%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-09-17
Description:    Calculates density of a pure gas given p, T, some gas properties, and choice of Equation of State (EOS).
%}

function rho = getRhofromPT(p, T)
%{
Inputs:         p = vector of pressures [Pa]
                T = vector of temperatures [K]
Outputs:        rho = vector of densities [kg m^-3] 
%}
    global M
    
    v = getVfromPT(p, T);
    rho = M ./ v;
end