%{
Author:             Jeremy Wang, University of Waterloo
Last Update:        2021-12-19
Description:        - Creates a fit of T from e and rho. See godunov_1D_code > main.m for details on notation and units.
%}

clear; clc; close all;
global A a b delta epsilon gamma getTfromERho_fit M omega p_c R T_c type_eos

% Load fluid properties
type_fluid = "N2gas";
filename_properties = 'gas_properties.xlsx';
properties = table2struct(readtable(filename_properties));
idx_gas = find(contains({properties.gas_name}, type_fluid));
A = str2num(properties(idx_gas).c_IG_p_fit_params); % vector of polynomial fit parameters for ideal-gas thermally perfect c_p
gamma = properties(idx_gas).gamma; % ideal-gas specific heat ratio [unitless]
do_create_energy_fits = false; % create (true) or load (false) the T(e,rho) and E(p,rho) function
M = properties(idx_gas).M; % molar mass [kg kmol^-1]
omega = properties(idx_gas).omega; % acentric factor [unitless]
p_c = properties(idx_gas).p_c; % critical pressure [Pa]
T_c = properties(idx_gas).T_c; % critical temperature [Pa]
R = 8314.4621; % Universal gas constant [J kmol^-1 K^-1]

% Configure EOS and EOS parameters based on Abbott's generic cubic equation (1979)
type_eos = "PR"; % {IG, RK, SRK, PR}
switch type_eos
    case "IG"
        a = 0;
        b = 0;
        delta = 0;
        epsilon = 0;
    case "RK"
        a = 0.4278*R^2*T_c^(2.5)/p_c;
        b = 0.0867*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "SRK"
        a = 0.42747*R^2*T_c^2/p_c;
        b = 0.08664*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "PR"
        a = 0.45724*R^2*T_c^2/p_c;
        b = 0.07780*R*T_c/p_c;
        delta = 2*b;
        epsilon = -b^2;
end

% Create curve fits of T(e, rho) for simplicity
% --------------------------------------------------------------------------------------------------
n_fit = 3000;
fit_type = 'poly55';
% For N2, PR, poly55 and n_fit=3000: max residual is 0.2456 K, rmse is <= 0.0451 K

rho_fit_min = 5; 
rho_fit_max = 800;
rho_fit = rho_fit_min:(rho_fit_max - rho_fit_min)/n_fit:rho_fit_max;
T_fit_min = 30; 
T_fit_max = 500;    
T_fit = T_fit_min:(T_fit_max - T_fit_min)/n_fit:T_fit_max;

for m = 1:length(rho_fit)
    for n = 1:length(T_fit)
        rho_fit_mat(m, n) = rho_fit(m);
        v_fit_mat(m, n) = M/rho_fit(m);
        T_fit_mat(m, n) = T_fit(n);
        p_fit_mat(m, n) = getPfromVT(v_fit_mat(m, n), T_fit_mat(m, n));
        e_fit_mat(m, n) = getEfromVT(v_fit_mat(m, n), T_fit_mat(m, n));
    end
    fprintf("m = %d\n", m);
end

rho_fit_reshaped = reshape(rho_fit_mat, [], 1);
v_fit_reshaped = reshape(v_fit_mat, [], 1);
T_fit_reshaped = reshape(T_fit_mat, [], 1);
p_fit_reshaped = reshape(p_fit_mat, [], 1);
e_fit_reshaped = reshape(e_fit_mat, [], 1);

[getTfromERho_fit, fit_T_e_rho_gof, fit_T_e_rho_outputs] = fit([e_fit_reshaped, rho_fit_reshaped], T_fit_reshaped, fit_type);
plot(getTfromERho_fit, [e_fit_reshaped rho_fit_reshaped], T_fit_reshaped);
save(sprintf('getTfromERho_fit_n=%d_fit=%s.mat', n_fit, fit_type), 'getTfromERho_fit', 'fit_T_e_rho_gof', 'fit_T_e_rho_outputs');
% save(sprintf('e_fit_reshaped_n=%d.mat', n_fit), 'e_fit_reshaped');
% save(sprintf('rho_fit_reshaped_n=%d.mat', n_fit), 'rho_fit_reshaped');
% save(sprintf('T_fit_reshaped_n=%d.mat', n_fit), 'T_fit_reshaped');
