%{
Author:             Jeremy Wang, University of Waterloo
Last Update:        2021-12-19
Description:        - 1D first-order Godunov scheme with modular thermodynamics and Riemann solver     
                    - Physical quantities generally follow the format: <quantity>_<location/time/conditions>_<calculationmethod>, for example:
                        c = specific heat capacity [J kmol^-1 K^-1]
                        h = enthalpy [J kmol^-1]
                        p = pressure [Pa]
                        rho = density [kg m^-3]
                        sos = speed of sound [m s^-1]
                        T = temperature [K]                      
                        u = speed [m s^-1]
                        v = molar volume [m^3 kmol^-1]
%}

clear; clc; close all;
global A a b delta dx epsilon gamma getTfromERho_fit M n_cells omega p_c R T_c type_bc type_eos type_riemann

%% Configure the problem
fprintf("Configuring problem...\n");

% Load fluid properties
type_fluid = "N2gas";
filename_properties = 'gas_properties.xlsx';
properties = table2struct(readtable(filename_properties));
idx_gas = find(contains({properties.gas_name}, type_fluid));
A = str2num(properties(idx_gas).c_IG_p_fit_params); % vector of polynomial fit parameters for ideal-gas thermally perfect c_p
gamma = properties(idx_gas).gamma; % ideal-gas specific heat ratio [unitless]
M = properties(idx_gas).M; % molar mass [kg kmol^-1]
omega = properties(idx_gas).omega; % acentric factor [unitless]
p_c = properties(idx_gas).p_c; % critical pressure [Pa]
T_c = properties(idx_gas).T_c; % critical temperature [Pa]
R = 8314.4621; % Universal gas constant [J kmol^-1 K^-1]

% Select the name of the test to simulate
test_name = "travel_super";

% Configure EOS and EOS parameters based on Abbott's generic cubic equation (1979)
type_eos = "PR"; % {IG, RK, SRK, PR}
switch type_eos
    case "IG"
        a = 0;
        b = 0;
        delta = 0;
        epsilon = 0;
    case "RK"
        a = 0.4278*R^2*T_c^(2.5)/p_c;
        b = 0.0867*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "SRK"
        a = 0.42747*R^2*T_c^2/p_c;
        b = 0.08664*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "PR"
        a = 0.45724*R^2*T_c^2/p_c;
        b = 0.07780*R*T_c/p_c;
        delta = 2*b;
        epsilon = -b^2;
end

% Configure Riemann solver
type_riemann = "Roe"; % {exact, HLLC, HLLC-StARS, Roe, Roe-StARS, Roe-Harten}

% Instantiate pressure, density (and molar volume), temperature, flow speed, and simulation time parameters
% --------------------------------------------------------------------------------------------------
[p, rho, v, T, u, t_final, CFL, n_cells, dx, x, type_bc, xlim_min, xlim_max] = selectTest(test_name);

% Instantiate internal energy, primitives, and conservatives
% --------------------------------------------------------------------------------------------------
e = getEfromVT(v, T);
W_prim = [rho; u; p]; % primitives [rho, u, p]
W_cons = [rho; rho.*u; e./v + (1/2)*rho.*u.^2]; % conservatives [rho, rho*u, total energy per unit volume]
p_0 = p; rho_0 = rho; v_0 = v; T_0 = T; u_0 = u; Ma_0 = u./getSOS(v,T); e_0 = e; E_0 = W_cons(3,:); % save initial conditions
% E_0_dx = E_0*(dx*ones(n_cells,1)); % save initial total energy for measuring energy conservation error

% Instantiate visualization parameters
% --------------------------------------------------------------------------------------------------
idx_viz = 1; % visualization index
idx_viz_int = 1; % visualization interval
line_width = 1;
marker_size = 2;
line_colour = [0 0 1];
line_spec = 'd-';

% Optional: load non-ideal gas curve fits of T(e, rho) and E(p, rho) for simplicity
% --------------------------------------------------------------------------------------------------
type_solve_getTfromEV = 'fit'; % {'fsolve', 'Newton', 'fit'} - applies to non-ideal EOS only
if type_solve_getTfromEV
    load('getTfromERho_fit_n=3000_fit=poly55.mat', 'getTfromERho_fit');
end

%% Solver
fprintf("Starting solver...\n");
t_sim = 0;

while t_sim < t_final
    fprintf("Progress (t_sim/t_final) = %d %% \n", 100*t_sim/t_final);
    tic

    % Solve for dt
    sos = getSOS(v, T); % speed of sound
    dt = CFL*dx/max([u + sos, u - sos]);
    if (t_sim + dt > t_final) % scale final time step accordingly
        dt = t_final - t_sim;
    end

%     % OPTION 1: Apply Godunov 1D flux model with three-stage SSP-RK3 time marching
%     k_1 = W_cons + dt*getFlux(W_prim, W_cons(3,:), T);
%     W_prim_k_1(1,:) = k_1(1,:);
%     W_prim_k_1(2,:) = k_1(2,:) ./ k_1(1,:);
%     v_k_1 = M ./ W_prim_k_1(1,:);
%     e_k_1 = (k_1(3,:) - (1/2)*W_prim_k_1(1,:).*W_prim_k_1(2,:).^2) .* v_k_1;
%     T_k_1 = getTfromEV(e_k_1, v_k_1, type_solve_getTfromEV);
%     W_prim_k_1(3,:) = getPfromVT(v_k_1, T_k_1);
%     
%     k_2 = 3/4*W_cons + 1/4*k_1 + 1/4*dt*getFlux(W_prim_k_1, k_1(3,:), T_k_1);
%     W_prim_k_2(1,:) = k_2(1,:);
%     W_prim_k_2(2,:) = k_2(2,:) ./ k_2(1,:);
%     v_k_2 = M ./ W_prim_k_2(1,:);
%     e_k_2 = (k_2(3,:) - (1/2)*W_prim_k_2(1,:).*W_prim_k_2(2,:).^2) .* v_k_2;
%     T_k_2 = getTfromEV(e_k_2, v_k_2, type_solve_getTfromEV);
%     W_prim_k_2(3,:) = getPfromVT(v_k_2, T_k_2);
% 
%     W_cons = 1/3*W_cons + 2/3*k_2 + 2/3*dt*getFlux(W_prim_k_2, k_2(3,:), T_k_2);

    % OPTION 2: Apply Godunov 1D flux model with forward Euler time marching
    W_cons = W_cons + dt*getFlux(W_prim, W_cons(3,:), T);
    
    % Update all primitives using the fully updated conservatives
    W_prim(1,:) = W_cons(1,:);
    W_prim(2,:) = W_cons(2,:) ./ W_cons(1,:);
    rho = W_prim(1,:);
    v = M ./ rho;
    u = W_prim(2,:);
    e = (W_cons(3,:) - (1/2)*rho.*u.^2) .* v;
    T = getTfromEV(e, v, type_solve_getTfromEV);
    p = getPfromVT(v, T);
    W_prim(3,:) = p;

    % Update energy conservation error
%     E_dx_pct(idx_viz) = abs( (W_cons(3,:)*(dx*ones(n_cells,1)) - E_0_dx)/E_0_dx);
    t(idx_viz) = t_sim;
    
    % Visualize results at "idx_step" intervals of "interval_viz"
    if (mod(idx_viz, idx_viz_int) == 0 || t_sim + dt >= t_final)
        figure(1), set(gcf, 'Position', [0 0 1500 500]);
           
        subplot(2,3,1);
        plot(x, p_0/p_c, 'k', 'LineWidth', line_width); 
        hold on;
        plot(x, p/p_c, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
        hold off;
        ylabel('p_r', 'FontSize', 15);
        xlim([xlim_min, xlim_max]);
        
        subplot(2,3,2);
        plot(x, rho_0, 'k', 'LineWidth', line_width); 
        hold on;
        plot(x, rho, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
        hold off;
        ylabel('\rho [kg/m^3]', 'FontSize', 15);
        xlim([xlim_min, xlim_max]);

        subplot(2,3,3);
        plot(x, T_0/T_c, 'k', 'LineWidth', line_width); 
        hold on;
        plot(x, T/T_c, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
        hold off;
        ylabel('T_r', 'FontSize', 15);
        xlim([xlim_min, xlim_max]);

        subplot(2,3,4);
        plot(x, u_0, 'k', 'LineWidth', line_width);
        hold on;
        plot(x, u, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size);
        hold off;
        ylabel('u [m/s]', 'FontSize', 15);
        xlabel('x [m]', 'FontSize', 15);
        xlim([xlim_min, xlim_max]);

        subplot(2,3,5);
        plot(x, Ma_0, 'k', 'LineWidth', line_width);
        hold on;
        plot(x, u./sos, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
        hold off;
        ylabel('Ma', 'FontSize', 15);
        xlabel('x [m]', 'FontSize', 15);
        xlim([xlim_min, xlim_max]);

        subplot(2,3,6);
        plot(x, e_0./M, 'k', 'LineWidth', line_width); 
        hold on;
        plot(x, e./M, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
        hold off;
        ylabel('e [J/kg]', 'FontSize', 15);
        xlabel('x [m]', 'FontSize', 15);
        xlim([xlim_min, xlim_max]);

%         figure(2), set(gcf, 'Position', [0 500 500 500]);
%         semilogy(t, E_dx_pct);
    end

    % Update time and step index
    t_sim = t_sim + dt;
    idx_viz = idx_viz + 1;
    toc
end

%% Save results

out_file = sprintf("%s_%s_%s_t=%f_n=%d.xlsx", test_name, type_eos, type_riemann, t_final, n_cells);

writematrix("x", out_file, 'Range', 'A1');
writematrix(x.', out_file, 'Range', 'A2');

writematrix("p_r", out_file, 'Range', 'B1');
writematrix((p/p_c).', out_file, 'Range', 'B2');

writematrix("rho", out_file, 'Range', 'C1');
writematrix(rho.', out_file, 'Range', 'C2');

writematrix("T_r", out_file, 'Range', 'D1');
writematrix((T/T_c).', out_file, 'Range', 'D2');

writematrix("u", out_file, 'Range', 'E1');
writematrix(u.', out_file, 'Range', 'E2');

writematrix("Ma", out_file, 'Range', 'F1');
writematrix((u./sos).', out_file, 'Range', 'F2');

writematrix("e", out_file, 'Range', 'G1');
writematrix((e./M).', out_file, 'Range', 'G2');
