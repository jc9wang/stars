%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-11-22
Description:    Chooses the test case and configs to run
%}

function [p, rho, v, T, u, t_final, CFL, n_cells, dx, x, type_bc, xlim_min, xlim_max] = selectTest(name_test)
%{
Inputs:         name_test = string representing test name
Outputs:        vectors of initial conditions
                grid and other configuration parameters
%}

global M

switch name_test
    case "advec"
        %% Advection test from Ma et al. (2017) - for spurious oscillations
        CFL = 0.8;
        x_min = 0;
        x_max = 1;
        n_cells = 128;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2;
        type_bc = "transparent";
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.01;
        
        p = [5e6*ones(1, n_cells)];
        T = [300*ones(1, n_cells/4), 100*ones(1, n_cells/2), 300*ones(1, n_cells/4)];
        rho = [getRhofromPT(p, T)];
        v = M./rho;
        u = [0*ones(1, n_cells/4), 0*ones(1, n_cells/2), 0*ones(1, n_cells/4)];

    case "double_rare_1"
        %% Double rarefaction test - with expansion shocks
        CFL = 0.8;
        x_min = 0;
        x_max = 0.5;
        n_cells = 128;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2;
        type_bc = "periodic";
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.0006;
        
        p = [1.1e7*ones(1, n_cells/4), 2e5*ones(1, n_cells/2), 1.1e7*ones(1, n_cells/4)];
        rho = [290*ones(1, n_cells/4), 7.4*ones(1, n_cells/2), 290*ones(1, n_cells/4)];
        v = M./rho;
        T = [getTfromPV(p, v)];
        u = [50*ones(1, n_cells/4), 50*ones(1, n_cells/2), 50*ones(1, n_cells/4)];

    case "double_rare_2"
        %% Einfeldt's double rarefaction test
        CFL = 0.5;
        x_min = 0;
        x_max = 1;
        n_cells = 100;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2;
        type_bc = "transparent";
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.15;
        
        p = [0.4*ones(1, n_cells)];
        rho = [1*ones(1, n_cells)];
        v = M./rho;
        T = [getTfromPV(p, v)];
        u = [-2*ones(1, n_cells/2), 2*ones(1, n_cells/2)];


    case "blast"
        %% Blast wave test based on Woodward and Colella (1984) - for entropy production
        CFL = 0.9;
        x_min = -1;
        x_max = 1;
        n_cells = 150;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2;
        type_bc = "reflective"; 
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.009;

        p = [1.1e7*ones(1, n_cells/10), 1e4*ones(1, n_cells*8/10), 1.1e6*ones(1, n_cells/10)]; 
        rho = [200*ones(1, n_cells)];
        v = M./rho;
        T = [getTfromPV(p, v)]; 
        u = [0*ones(1, n_cells)];

    case "grad"
        %% Gradient test - for entropy violations in the limit as gradients approach the Riemann problem initial conditions
        CFL = 0.5;
        x_min = -0.5;
        x_max = 0.5;
        n_cells = 128;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2;
        type_bc = "transparent"; 
        xlim_min = -0.4;
        xlim_max = 0.4;
        t_final = 0.0005;
        
        dx_mid = 7*dx; % go from 1 dx to 7 dx in increments of 2 dx
        p_max = 1.1e7;
        p_min = 2e5;
        p = max(min([(p_max - p_min)/(dx_mid)*(-x).*ones(1,n_cells) + (p_max + p_min)/2], p_max), p_min);
        rho_max = 180;
        rho_min = 7.4;
        rho = max(min([(rho_max - rho_min)/(dx_mid)*(-x).*ones(1,n_cells) + (rho_max + rho_min)/2], rho_max), rho_min);
        v = M./rho;
        T = [getTfromPV(p, v)];
        u_max = 150;
        u_min = 150;
        u = max(min([(u_max - u_min)/(dx_mid)*(-x).*ones(1,n_cells) + (u_max + u_min)/2], u_max), u_min);

    case "periodic"
        %% Periodic flow test - for expansion shocks and effects on frequency decomposition
        CFL = 0.9;
        x_min = -1;
        x_max = 1;
        n_cells = 200;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
        type_bc = "periodic"; 
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.001;
        
        p = [5.5e6*sin(10*pi*x).*ones(1, n_cells) + 5.6e6];
        rho = [145*cos(10*pi*x).*ones(1, n_cells) + 152.4];
        v = M./rho;
        T = [getTfromPV(p, v)];
        u = [0*sin(40*pi*x).*ones(1, n_cells) + 50];

    case "trans_rcs"
        %% Transcritical RCS shock tube test - for expansion shocks
        CFL = 0.5;
        x_min = -1;
        x_max = 1;
        n_cells = 256; % 128
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
        type_bc = "transparent"; 
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.0009;

        p = [1.1e7*ones(1, n_cells/2), 2e5*ones(1, n_cells/2)]; 
        rho = [180*ones(1, n_cells/2), 7.4*ones(1, n_cells/2)];
        v = M./rho;
        T = [getTfromPV(p, v)]; 
        u = [150*ones(1, n_cells/2), 50*ones(1, n_cells/2)];

    case "trans_rcs_2"
        %% Transcritical RCS shock tube test w/ periodic bounds - for expansion shocks
        CFL = 0.5;
        x_min = -1;
        x_max = 1;
        n_cells = 256;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
        type_bc = "periodic";
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.0075;

        p = [1.1e7*ones(1, n_cells/2), 2e5*ones(1, n_cells/2)]; 
        rho = [290*ones(1, n_cells/2), 7.4*ones(1, n_cells/2)];
        v = M./rho;
        T = [getTfromPV(p, v)]; 
        u = [150*ones(1, n_cells/2), 50*ones(1, n_cells/2)];

    case "trans_scr"
        %% Transcritical SCR shock tube test - sanity check that expansion shocks occur in both directions
        CFL = 0.9;
        x_min = -1;
        x_max = 1;
        n_cells = 128;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
        type_bc = "transparent"; 
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.0015;

        p = [2e5*ones(1, n_cells/2), 1.1e7*ones(1, n_cells/2)]; 
        rho = [7.4*ones(1, n_cells/2), 290*ones(1, n_cells/2)];
        v = M./rho;
        T = [getTfromPV(p, v)]; 
        u = [50*ones(1, n_cells/2), 150*ones(1, n_cells/2)];

    case "travel_super"
        %% Travelling supersonic wave test - for entropy violations in high gradients
        CFL = 0.9;
        x_min = -1;
        x_max = 1;
        n_cells = 128;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
        type_bc = "reflective"; 
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.01;

        p = [5.5e6*sin(2*pi*x(1:end/2)).*ones(1, n_cells/2) + 5.6e6, + 5.6e6*ones(1, n_cells/2)]; 
        rho = [145*sin(2*pi*x(1:end/2)).*ones(1, n_cells/2) + 152.4, 152.4*ones(1, n_cells/2)];
        v = M./rho;
        T = [getTfromPV(p, v)]; 
        u = [50*sin(-2*pi*x(1:end/2)).*ones(1, n_cells/2), 0*ones(1, n_cells/2)];
    
    case "travel_sub"
        %% Travelling subsonic wave test - confirm no entropy violations in subsonic flow
        CFL = 0.9;
        x_min = -1;
        x_max = 1;
        n_cells = 32;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
        type_bc = "reflective"; 
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 10;

        p = [5.6e6*ones(1, n_cells)]; 
        rho = [152.4*ones(1, n_cells)];
        v = M./rho;
        T = [getTfromPV(p, v)]; 
        u = [50*sin(2*pi*x(1:end/8)).*ones(1, n_cells/8), 0*ones(1, n_cells*7/8)];

    case "uniform" 
        %% Uniform flow test - confirm flow variables remain constant with time
        CFL = 0.9;
        x_min = -1;
        x_max = 1;
        n_cells = 128;
        dx = (x_max - x_min)/n_cells;
        x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
        type_bc = "transparent"; 
        xlim_min = -inf;
        xlim_max = inf;
        t_final = 0.0015;
       
        p = [11000000*ones(1, n_cells)];
        rho = [M/0.0966*ones(1, n_cells)];
        v = M./rho;
        T = [getTfromPV(p(1), v(1))*ones(1, n_cells)];
        u = zeros(1, n_cells);
end