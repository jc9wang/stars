%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-09-23
Description:    Calculates the HLLC flux with the expansion wave restored exactly for real gases
%}

function [flux_HLLC] = HLLC_StARS(W_prim_L, W_prim_R, E_L, E_R, T_L, T_R)
%{
Inputs:         W_prim = 3 x n_cells matrix of primitive variables [kg m^-3; m s^-2; Pa]
                E = vector of volume-specific total energies [J m^-3]
                T = vector of temperatures [K]
Outputs:        flux = vector of intercell fluxes [kg m^-3 s^-1 ; kg m^-2 s^-2, J m^-2 s^-2] 
%}
    global M 

    % Left region "L" conditions
    rho_L = W_prim_L(1,:);
    v_L = M ./ rho_L;
    u_L = W_prim_L(2,:);
    p_L = W_prim_L(3,:);
    sos_L = getSOS(v_L, T_L);
    s_L = u_L - sos_L;
    W_cons_L = [rho_L; rho_L.*u_L; E_L];
    flux_L = [rho_L.*u_L; rho_L.*(u_L).^2 + p_L; u_L.*(E_L + p_L)];

    % Right region "R" conditions
    rho_R = W_prim_R(1,:);
    v_R = M ./ rho_R;
    u_R = W_prim_R(2,:);
    p_R = W_prim_R(3,:);
    sos_R = getSOS(v_R, T_R);
    s_R = u_R + sos_R;
    W_cons_R = [rho_R; rho_R.*u_R; E_R];
    flux_R = [rho_R.*u_R; rho_R.*(u_R).^2 + p_R; u_R.*(E_R + p_R)];

    % Star region "S" conditions
    s_S = (p_L - p_R - rho_L.*u_L.*(s_L-u_L) + rho_R.*u_R.*(s_R-u_R))./(rho_R.*(s_R-u_R)-rho_L.*(s_L-u_L));
    rho_avg = 0.5*(rho_L + rho_R);
    sos_avg = 0.5*(sos_L + sos_R);
    p_pvrs = 0.5*(p_L + p_R) - 0.5*(u_R - u_L).*rho_avg.*sos_avg;
    p_S = max(0, p_pvrs);
    % p_S = rho_R.*(u_R - s_R).*(u_R - s_S) + p_R;

    % Left-star "SL" conditions
    rho_sL = rho_L.*(s_L - u_L)./(s_L - s_S);
    v_sL = M./rho_sL;
    T_sL = getTfromPV(p_S, v_sL);
    sos_sL = getSOS(v_sL, T_sL);
    W_cons_SL = [(s_L-u_L)./(s_L-s_S).*rho_L;
           ((s_L-u_L).*rho_L.*u_L + (p_S-p_L))./(s_L-s_S);
           ((s_L-u_L).*E_L - p_L.*u_L + p_S.*s_S)./(s_L-s_S)];

    % Right-star "SR" conditions
    rho_sR = rho_R.*(s_R - u_R)./(s_R - s_S);
    v_sR = M./rho_sR;
    T_sR = getTfromPV(p_S, v_sR);
    sos_sR = getSOS(v_sR, T_sR);
    W_cons_SR = [(s_R-u_R)./(s_R-s_S).*rho_R;
           ((s_R-u_R).*rho_R.*u_R + (p_S-p_R))./(s_R-s_S);
           ((s_R-u_R).*E_R - p_R.*u_R + p_S.*s_S)./(s_R-s_S)];

    % Left expansion wave "LEW" conditions - applicable only if p_L(i) > p_R(i)
    A_2_LEW = -0.5*log(v_L.*v_sL) + 0.5*log(v_L./v_sL).*(u_L - sos_L + s_S - sos_sL)./(u_L - sos_L - s_S + sos_sL);
    v_LEW = exp(-A_2_LEW);        
    rho_LEW = M./v_LEW;
    A_4_LEW = -0.5*log(p_L.*p_S) + 0.5*log(p_L./p_S).*(u_L - sos_L + s_S - sos_sL)./(u_L - sos_L - s_S + sos_sL);
    p_LEW = exp(-A_4_LEW);
    A_5_LEW = 0.5*(u_L + s_S) - 0.5*(u_L - sos_L + s_S - sos_sL).*(u_L - s_S)./(u_L - sos_L - s_S + sos_sL);
    u_LEW = A_5_LEW;
    T_LEW = getTfromPV(p_LEW, v_LEW);
    e_LEW = getEfromVT(v_LEW, T_LEW);

    % Right expansion wave "REW" conditions - applicable only if p_L(i) < p_R(i)
    A_2_REW = -0.5*log(v_R.*v_sR) + 0.5*log(v_R./v_sR).*(u_R + sos_R + s_S + sos_sR)./(u_R + sos_R - s_S - sos_sR);
    v_REW = exp(-A_2_REW);
    rho_REW = M./v_REW;
    A_4_REW = -0.5*log(p_R.*p_S) + 0.5*log(p_R./p_S).*(u_R + sos_R + s_S + sos_sR)./(u_R + sos_R - s_S - sos_sR);
    p_REW = exp(-A_4_REW);
    A_5_REW = 0.5*(u_R + s_S) - 0.5*(u_R + sos_R + s_S + sos_sR).*(u_R - s_S)./(u_R + sos_R - s_S - sos_sR);
    u_REW = A_5_REW;
    T_REW = getTfromPV(p_REW, v_REW);
    e_REW = getEfromVT(v_REW, T_REW);

    % Check wavespeeds to select correct flux
    flux_HLLC = zeros(3, length(W_prim_L));
    for i=1:length(W_prim_L)
        if s_L(i) >= 0 % Left
            flux_HLLC(:,i) = flux_L(:,i);
        elseif p_L(i) > p_R(i) && u_L(i) - sos_L(i) < 0 && s_S(i) - sos_sL(i) >= 0 % Left expansion wave
            flux_HLLC(:,i) = [rho_LEW(i).*u_LEW(i); 
                rho_LEW(i).*u_LEW(i).^2 + p_LEW(i); 
                u_LEW(i).*(e_LEW(i)./v_LEW(i) + (1/2)*rho_LEW(i).*u_LEW(i).^2 + p_LEW(i))];
        elseif s_S(i) - sos_sL(i) < 0 && s_S(i) >= 0 % Left star
            flux_HLLC(:,i) = flux_L(:,i) + s_L(i)*(W_cons_SL(:,i) - W_cons_L(:,i));
        elseif s_S(i) < 0 && s_R(i) + sos_sR(i) >= 0 % Right star
            flux_HLLC(:,i) = flux_R(:,i) + s_R(i)*(W_cons_SR(:,i) - W_cons_R(:,i));
        elseif p_L(i) < p_R(i) && s_S(i) + sos_sR(i) < 0 && u_R(i) + sos_R(i) >= 0 % Right expansion wave
            flux_HLLC(:,i) = [rho_REW(i).*u_REW;
                rho_REW(i).*u_REW(i).^2 + p_REW(i);
                u_REW(i).*(e_REW(i)./v_REW(i) + (1/2)*rho_REW(i).*u_REW(i).^2 + p_REW)];
        else % Right
            flux_HLLC(:,i) = flux_R(:,i);
        end
    end
end