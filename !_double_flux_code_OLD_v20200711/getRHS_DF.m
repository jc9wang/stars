function [RHS] = getRHS_DF(W, V, gammaS, e0S, solver_num)
%% IPR: tentatively no changes
% getRHS_DF Compute RHS vector using the double-flux model. First-order
% reconstruction and HLLC Riemann solver are used.

global Nx dx

% First-order reconstruction
fa_L = [V(:,Nx),V(:,1:Nx)];
fa_R = [V(:,1:Nx),V(:,1)];

if solver_num == 0     % HLLC flux
    flux_L = HLLC_DF(fa_L(:,1:Nx),fa_R(:,1:Nx),gammaS,e0S);
    flux_R = HLLC_DF(fa_L(:,2:Nx+1),fa_R(:,2:Nx+1),gammaS,e0S);
elseif solver_num == 1     % HLLE Flux
    flux_L = HLLE_DF(fa_L(:,1:Nx),fa_R(:,1:Nx),gammaS,e0S);
    flux_R = HLLE_DF(fa_L(:,2:Nx+1),fa_R(:,2:Nx+1),gammaS,e0S);
end

% disp(flux_R);
% disp(flux_L);
% Calculate RHS
RHS = -(flux_R-flux_L)/dx;
[RHS(1,1),RHS(2,1),RHS(3,1)]
[RHS(1,end),RHS(2,end),RHS(3,end)]
end

