function HLLE = HLLE_DF(V_L, V_R, gammaS, e0S, L_R)
% Compute HLLE flux

sign = @(s) (s>=0) - (s<=0);

% Left state
rho_L = V_L(1,:);
u_L = V_L(2,:);
p_L = V_L(3,:);
a_L = sqrt(gammaS.*p_L./rho_L);
E_L = p_L./(gammaS-1) + rho_L.*e0S + 0.5*rho_L.*u_L.^2; % orig: E_L = qL(3)./rho_L;H_L = E_L + p_L;
H_L = E_L + p_L;

% Right state
rho_R = V_R(1,:);
u_R = V_R(2,:);
p_R = V_R(3,:);
a_R = sqrt(gammaS.*p_R./rho_R);
E_R = p_R./(gammaS-1) + rho_R.*e0S + 0.5*rho_R.*u_R.^2; % orig: V_R(3)./rho_R;
H_R = E_R + p_R;

% Evaluate the two wave speeds: Einfeldt.
RT = sqrt(rho_R./rho_L);
H = (H_L+RT.*H_R)./(1+RT);
u = (u_L+RT.*u_R)./(1+RT);
a = sqrt((gammaS-1).*(H-u.*u./2));

% Wave speed estimates
S_L = min(u_L-a_L, u-a);
S_R = max(u_R+a_R, u+a);

W_L = [rho_L; rho_L.*u_L; E_L];
W_R = [rho_R; rho_R.*u_R; E_R];

% Left and Right fluxes
f_L=[rho_L.*u_L; rho_L.*u_L.^2+p_L; u_L.*(E_L+p_L)]; % Here? Remove rho_L?
f_R=[rho_R.*u_R; rho_R.*u_R.^2+p_R; u_R.*(E_R+p_R)]; % ^^

% Compute the HLL flux.
if S_L(1) >= 0  % Right-going supersonic flow
    HLLE = f_L;
elseif (S_L(1) <= 0) && (S_R(1) >= 0) % Subsonic flow
    HLLE = ( S_R.*f_L - S_L.*f_R + S_L.*S_R.*(V_R-V_L) )./(S_R-S_L);
elseif  S_R(1) <= 0 % Left-going supersonic flow
    HLLE = f_R;
end

