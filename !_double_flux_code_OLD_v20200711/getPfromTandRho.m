function [p] = getPfromTandRho(T, rho, eos)
%% IPR: hardcode RK, SRK, maybe more methods
% getPfromTandRho Compute pressure given temperature and density

[a, b, R, dadT, d2adT2] = getThermo(T, eos);
v = 1./rho;

if eos == 0
    p = (R*T)./(v-b) - a./(v.^2+2*v*b-b^2); %% Peng-Robinson
elseif eos == 1 || eos == 2
    p = (R*T)./(v-b) - a./(v.*(v+b)); %% Redlich-Kwong & SRK

end

