function [ ] = plotSolnInit( W,V,x,t,conservatives0,primitives0, residuals, eos )
% plotSolnInit Plot solutions along with initial conditions.

fprintf('plotting solution at time t = %f ...\n',t);

% get initial conditions
rho0 = primitives0(1,:);
u0 = primitives0(2,:);
p0 = primitives0(3,:);
E0 = conservatives0(3,:);
e0 = (E0 - 0.5*rho0.*u0.^2)./rho0;
T0 = getTfromPandRho(p0, rho0, eos);

% get solutions
rho = V(1,:);
u = V(2,:);
p = V(3,:);
E = W(3,:);
e = (E - 0.5*rho.*u.^2)./rho;
T = getTfromPandRho(p, rho, eos);

% get residuals
rho_res = residuals(1, 1:end-1);
u_res = residuals(2, 1:end-1); 
p_res = residuals(3, 1:end-1);
E_res = residuals(4, 1:end-1);
e_res = residuals(5, 1:end-1);
T_res = residuals(6, 1:end-1);

% plot
lw = 1;

figure(1), set(gcf, 'Position', [0 0 768 784]);

subplot(3,2,1), plot(x, u, 'o-', x, u0, 'k', 'LineWidth', lw);
ylabel('u [m/s]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
h_legend = legend('Solution', 'Initial Condition', 'Location', 'Best');
set(h_legend, 'FontSize', 12, 'Location', 'NorthWest');

subplot(3,2,2), plot(x, p/1e6, 'o-', x, p0/1e6, 'k', 'LineWidth', lw);
ylabel('p [MPa]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);

subplot(3,2,3), plot(x, rho, 'o-', x,rho0, 'k', 'LineWidth', lw);
ylabel('\rho [kg/m^3]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);

subplot(3,2,4), plot(x, T, 'o-', x, T0, 'k', 'LineWidth', lw);
ylabel('T [K]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);

subplot(3,2,5), plot(x, E, 'o-', x, E0, 'k', 'LineWidth', lw);
ylabel('E [J/m^3]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);

subplot(3,2,6), plot(x, e, 'o-', x, e0, 'k', 'LineWidth', lw);
ylabel('e [J/kg]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);

its = [2:length(residuals)];
figure(2), set(gcf, 'Position', [768 0 768 784]);

subplot(3,2,1), semilogy(its, u_res, 'o-', 'LineWidth', lw);
ylabel('U Residual at x = 0.5m');
xlabel('Iterations');

subplot(3,2,2), semilogy(its, p_res, 'o-', 'LineWidth', lw);
ylabel('P Residual at x = 0.5m');
xlabel('Iterations');

subplot(3,2,3), semilogy(its, rho_res, 'o-', 'LineWidth', lw);
ylabel('Rho Residual at x = 0.5m');
xlabel('Iterations');

subplot(3,2,4), semilogy(its, T_res, 'o-', 'LineWidth', lw);
ylabel('T Residual at x = 0.5m');
xlabel('Iterations');

subplot(3,2,5), semilogy(its, E_res, 'o-', 'LineWidth', lw);
ylabel('E Residual at x = 0.5m'); % ?
xlabel('Iterations');

subplot(3,2,6), semilogy(its, e_res, 'o-', 'LineWidth', lw);
ylabel('e Residual at x = 0.5m');
xlabel('Iterations');

end

