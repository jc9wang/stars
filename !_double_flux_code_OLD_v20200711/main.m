%% Init

clear all, close all, clc;

global Nx dx x dt eos solver

% CFL number & EOS choice
CFL = 0.8; %DANIEL CHANGE: was 1

eos = 2;
solver = 0; % 0 = HLLC, 1 = HLLE

% Grids (periodic)
a = -1;
b = 2;
Nx = 2048;
dx = (b-a)/Nx;
x = a+dx/2:dx:b-dx/2; % cell center location
% 
% % Initial conditions (sharp bump)
% eta = 1e-10;
% T_L = 300;
% T_R = 100;
% T0 = T_L+(T_R-T_L)/2.0*(tanh((x-0.25)/eta)+tanh(-(x-0.75)/eta));
% u0 = 200*ones(1,Nx);
% p0 = 50e5*ones(1,Nx);
% rho0 = getRhofromTandP(T0, p0, eos);
% % disp(rho0)

% Initial conditions (shock tube)
eta = 1e-10;
T_L = 350;
T_R = 273;
rhoL=800;
rhoR=80;
pL=60e6;
pR=6e6;
T0 = ones(1,Nx);
u0 = 0*ones(1,Nx);
p0 = ones(1,Nx);
rho0 = ones(1,Nx);
%filling matrix
p0(1,1:Nx/2) = pL;
p0(1,Nx/2+1:Nx) = pR;
rho0(1,1:Nx/2) = rhoL;
rho0(1,Nx/2+1:Nx) = rhoR;
T0(1,1:Nx/2) = T_L;
T0(1,Nx/2+1:Nx) = T_R;

primitives0 = [rho0;u0;p0]; % [rho, u, p]
conservatives0 = primitivesToConservatives(primitives0, eos); % [rho, rho*u, E]
W = conservatives0;
V = primitives0;
printInfo(W,V,0,0,0); % print information
% plotSolnInit(W,V,x,0,conservatives0,primitives0); % plot solution

% Simulation time
% t_final = b/u0(1); % simulation end time = one period
t_final = 0.0005; % simulation end time = one period... DANIEL CHANGE: was b/u0(1)
%t_final = 0.0009;
t_sim = 0; % simulation time
step = 0; % time step
CHECK = 100; % check interval

%% Solve

poi = Nx/2;
residuals = zeros(6, 1);
while (t_sim < t_final)

    %%%%%%%%%%%%%%%%
    % Calculate dt %
    %%%%%%%%%%%%%%%%

    sos = getSos(V, eos); % speed of sound
    u = V(2,:);
    dt = CFL*dx/max(max(max(u,u+sos),u-sos));
    if (t_sim + dt > t_final) 
        dt = t_final - t_sim; 
    end % last time step dt
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Time Advancement (Algorithm 1) %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Store gammaS and e0S
    [gammaS, e0S] = getGammaSandE0S(V, eos);

    % Time marching with double-flux model (RK3)
    k1 = W + dt*getRHS_DF(W, V, gammaS, e0S, solver);
    Vk1 = updatePrimitives_DF(k1, gammaS, e0S);
    
    k2 = 3/4*W + 1/4*k1 + 1/4*dt*getRHS_DF(k1,Vk1,gammaS,e0S, solver);
    Vk2 = updatePrimitives_DF(k2, gammaS, e0S);
    
    W = 1/3*W + 2/3*k2 + 2/3*dt*getRHS_DF(k2, Vk2, gammaS, e0S, solver);
    V = updatePrimitives_DF(W, gammaS, e0S);
    
    % Update conservatives
    W = primitivesToConservatives(V, eos);
    e = (W(3,:) - 0.5*V(1,:).*u.^2)./V(1,:);
    T = getTfromPandRho(V(3,:), V(1,:), eos);
    
    %%%%%%%%%%%%%%%%%%
    % Check Solution %
    %%%%%%%%%%%%%%%%%%
    
    % Update simulation time and step
    t_sim = t_sim + dt;
    step = step + 1;
    
    % Compute residual for Xn - X(n-1);
    
    if step == 1
        residuals(1, step) = V(1, poi);
        residuals(2, step) = V(2, poi);
        residuals(3, step) = V(3, poi);
        residuals(4, step) = W(3, poi);
        residuals(5, step) = e(poi);
        residuals(6, step) = T(poi);
    else
        residuals(1, step) = V(1, poi);
        residuals(1, step-1) = residuals(1, step-1) - V(1, poi);
        residuals(2, step) = V(2, poi);
        residuals(2, step-1) = residuals(2, step-1) - V(2, poi);
        residuals(3, step) = V(3, poi);
        residuals(3, step-1) = residuals(3, step-1) - V(3, poi);
        residuals(4, step) = W(3, poi);
        residuals(4, step-1) = residuals(4, step-1) - W(3, poi);
        residuals(5, step) = e(poi);
        residuals(5, step-1) = residuals(5, step-1) - e(poi);
        residuals(6, step) = T(poi);
        residuals(6, step-1) = residuals(6, step-1) - T(poi);
    end
    
    % Print information
    if (mod(step,CHECK) == 0 || t_sim >= t_final)
        printInfo(W,V,step,t_sim,dt);
    end
    % disp(t_sim)
                
end

%% Plot

% plotSolnInit(W,V,x,t_final,conservatives0,primitives0, eos);
plotSolnInit(W(:,400:800),V(:,400:800),x(400:800),t_final,conservatives0(:,400:800),primitives0(:,400:800), residuals, eos);
