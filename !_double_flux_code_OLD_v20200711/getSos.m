function [sos] = getSos(V, eos)
%% IPR: looks like replace derivatives with general forms and add eos arg to func
% getSos Compute speed of sound given primitive variables. NASA polynomial
% for N2 is used.

% nasa polynomial for N2
an = [3.531005280E+00,-1.236609870E-04,-5.029994370E-07,2.435306120E-09,...
     -1.408812350E-12,-1.046976280E+03,2.967474680E+00];
MW = 28.0134e-3;

% rho = V(1,:);
% p = V(3,:);
% v = 1./rho;
% 
% T = getTfromPandRho(p, rho, eos);
% [a,b,R,dadT,d2adT2] = getThermo(T, eos);
% cp_ideal = R*(an(1) + an(2)*T + an(3)*T.^2 + an(4)*T.^3 + an(5)*T.^4);
% cv_ideal = cp_ideal - R;
% 
% dpdT = R./(v-b) - dadT./(v.^2+2*v.*b-b.^2);
% dpdv = -R.*T./(v-b).^2.*(1-2*a.*((R.*T.*(v+b).*((v.^2+2*v.*b-b.^2)./(v.^2-b.^2)).^2).^(-1)));
% K1 = 1/sqrt(8)./b.*log((v+(1-sqrt(2)).*b)./(v+(1+sqrt(2)).*b));
% cp = cv_ideal - T.*(dpdT).^2./dpdv - K1.*T.*d2adT2;
% 
% kT = -1./v./dpdv;
% av = -dpdT./v./dpdv;
% ks = kT - v.*T.*av.^2./cp;
% 
% sos = sqrt(1./rho./ks);

rho = V(1,:);
p = V(3,:);
v = 1000*MW./rho; % JEREMY CHANGE

T = getTfromPandRho(p, rho, eos);
[a,b,R,dadT,d2adT2] = getThermo(T, eos);
cp_ideal = R*(an(1) + an(2)*T + an(3)*T.^2 + an(4)*T.^3 + an(5)*T.^4);
cv_ideal = cp_ideal - R;
R = 8.314462;
cp_ideal = cp_ideal * MW; % JEREMY CHANGE
cv_ideal = cv_ideal * MW; % JEREMY CHANGE

if eos == 0 %% PR
    delta = 2*b;
    epsilon = -(b^2);   

elseif eos == 1 %% RK
    delta = b;
    epsilon = 0; 
    
elseif eos == 2 %% SRK
    delta = b;
    epsilon = 0;
end
int_dP2dT2 = -(2*d2adT2.*atanh((delta+2*v)./(sqrt(delta^2-4*epsilon))))/(sqrt(delta^2-4*epsilon));
int_dP2dT2 = real(int_dP2dT2); % JEREMY CHANGE
cv_real = cv_ideal + (T.*int_dP2dT2);
dPdT = R./(v-b) - dadT./(v.^2 + delta*v + epsilon); 
dPdV = ((2*v + delta).*a)./(v.^2 + delta*v + epsilon).^2 - (R*T)./((v-b).^2);
cp_real = cv_real - T.*(dPdT).^2./(dPdV);

sos_squared = (-(v.^2)./MW).*dPdV*(cp_real/cv_real);
sos = sqrt(sos_squared);

end

