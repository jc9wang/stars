%{
Author:             Jeremy Wang, University of Waterloo
Last Update:        2022-05-21
Description:        Analyzes grid convergence characteristics for 1D flow problems
%}

clear; clc; close all;
cur_dir = pwd; % gets present working directory
n_cells = [32, 64, 128, 256, 512, 1024];

%% Test case 1.a: transcritical RCS shock tube (IG)
trans_rcs_IG_exact_n2048 = readtable(sprintf("%s/trans_rcs_IG_exact_t=0.000900_n=2048.xlsx", cur_dir));

% Calculate errors
trans_rcs_IG_RoeStARS_n32 = readtable(sprintf("%s/trans_rcs_IG_Roe-StARS_t=0.000900_n=32.xlsx", cur_dir));
trans_rcs_IG_exact_n32 = downsample(trans_rcs_IG_exact_n2048, 64);
errors_trans_rcs_IG_n32.p_r = (trans_rcs_IG_RoeStARS_n32.p_r - trans_rcs_IG_exact_n32.p_r)./trans_rcs_IG_exact_n32.p_r;
errors_trans_rcs_IG_n32.rho = (trans_rcs_IG_RoeStARS_n32.rho - trans_rcs_IG_exact_n32.rho)./trans_rcs_IG_exact_n32.rho;
errors_trans_rcs_IG_n32.u = (trans_rcs_IG_RoeStARS_n32.u - trans_rcs_IG_exact_n32.u)./trans_rcs_IG_exact_n32.u;

trans_rcs_IG_RoeStARS_n64 = readtable(sprintf("%s/trans_rcs_IG_Roe-StARS_t=0.000900_n=64.xlsx", cur_dir));
trans_rcs_IG_exact_n64 = downsample(trans_rcs_IG_exact_n2048, 32);
errors_trans_rcs_IG_n64.p_r = (trans_rcs_IG_RoeStARS_n64.p_r - trans_rcs_IG_exact_n64.p_r)./trans_rcs_IG_exact_n64.p_r;
errors_trans_rcs_IG_n64.rho = (trans_rcs_IG_RoeStARS_n64.rho - trans_rcs_IG_exact_n64.rho)./trans_rcs_IG_exact_n64.rho;
errors_trans_rcs_IG_n64.u = (trans_rcs_IG_RoeStARS_n64.u - trans_rcs_IG_exact_n64.u)./trans_rcs_IG_exact_n64.u;

trans_rcs_IG_RoeStARS_n128 = readtable(sprintf("%s/trans_rcs_IG_Roe-StARS_t=0.000900_n=128.xlsx", cur_dir));
trans_rcs_IG_exact_n128 = downsample(trans_rcs_IG_exact_n2048, 16);
errors_trans_rcs_IG_n128.p_r = (trans_rcs_IG_RoeStARS_n128.p_r - trans_rcs_IG_exact_n128.p_r)./trans_rcs_IG_exact_n128.p_r;
errors_trans_rcs_IG_n128.rho = (trans_rcs_IG_RoeStARS_n128.rho - trans_rcs_IG_exact_n128.rho)./trans_rcs_IG_exact_n128.rho;
errors_trans_rcs_IG_n128.u = (trans_rcs_IG_RoeStARS_n128.u - trans_rcs_IG_exact_n128.u)./trans_rcs_IG_exact_n128.u;

trans_rcs_IG_RoeStARS_n256 = readtable(sprintf("%s/trans_rcs_IG_Roe-StARS_t=0.000900_n=256.xlsx", cur_dir));
trans_rcs_IG_exact_n256 = downsample(trans_rcs_IG_exact_n2048, 8);
errors_trans_rcs_IG_n256.p_r = (trans_rcs_IG_RoeStARS_n256.p_r - trans_rcs_IG_exact_n256.p_r)./trans_rcs_IG_exact_n256.p_r;
errors_trans_rcs_IG_n256.rho = (trans_rcs_IG_RoeStARS_n256.rho - trans_rcs_IG_exact_n256.rho)./trans_rcs_IG_exact_n256.rho;
errors_trans_rcs_IG_n256.u = (trans_rcs_IG_RoeStARS_n256.u - trans_rcs_IG_exact_n256.u)./trans_rcs_IG_exact_n256.u;

trans_rcs_IG_RoeStARS_n512 = readtable(sprintf("%s/trans_rcs_IG_Roe-StARS_t=0.000900_n=512.xlsx", cur_dir));
trans_rcs_IG_exact_n512 = downsample(trans_rcs_IG_exact_n2048, 4);
errors_trans_rcs_IG_n512.p_r = (trans_rcs_IG_RoeStARS_n512.p_r - trans_rcs_IG_exact_n512.p_r)./trans_rcs_IG_exact_n512.p_r;
errors_trans_rcs_IG_n512.rho = (trans_rcs_IG_RoeStARS_n512.rho - trans_rcs_IG_exact_n512.rho)./trans_rcs_IG_exact_n512.rho;
errors_trans_rcs_IG_n512.u = (trans_rcs_IG_RoeStARS_n512.u - trans_rcs_IG_exact_n512.u)./trans_rcs_IG_exact_n512.u;

trans_rcs_IG_RoeStARS_n1024 = readtable(sprintf("%s/trans_rcs_IG_Roe-StARS_t=0.000900_n=1024.xlsx", cur_dir));
trans_rcs_IG_exact_n1024 = downsample(trans_rcs_IG_exact_n2048, 2);
errors_trans_rcs_IG_n1024.p_r = (trans_rcs_IG_RoeStARS_n1024.p_r - trans_rcs_IG_exact_n1024.p_r)./trans_rcs_IG_exact_n1024.p_r;
errors_trans_rcs_IG_n1024.rho = (trans_rcs_IG_RoeStARS_n1024.rho - trans_rcs_IG_exact_n1024.rho)./trans_rcs_IG_exact_n1024.rho;
errors_trans_rcs_IG_n1024.u = (trans_rcs_IG_RoeStARS_n1024.u - trans_rcs_IG_exact_n1024.u)./trans_rcs_IG_exact_n1024.u;

% Visualize
line_width = 1;
marker_size = 5;
line_colour_p_r = 'r';
line_colour_rho = 'g';
line_colour_u = 'b';
line_spec = '-';
xlim_min = -1;
xlim_max = 1;
figure(1), set(gcf, 'Position', [0 0 1500 600]);
   
subplot(1,3,1);
hold on;
plot(trans_rcs_IG_RoeStARS_n32.x, errors_trans_rcs_IG_n32.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n64.x, errors_trans_rcs_IG_n64.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n128.x, errors_trans_rcs_IG_n128.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_IG_RoeStARS_n256.x, errors_trans_rcs_IG_n256.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n512.x, errors_trans_rcs_IG_n512.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n1024.x, errors_trans_rcs_IG_n1024.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('Error %', 'FontSize', 15);
xlim([xlim_min, xlim_max]);
legend('n=32', 'n=64', 'n=128', 'n=256', 'n=512', 'n=1024', 'Location', 'northwest');

subplot(1,3,2);
hold on;
plot(trans_rcs_IG_RoeStARS_n32.x, errors_trans_rcs_IG_n32.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n64.x, errors_trans_rcs_IG_n64.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n128.x, errors_trans_rcs_IG_n128.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n256.x, errors_trans_rcs_IG_n256.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n512.x, errors_trans_rcs_IG_n512.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n1024.x, errors_trans_rcs_IG_n1024.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(1,3,3);
hold on;
plot(trans_rcs_IG_RoeStARS_n32.x, errors_trans_rcs_IG_n32.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_IG_RoeStARS_n64.x, errors_trans_rcs_IG_n64.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_IG_RoeStARS_n128.x, errors_trans_rcs_IG_n128.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_IG_RoeStARS_n256.x, errors_trans_rcs_IG_n256.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_IG_RoeStARS_n512.x, errors_trans_rcs_IG_n512.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_IG_RoeStARS_n1024.x, errors_trans_rcs_IG_n1024.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

%% Test case 1.b: transcritical RCS shock tube (PR)
trans_rcs_PR_exact_n2048 = readtable(sprintf("%s/trans_rcs_PR_exact_t=0.000900_n=2048.xlsx", cur_dir));

trans_rcs_PR_RoeStARS_n32 = readtable(sprintf("%s/trans_rcs_PR_Roe-StARS_t=0.000900_n=32.xlsx", cur_dir));
trans_rcs_PR_exact_n32 = downsample(trans_rcs_PR_exact_n2048, 64);
errors_trans_rcs_PR_n32.p_r = (trans_rcs_PR_RoeStARS_n32.p_r - trans_rcs_PR_exact_n32.p_r)./trans_rcs_PR_exact_n32.p_r;
errors_trans_rcs_PR_n32.rho = (trans_rcs_PR_RoeStARS_n32.rho - trans_rcs_PR_exact_n32.rho)./trans_rcs_PR_exact_n32.rho;
errors_trans_rcs_PR_n32.u = (trans_rcs_PR_RoeStARS_n32.u - trans_rcs_PR_exact_n32.u)./trans_rcs_PR_exact_n32.u;

trans_rcs_PR_RoeStARS_n64 = readtable(sprintf("%s/trans_rcs_PR_Roe-StARS_t=0.000900_n=64.xlsx", cur_dir));
trans_rcs_PR_exact_n64 = downsample(trans_rcs_PR_exact_n2048, 32);
errors_trans_rcs_PR_n64.p_r = (trans_rcs_PR_RoeStARS_n64.p_r - trans_rcs_PR_exact_n64.p_r)./trans_rcs_PR_exact_n64.p_r;
errors_trans_rcs_PR_n64.rho = (trans_rcs_PR_RoeStARS_n64.rho - trans_rcs_PR_exact_n64.rho)./trans_rcs_PR_exact_n64.rho;
errors_trans_rcs_PR_n64.u = (trans_rcs_PR_RoeStARS_n64.u - trans_rcs_PR_exact_n64.u)./trans_rcs_PR_exact_n64.u;

trans_rcs_PR_RoeStARS_n128 = readtable(sprintf("%s/trans_rcs_PR_Roe-StARS_t=0.000900_n=128.xlsx", cur_dir));
trans_rcs_PR_exact_n128 = downsample(trans_rcs_PR_exact_n2048, 16);
errors_trans_rcs_PR_n128.p_r = (trans_rcs_PR_RoeStARS_n128.p_r - trans_rcs_PR_exact_n128.p_r)./trans_rcs_PR_exact_n128.p_r;
errors_trans_rcs_PR_n128.rho = (trans_rcs_PR_RoeStARS_n128.rho - trans_rcs_PR_exact_n128.rho)./trans_rcs_PR_exact_n128.rho;
errors_trans_rcs_PR_n128.u = (trans_rcs_PR_RoeStARS_n128.u - trans_rcs_PR_exact_n128.u)./trans_rcs_PR_exact_n128.u;

trans_rcs_PR_RoeStARS_n256 = readtable(sprintf("%s/trans_rcs_PR_Roe-StARS_t=0.000900_n=256.xlsx", cur_dir));
trans_rcs_PR_exact_n256 = downsample(trans_rcs_PR_exact_n2048, 8);
errors_trans_rcs_PR_n256.p_r = (trans_rcs_PR_RoeStARS_n256.p_r - trans_rcs_PR_exact_n256.p_r)./trans_rcs_PR_exact_n256.p_r;
errors_trans_rcs_PR_n256.rho = (trans_rcs_PR_RoeStARS_n256.rho - trans_rcs_PR_exact_n256.rho)./trans_rcs_PR_exact_n256.rho;
errors_trans_rcs_PR_n256.u = (trans_rcs_PR_RoeStARS_n256.u - trans_rcs_PR_exact_n256.u)./trans_rcs_PR_exact_n256.u;

trans_rcs_PR_RoeStARS_n512 = readtable(sprintf("%s/trans_rcs_PR_Roe-StARS_t=0.000900_n=512.xlsx", cur_dir));
trans_rcs_PR_exact_n512 = downsample(trans_rcs_PR_exact_n2048, 4);
errors_trans_rcs_PR_n512.p_r = (trans_rcs_PR_RoeStARS_n512.p_r - trans_rcs_PR_exact_n512.p_r)./trans_rcs_PR_exact_n512.p_r;
errors_trans_rcs_PR_n512.rho = (trans_rcs_PR_RoeStARS_n512.rho - trans_rcs_PR_exact_n512.rho)./trans_rcs_PR_exact_n512.rho;
errors_trans_rcs_PR_n512.u = (trans_rcs_PR_RoeStARS_n512.u - trans_rcs_PR_exact_n512.u)./trans_rcs_PR_exact_n512.u;

trans_rcs_PR_RoeStARS_n1024 = readtable(sprintf("%s/trans_rcs_PR_Roe-StARS_t=0.000900_n=1024.xlsx", cur_dir));
trans_rcs_PR_exact_n1024 = downsample(trans_rcs_PR_exact_n2048, 2);
errors_trans_rcs_PR_n1024.p_r = (trans_rcs_PR_RoeStARS_n1024.p_r - trans_rcs_PR_exact_n1024.p_r)./trans_rcs_PR_exact_n1024.p_r;
errors_trans_rcs_PR_n1024.rho = (trans_rcs_PR_RoeStARS_n1024.rho - trans_rcs_PR_exact_n1024.rho)./trans_rcs_PR_exact_n1024.rho;
errors_trans_rcs_PR_n1024.u = (trans_rcs_PR_RoeStARS_n1024.u - trans_rcs_PR_exact_n1024.u)./trans_rcs_PR_exact_n1024.u;

% Visualize
line_width = 1;
marker_size = 5;
line_colour_p_r = 'r';
line_colour_rho = 'g';
line_colour_u = 'b';
line_spec = '-';
xlim_min = -1;
xlim_max = 1;
figure(2), set(gcf, 'Position', [0 0 1500 600]);
   
subplot(1,3,1);
hold on;
plot(trans_rcs_PR_RoeStARS_n32.x, errors_trans_rcs_PR_n32.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n64.x, errors_trans_rcs_PR_n64.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n128.x, errors_trans_rcs_PR_n128.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_PR_RoeStARS_n256.x, errors_trans_rcs_PR_n256.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n512.x, errors_trans_rcs_PR_n512.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n1024.x, errors_trans_rcs_PR_n1024.p_r, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('Error %', 'FontSize', 15);
xlim([xlim_min, xlim_max]);
legend('n=32', 'n=64', 'n=128', 'n=256', 'n=512', 'n=1024', 'Location', 'northwest');

subplot(1,3,2);
hold on;
plot(trans_rcs_PR_RoeStARS_n32.x, errors_trans_rcs_PR_n32.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n64.x, errors_trans_rcs_PR_n64.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n128.x, errors_trans_rcs_PR_n128.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n256.x, errors_trans_rcs_PR_n256.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n512.x, errors_trans_rcs_PR_n512.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n1024.x, errors_trans_rcs_PR_n1024.rho, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(1,3,3);
hold on;
plot(trans_rcs_PR_RoeStARS_n32.x, errors_trans_rcs_PR_n32.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_PR_RoeStARS_n64.x, errors_trans_rcs_PR_n64.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_PR_RoeStARS_n128.x, errors_trans_rcs_PR_n128.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_PR_RoeStARS_n256.x, errors_trans_rcs_PR_n256.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size);
plot(trans_rcs_PR_RoeStARS_n512.x, errors_trans_rcs_PR_n512.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
plot(trans_rcs_PR_RoeStARS_n1024.x, errors_trans_rcs_PR_n1024.u, line_spec, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

%% Grid convergence 

% error_threshold = 0.3;
% errors_trans_rcs_IG_n32.rho(errors_trans_rcs_IG_n32.rho>=error_threshold)=0;
% L2_trans_rcs_IG(1) = sqrt(sum((errors_trans_rcs_IG_n32.rho).^2));
% errors_trans_rcs_IG_n64.rho(errors_trans_rcs_IG_n64.rho>=error_threshold)=0;
% L2_trans_rcs_IG(2) = sqrt(sum((errors_trans_rcs_IG_n64.rho).^2));
% errors_trans_rcs_IG_n128.rho(errors_trans_rcs_IG_n128.rho>=error_threshold)=0;
% L2_trans_rcs_IG(3) = sqrt(sum((errors_trans_rcs_IG_n128.rho).^2));
% errors_trans_rcs_IG_n256.rho(errors_trans_rcs_IG_n256.rho>=error_threshold)=0;
% L2_trans_rcs_IG(4) = sqrt(sum((errors_trans_rcs_IG_n256.rho).^2));
% errors_trans_rcs_IG_n512.rho(errors_trans_rcs_IG_n512.rho>=error_threshold)=0;
% L2_trans_rcs_IG(5) = sqrt(sum((errors_trans_rcs_IG_n512.rho).^2));
% errors_trans_rcs_IG_n1024.rho(errors_trans_rcs_IG_n1024.rho>=error_threshold)=0;
% L2_trans_rcs_IG(6) = sqrt(sum((errors_trans_rcs_IG_n1024.rho).^2));

% x_threshold = 1;
% L2_trans_rcs_IG(1) = sqrt(sum(errors_trans_rcs_IG_n32.rho(trans_rcs_IG_RoeStARS_n32.x<=x_threshold).^2));
% L2_trans_rcs_IG(2) = sqrt(sum(errors_trans_rcs_IG_n64.rho(trans_rcs_IG_RoeStARS_n64.x<=x_threshold).^2));
% L2_trans_rcs_IG(3) = sqrt(sum(errors_trans_rcs_IG_n128.rho(trans_rcs_IG_RoeStARS_n128.x<=x_threshold).^2));
% L2_trans_rcs_IG(4) = sqrt(sum(errors_trans_rcs_IG_n256.rho(trans_rcs_IG_RoeStARS_n256.x<=x_threshold).^2));
% L2_trans_rcs_IG(5) = sqrt(sum(errors_trans_rcs_IG_n512.rho(trans_rcs_IG_RoeStARS_n512.x<=x_threshold).^2));
% L2_trans_rcs_IG(6) = sqrt(sum(errors_trans_rcs_IG_n1024.rho(trans_rcs_IG_RoeStARS_n1024.x<=x_threshold).^2));
% Linfty_trans_rcs_IG(1) = max(abs(errors_trans_rcs_IG_n32.rho(trans_rcs_IG_RoeStARS_n32.x<=x_threshold)));
% Linfty_trans_rcs_IG(2) = max(abs(errors_trans_rcs_IG_n64.rho(trans_rcs_IG_RoeStARS_n64.x<=x_threshold)));
% Linfty_trans_rcs_IG(3) = max(abs(errors_trans_rcs_IG_n128.rho(trans_rcs_IG_RoeStARS_n128.x<=x_threshold)));
% Linfty_trans_rcs_IG(4) = max(abs(errors_trans_rcs_IG_n256.rho(trans_rcs_IG_RoeStARS_n256.x<=x_threshold)));
% Linfty_trans_rcs_IG(5) = max(abs(errors_trans_rcs_IG_n512.rho(trans_rcs_IG_RoeStARS_n512.x<=x_threshold)));
% Linfty_trans_rcs_IG(6) = max(abs(errors_trans_rcs_IG_n1024.rho(trans_rcs_IG_RoeStARS_n1024.x<=x_threshold)));

L2_trans_rcs_IG(1) = sqrt(errors_trans_rcs_IG_n32.rho(floor(end/2+1)).^2 + errors_trans_rcs_IG_n32.rho(floor(end/2+1) - 1).^2);
L2_trans_rcs_IG(2) = sqrt(errors_trans_rcs_IG_n64.rho(floor(end/2+1)).^2 + errors_trans_rcs_IG_n64.rho(floor(end/2+1) - 1).^2);
L2_trans_rcs_IG(3) = sqrt(errors_trans_rcs_IG_n128.rho(floor(end/2+1)).^2 + errors_trans_rcs_IG_n128.rho(floor(end/2+1) - 1).^2);
L2_trans_rcs_IG(4) = sqrt(errors_trans_rcs_IG_n256.rho(floor(end/2+1)).^2 + errors_trans_rcs_IG_n256.rho(floor(end/2+1) - 1).^2);
L2_trans_rcs_IG(5) = sqrt(errors_trans_rcs_IG_n512.rho(floor(end/2+1)).^2 + errors_trans_rcs_IG_n512.rho(floor(end/2+1) - 1).^2);
L2_trans_rcs_IG(6) = sqrt(errors_trans_rcs_IG_n1024.rho(floor(end/2+1)).^2 + errors_trans_rcs_IG_n1024.rho(floor(end/2+1) - 1).^2);
% Linfty_trans_rcs_IG(1) = max(abs(errors_trans_rcs_IG_n32.rho(floor(end/2+1))), abs(errors_trans_rcs_IG_n32.rho(floor(end/2+1) - 1)));
% Linfty_trans_rcs_IG(2) = max(abs(errors_trans_rcs_IG_n64.rho(floor(end/2+1))), abs(errors_trans_rcs_IG_n64.rho(floor(end/2+1) - 1)));
% Linfty_trans_rcs_IG(3) = max(abs(errors_trans_rcs_IG_n128.rho(floor(end/2+1))), abs(errors_trans_rcs_IG_n128.rho(floor(end/2+1) - 1)));
% Linfty_trans_rcs_IG(4) = max(abs(errors_trans_rcs_IG_n256.rho(floor(end/2+1))), abs(errors_trans_rcs_IG_n256.rho(floor(end/2+1) - 1)));
% Linfty_trans_rcs_IG(5) = max(abs(errors_trans_rcs_IG_n512.rho(floor(end/2+1))), abs(errors_trans_rcs_IG_n512.rho(floor(end/2+1) - 1)));
% Linfty_trans_rcs_IG(6) = max(abs(errors_trans_rcs_IG_n1024.rho(floor(end/2+1))), abs(errors_trans_rcs_IG_n1024.rho(floor(end/2+1) - 1)));

% L2_trans_rcs_PR(1) = sqrt(errors_trans_rcs_PR_n32.p_r(floor(end/2+1)).^2 + errors_trans_rcs_PR_n32.p_r(floor(end/2+1) - 1).^2);
% L2_trans_rcs_PR(2) = sqrt(errors_trans_rcs_PR_n64.p_r(floor(end/2+1)).^2 + errors_trans_rcs_PR_n64.p_r(floor(end/2+1) - 1).^2);
% L2_trans_rcs_PR(3) = sqrt(errors_trans_rcs_PR_n128.p_r(floor(end/2+1)).^2 + errors_trans_rcs_PR_n128.p_r(floor(end/2+1) - 1).^2);
% L2_trans_rcs_PR(4) = sqrt(errors_trans_rcs_PR_n256.p_r(floor(end/2+1)).^2 + errors_trans_rcs_PR_n256.p_r(floor(end/2+1) - 1).^2);
% L2_trans_rcs_PR(5) = sqrt(errors_trans_rcs_PR_n512.p_r(floor(end/2+1)).^2 + errors_trans_rcs_PR_n512.p_r(floor(end/2+1) - 1).^2);
% L2_trans_rcs_PR(6) = sqrt(errors_trans_rcs_PR_n1024.p_r(floor(end/2+1)).^2 + errors_trans_rcs_PR_n1024.p_r(floor(end/2+1) - 1).^2);
% Linfty_trans_rcs_PR(1) = max(abs(errors_trans_rcs_PR_n32.p_r(floor(end/2+1))), abs(errors_trans_rcs_PR_n32.p_r(floor(end/2+1) - 1)));
% Linfty_trans_rcs_PR(2) = max(abs(errors_trans_rcs_PR_n64.p_r(floor(end/2+1))), abs(errors_trans_rcs_PR_n64.p_r(floor(end/2+1) - 1)));
% Linfty_trans_rcs_PR(3) = max(abs(errors_trans_rcs_PR_n128.p_r(floor(end/2+1))), abs(errors_trans_rcs_PR_n128.p_r(floor(end/2+1) - 1)));
% Linfty_trans_rcs_PR(4) = max(abs(errors_trans_rcs_PR_n256.p_r(floor(end/2+1))), abs(errors_trans_rcs_PR_n256.p_r(floor(end/2+1) - 1)));
% Linfty_trans_rcs_PR(5) = max(abs(errors_trans_rcs_PR_n512.p_r(floor(end/2+1))), abs(errors_trans_rcs_PR_n512.p_r(floor(end/2+1) - 1)));
% Linfty_trans_rcs_PR(6) = max(abs(errors_trans_rcs_PR_n1024.p_r(floor(end/2+1))), abs(errors_trans_rcs_PR_n1024.p_r(floor(end/2+1) - 1)));

figure(3), set(gcf, 'Position', [0 0 600 400]);
loglog(n_cells, L2_trans_rcs_IG, '^-'); hold on;
% loglog(n_cells, Linfty_trans_rcs_IG, 's-');
loglog(n_cells, 10./n_cells, '--', 'Color', [0.75, 0.75, 0.75], 'LineWidth', line_width);
xlabel('n_{cells}', 'FontSize', 15);
ylabel('Error', 'FontSize', 15);
xlim([10, 10^(3.5)]);
ylim([10^-(2.75), 0.5]);
legend('L_2 error', 'O(1)', 'Location', 'southwest');

% subplot(1,2,2);
% loglog(n_cells, L2_trans_rcs_PR, '^-'); hold on;
% loglog(n_cells, Linfty_trans_rcs_PR, 's-');
% loglog(n_cells, 10./n_cells, '--');
% xlabel('n_{cells}', 'FontSize', 15);
% ylabel('Error (non-ideal gas)', 'FontSize', 15);xlim([10, 10^(3.5)]);
% ylim([10^-(2.75), 0.5]);
% legend('L_2 error', 'L_\infty error', 'O(1)', 'Location', 'southwest');