%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-09-17
Description:    Calculates pressure of a pure gas given v, T, some gas properties, and choice of Equation of State (EOS).
%}

function [p] = getPfromVT(v, T)
%{
Inputs:         v = vector of molar specific volumes [m^3 kmol^-1]
                T = vector of temperatures [K]
Outputs:        p = vector of pressures [Pa]
%}
    global a b delta epsilon omega R T_c type_eos

    switch type_eos
        case "IG"
            p = R*T./v;
            return;
        case "RK"
            Theta = a*(1./T).^0.5;
        case "SRK"
            Theta = a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T./T_c).^0.5)).^2;
        case "PR"
            Theta = a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T./T_c).^0.5)).^2;
    end

    p = R.*T./(v - b) - Theta./(v.^2 + delta.*v + epsilon);
end