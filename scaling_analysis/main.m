%{
Author:             Jeremy Wang, University of Waterloo
Last Update:        2021-12-19
Description:        - Physical quantities generally follow the format: <quantity>_<location/time/conditions>_<calculationmethod>, for example:
                        c = specific heat capacity [J kmol^-1 K^-1]
                        h = enthalpy [J kmol^-1]
                        p = pressure [Pa]
                        rho = density [kg m^-3]
                        sos = speed of sound [m s^-1]
                        T = temperature [K]                      
                        u = speed [m s^-1]
                        v = molar volume [m^3 kmol^-1]
 %}

clear; clc; close all;
global A a b delta epsilon gamma omega M p_c R T_c type_eos

%% Configure the analysis
fprintf("Populating input variables...\n");

% Load fluid properties
type_fluid = "N2gas";
filename_properties = 'gas_properties.xlsx';
properties = table2struct(readtable(filename_properties));
idx_gas = find(contains({properties.gas_name}, type_fluid));
A = str2num(properties(idx_gas).c_IG_p_fit_params); % vector of polynomial fit parameters for ideal-gas thermally perfect c_p
gamma = properties(idx_gas).gamma; % ideal-gas specific heat ratio [unitless]
M = properties(idx_gas).M; % molar mass [kg kmol^-1]
omega = properties(idx_gas).omega; % acentric factor [unitless]
p_c = properties(idx_gas).p_c; % critical pressure [Pa]
T_c = properties(idx_gas).T_c; % critical temperature [Pa]
R = 8314.4621; % Universal gas constant [J kmol^-1 K^-1]

% Configure EOS and EOS parameters based on Abbott's generic cubic equation (1979)
type_eos = "PR"; % {IG, RK, SRK, PR}
switch type_eos
    case "IG"
        a = 0;
        b = 0;
        delta = 0;
        epsilon = 0;
    case "RK"
        a = 0.4278*R^2*T_c^(2.5)/p_c;
        b = 0.0867*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "SRK"
        a = 0.42747*R^2*T_c^2/p_c;
        b = 0.08664*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "PR"
        a = 0.45724*R^2*T_c^2/p_c;
        b = 0.07780*R*T_c/p_c;
        delta = 2*b;
        epsilon = -b^2;
end

dpdx = -1e7; % pressure gradient
dTdx = -1e3; % temperature gradient
dudx = 1e4; % flow speed gradient
dx = 5e-3; % cell width
u = 300; % flow speed for analysis
p_r = 0.5:0.01:4.5; % reduced pressures for analysis
T_r = 0.5:0.01:2.5; % reduced temperatures for analysis

save_results = 0;
iterations_fsolve_max = 1000;
    
%% COMPUTING ERRORS FROM OMITTING THE RAREFACTION
fprintf('Computing errors from omitting the rarefaction...\n');
f = waitbar(0, 'Processing the domain of interest...');
counter = 0;
total_count = length(p_r)*length(T_r)*length(u);

% Find u for {L, R}
u_L = u;
u_R = u_L + dudx*dx;

for i = 1:length(p_r)
    for j = 1:length(T_r)
        % Compute quantities for {L, R}
        % ------------------------------------------------------------------------------------------------------------
        p_L(i,j) = p_r(i)*p_c;
        p_R(i,j) = p_L(i,j) + dpdx*dx;
        T_L(i,j) = T_r(j)*T_c;
        T_R(i,j) = T_L(i,j) + dTdx*dx;
        v_L(i,j) = getVfromPT(p_L(i,j), T_L(i,j));
        v_R(i,j) = getVfromPT(p_R(i,j), T_L(i,j));
        rho_L(i,j) = M/v_L(i,j);
        rho_R(i,j) = M/v_R(i,j);
        c_L(i,j) = getSOS(v_L(i,j), T_L(i,j));
        c_R(i,j) = getSOS(v_R(i,j), T_R(i,j));
        E_L(i,j) = getEfromVT(v_L(i,j), T_L(i,j))./v_L(i,j) + (1/2)*rho_L(i,j).*u_L.^2;
        E_R(i,j) = getEfromVT(v_R(i,j), T_R(i,j))./v_R(i,j) + (1/2)*rho_R(i,j).*u_R.^2;
        H_L(i,j) = (E_L(i,j) + p_L(i,j))./rho_L(i,j);
        H_R(i,j) = (E_R(i,j) + p_R(i,j))./rho_R(i,j);
        U_L(:,i,j) = [rho_L(i,j); rho_L(i,j).*u_L; E_L(i,j)];
        U_R(:,i,j) = [rho_R(i,j); rho_R(i,j).*u_R; E_R(i,j)];
        F_L(:,i,j) = [rho_L(i,j).*u_L; rho_L(i,j).*(u_L).^2 + p_L(i,j); u_L.*(E_L(i,j) + p_L(i,j))];
        F_R(:,i,j) = [rho_R(i,j).*u_R; rho_R(i,j).*(u_R).^2 + p_R(i,j); u_R.*(E_R(i,j) + p_R(i,j))];
        
        % Compute the Roe flux vector at the cell interface
        % ------------------------------------------------------------------------------------------------------------     
        rho_avg(i,j) = sqrt(rho_L(i,j)).*sqrt(rho_R(i,j));
        p_avg(i,j) = (sqrt(rho_L(i,j)).*p_L(i,j) + sqrt(rho_R(i,j)).*p_R(i,j))./(sqrt(rho_L(i,j)) + sqrt(rho_R(i,j)));
        u_avg(i,j) = (sqrt(rho_L(i,j)).*u_L + sqrt(rho_R(i,j)).*u_R)./(sqrt(rho_L(i,j)) + sqrt(rho_R(i,j)));
        H_avg(i,j) = (sqrt(rho_L(i,j)).*H_L(i,j) + sqrt(rho_R(i,j)).*H_R(i,j))./(sqrt(rho_L(i,j)) + sqrt(rho_R(i,j)));
        v_avg(i,j) = (sqrt(rho_L(i,j)).*v_L(i,j) + sqrt(rho_R(i,j)).*v_R(i,j))./(sqrt(rho_L(i,j)) + sqrt(rho_R(i,j)));
        T_avg(i,j) = (sqrt(rho_L(i,j)).*T_L(i,j) + sqrt(rho_R(i,j)).*T_R(i,j))./(sqrt(rho_L(i,j)) + sqrt(rho_R(i,j)));
        c_avg(i,j) = getSOS(v_avg(i,j), T_avg(i,j));
        
        % Compute the wavespeeds / eigenvalues
        lambda = [abs(u_avg(i,j) - c_avg(i,j)); 
            abs(u_avg(i,j)); 
            abs(u_avg(i,j) + c_avg(i,j))];
    
        % Compute the right eigenvectors
        K = [1, 1, 1; 
            u_avg(i,j) - c_avg(i,j), u_avg(i,j), u_avg(i,j) + c_avg(i,j); 
            H_avg(i,j) - u_avg(i,j)*c_avg(i,j), u_avg(i,j)^2/2, H_avg(i,j) + u_avg(i,j)*c_avg(i,j)];
    
        % Compute the differences in primitive variables
        d_rho = rho_R(i,j) - rho_L(i,j);
        d_u = u_R - u_L;
        d_p = p_R(i,j) - p_L(i,j);
        
        % Compute the wave strengths
        dV = [(d_p - rho_avg(i,j)*c_avg(i,j)*d_u)/(2*c_avg(i,j)^2);
            -(d_p/c_avg(i,j)^2 - d_rho); 
            (d_p + rho_avg(i,j)*c_avg(i,j)*d_u)/(2*c_avg(i,j)^2)];
    
        % Compute the Roe flux vector
        F_Roe(:,i,j) = (F_L(:,i,j) + F_R(:,i,j) - K*(lambda.*dV))/2;

%         % Compute the Roe conservative vector
%         U_Roe(:,i,j) = U_L(:,i,j);
%         for m = 1:length(lambda)
%             if lambda(m) <= 0
%                 U_Roe(:,i,j) = U_Roe(:,i,j) + dV(i)*K(:,i);
%             end
%         end
   
        % Compute the Roe StARS flux and conservative vectors at the cell interface
        % ------------------------------------------------------------------------------------------------------------

        % Compute Ma_star
        Ma(i,j) = abs(u_L / c_L(i,j));
        Ma_star(i,j) = abs(u_avg(i,j) / c_avg(i,j));
        
        % Determine if a left transonic rarefaction would occur, and if so, the corresponding error
        if (Ma(i,j) < 1) && (Ma_star(i,j) > 1)
            A_2_LEW = -0.5*log(v_L(i,j).*v_avg(i,j)) + 0.5*log(v_L(i,j)./v_avg(i,j)).*(u_L - c_L(i,j) + u_avg(i,j) - c_avg(i,j))./(u_L - c_L(i,j) - u_avg(i,j) + c_avg(i,j));
            v_LEW = exp(-A_2_LEW);        
            rho_LEW = M./v_LEW;
            A_4_LEW = -0.5*log(p_L(i,j).*p_avg(i,j)) + 0.5*log(p_L(i,j)./p_avg(i,j)).*(u_L - c_L(i,j) + u_avg(i,j) - c_avg(i,j))./(u_L - c_L(i,j) - u_avg(i,j) + c_avg(i,j));
            p_LEW = exp(-A_4_LEW);
            A_5_LEW = 0.5*(u_L + u_avg(i,j)) - 0.5*(u_L - c_L(i,j) + u_avg(i,j) - c_avg(i,j)).*(u_L - u_avg(i,j))./(u_L - c_L(i,j) - u_avg(i,j) + c_avg(i,j));
            u_LEW = A_5_LEW;
            T_LEW = getTfromPV(p_LEW, v_LEW);
            e_LEW = getEfromVT(v_LEW, T_LEW);

            dvv(i,j) = abs((v_LEW - v_avg(i,j))/v_L(i,j));
            dpp(i,j) = abs((p_LEW - p_avg(i,j))/p_L(i,j));
            duu(i,j) = abs((u_LEW - u_avg(i,j))/u_L);

            F_Roe_StARS(:,i,j) = [rho_LEW*u_LEW; 
                rho_LEW.*u_LEW.^2 + p_LEW; 
                u_LEW.*(e_LEW./v_LEW + (1/2)*rho_LEW.*u_LEW.^2 + p_LEW)];

            dFF(:,i,j) = abs((F_Roe(:,i,j) - F_Roe_StARS(:,i,j))./F_Roe_StARS(:,i,j));
        else
            dvv(i,j) = 0;
            dpp(i,j) = 0;
            duu(i,j) = 0;
            dFF(:,i,j) = [0;0;0];
        end
        counter = counter + 1;
        
        pct_complete = counter/total_count;
        f = waitbar(pct_complete, f, sprintf('Processing the domain of interest... %2.3f %%', pct_complete*100));
    end
end
close(f)

%% PLOTTING THE RESULTS
fprintf('Now plotting results...\n');

% Plot Ma_star == 1
% ------------------------------------------------------------------------------------------------------------
figure; hold on;
[X,Y] = meshgrid(T_r, p_r); % since i <> Y, j <> X, k <> Z
contour(X,Y,Ma,[1,1], 'r-');
contour(X,Y,Ma_star,[1,1], 'b');
xlabel('T_r');
ylabel('p_r');
legend('Ma = 1','Ma_* = 1');

% Plot relative errors in primitive variables and fluxes
% ------------------------------------------------------------------------------------------------------------
figure; hold on;
set(gcf,'position',[0,0,1500,800])

subplot(2,3,1);
% contourf(X,Y,dvv,[10]);
pcolor(X,Y,dvv); hold on;
shading interp
caxis([0 max(dvv, [], 'all')]);
% caxis([0 0.04]);
colorbar;
contour(X,Y,Ma,[1,1], 'r-');
contour(X,Y,Ma_star,[1,1], 'b');
title('|\Deltav/v|');
ylabel('p_r');

subplot(2,3,2);
% contourf(X,Y,dpp,[10]);
pcolor(X,Y,dpp); hold on;
shading interp
caxis([0 max(dpp, [], 'all')]);
colorbar;
contour(X,Y,Ma,[1,1], 'r-');
contour(X,Y,Ma_star,[1,1], 'b');
title('|\Deltap/p|');
ylabel('p_r');

subplot(2,3,3);
% contourf(X,Y,duu,[10]);
pcolor(X,Y,duu); hold on;
shading interp
caxis([0 max(duu, [], 'all')]);
colorbar;
contour(X,Y,Ma,[1,1], 'r-');
contour(X,Y,Ma_star,[1,1], 'b');
title('|\Deltau/u|');
ylabel('p_r');

subplot(2,3,4);
% contourf(X,Y,squeeze(dFF(1,:,:)),[10]);
pcolor(X,Y,squeeze(dFF(1,:,:))); hold on;
shading interp
caxis([0 max(squeeze(dFF(1,:,:)), [], 'all')]);
colorbar;
contour(X,Y,Ma,[1,1], 'r-');
contour(X,Y,Ma_star,[1,1], 'b');
title('|\DeltaF_1/F_{1 Roe-StARS}|');
xlabel('T_r');
ylabel('p_r');

subplot(2,3,5);
% contourf(X,Y,squeeze(dFF(2,:,:)),[10]);
pcolor(X,Y,squeeze(dFF(2,:,:))); hold on;
shading interp
caxis([0 max(squeeze(dFF(2,:,:)), [], 'all')]);
colorbar;
contour(X,Y,Ma,[1,1], 'r-');
contour(X,Y,Ma_star,[1,1], 'b');
title('|\DeltaF_2/F_{2 Roe-StARS}|');
xlabel('T_r');
ylabel('p_r');

subplot(2,3,6);
% contourf(X,Y,squeeze(dFF(3,:,:)),[10]);
pcolor(X,Y,squeeze(dFF(3,:,:))); hold on;
shading interp
caxis([0 max(squeeze(dFF(3,:,:)), [], 'all')]);
colorbar;
contour(X,Y,Ma,[1,1], 'r-');
contour(X,Y,Ma_star,[1,1], 'b');
title('|\DeltaF_3/F_{3 Roe-StARS}|');
xlabel('T_r');
ylabel('p_r');

% Plot c
% ------------------------------------------------------------------------------------------------------------
figure; hold on;
% contour(X,Y,c_out,[10]);
pcolor(X,Y,c_L); hold on;
shading interp
colorbar;
contour(X,Y,Ma,[1,1], 'r-');
title('c [m s^{-1}]');
xlabel('T_r');
ylabel('p_r');

%% SAVE THE RESULTS

if save_results
    save(sprintf('scaling_analysis_pr%0.1fto%0.1f_Tr%0.1fto%0.1f.mat', min(p_r), max(p_r), min(T_r), max(T_r)), ...
        'T_r', 'p_r', 'Ma', 'Ma_star', 'dvv', 'dpp', 'duu', 'dFF', 'c_L');
end
