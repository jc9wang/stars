%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-09-17
Description:    Computes enthalpy for a pure gas given choice of EOS and known variables.
%}

function [h] = getHfromVT(v, T)
%{
Inputs:         v = vector of molar specific volumes [m^3 mol^-1]
                T = vector of temperatures [K]
Outputs:        h = vector of enthalpies [J kmol^-1]
%}
    
    global A a delta epsilon omega R T_c type_eos

    % Calculate the ideal-gas thermally perfect c_p using polynomial approximation
    syms x
    for i = 1:length(A) 
        T_mat(i,1) = x^(i-1); 
    end
    c_p_IG_TP = R*(A*T_mat);

    % Calculate the ideal-gas thermally perfect enthalpy, assuming reference enthalpy is zero
    h_IG_TP = int(c_p_IG_TP, x);
    h_IG_TP = double(subs(h_IG_TP, T));

    % Configure EOS
    switch type_eos
        case "IG"
            h = h_IG_TP;
            return;
        case "RK"
            Theta = a*(1./T).^0.5;
            dThetadT = -0.5*a*T.^-1.5;
        case "SRK"
            Theta = a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T./T_c).^0.5)).^2;
            dThetadT = -a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T./T_c).^0.5)).*(0.48 + ...
                1.574*omega - 0.176*omega^2)./(T.*T_c).^0.5; 
        case "PR"
            Theta = a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T./T_c).^0.5)).^2;
            dThetadT = -a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T./T_c).^0.5)).*(0.37464 + ...
                1.54226*omega - 0.2699*omega^2)./(T.*T_c).^0.5;
    end

    % Calculate the enthalpy departure function
    p = getPfromVT(v, T);
    delta_h_IG_TP = real((Theta - T.*dThetadT).*log((sqrt(delta.^2 - 4*epsilon) + ...
        2*v + delta)./(sqrt(delta^2 - 4*epsilon) - 2*v - delta))./(sqrt(delta^2 - 4*epsilon)) + R*T - p.*v);
    if type_eos == "IG" % if IG EOS selected, delta_h_IG evaluates to a NaN when it should be zero.
            delta_h_IG_TP = 0;
    end
    
    % Calculate the real-gas enthalpy
    h = h_IG_TP - delta_h_IG_TP;
end