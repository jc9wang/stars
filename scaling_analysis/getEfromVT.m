%{
Author:         Jeremy Wang, University of Waterloo
Last Update:    2021-09-17
Description:    Computes internal energy for a pure gas given choice of EOS and known variables.
%}

function [e] = getEfromVT(v, T)
%{
Inputs:         v = vector of molar specific volumes [m^3 mol^-1]
                T = vector of temperatures [K]
Outputs:        e = vector of internal energies [J kmol^-1]
%}

    h = getHfromVT(v, T);
    p = getPfromVT(v, T);
    e = h - p.*v;
end