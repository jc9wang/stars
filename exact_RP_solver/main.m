%{
Author:             Jeremy Wang, University of Waterloo
Last Update:        2021-12-18
Description:        - 1D exact shock tube solver with modular thermodynamics
                    - Physical quantities generally follow the format: <quantity>_<location/time/conditions>_<calculationmethod>, for example:
                        c = specific heat capacity [J kmol^-1 K^-1]
                        h = enthalpy [J kmol^-1]
                        p = pressure [Pa]
                        rho = density [kg m^-3]
                        sos = speed of sound [m s^-1]
                        T = temperature [K]                      
                        u = speed [m s^-1]
                        v = molar volume [m^3 kmol^-1]
%}

clear; clc; close all;
global A a b delta dx epsilon M n_cells omega p_c R T_c type_eos

%% Configure the problem
fprintf("Configuring problem...\n");

% Load fluid properties
type_fluid = "N2gas";
filename_properties = 'gas_properties.xlsx';
properties = table2struct(readtable(filename_properties));
idx_gas = find(contains({properties.gas_name}, type_fluid));
A = str2num(properties(idx_gas).c_IG_p_fit_params); % vector of polynomial fit parameters for ideal-gas thermally perfect c_p
gamma = properties(idx_gas).gamma; % ideal-gas specific heat ratio [unitless]
M = properties(idx_gas).M; % molar mass [kg kmol^-1]
omega = properties(idx_gas).omega; % acentric factor [unitless]
p_c = properties(idx_gas).p_c; % critical pressure [Pa]
T_c = properties(idx_gas).T_c; % critical temperature [Pa]
R = 8314.4621; % Universal gas constant [J kmol^-1 K^-1]
type_eos = "PR"; % {IG, RK, SRK, PR}

% EOS and EOS parameters based on Abbott's generic cubic equation (1979)
switch type_eos
    case "IG"
        a = 0;
        b = 0;
        delta = 0;
        epsilon = 0;
    case "RK"
        a = 0.4278*R^2*T_c^(2.5)/p_c;
        b = 0.0867*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "SRK"
        a = 0.42747*R^2*T_c^2/p_c;
        b = 0.08664*R*T_c/p_c;
        delta = b;
        epsilon = 0;
    case "PR"
        a = 0.45724*R^2*T_c^2/p_c;
        b = 0.07780*R*T_c/p_c;
        delta = 2*b;
        epsilon = -b^2;
end

% Instantiate pressure, density (and molar volume), temperature, flow speed, and simulation (x,t) parameters
% --------------------------------------------------------------------------------------------------

x_min = -1;
x_max = 1;
n_cells = 2048; % 128
dx = (x_max - x_min)/n_cells;
x = x_min + dx/2 : dx : x_max - dx/2; % vector of cell centres
t_final = 0.0009;

test_name = "trans_rcs";
p_L = 1.1e7;    % 2e5  1.1e7
rho_L = 180;    % 7.4  180
v_L = M./rho_L;
T_L = getTfromPV(p_L, v_L);
h_L = getHfromVT(v_L, T_L);
c_L = getSOS(v_L, T_L);
u_L = 150;      % -50 150

p_R = 2e5; 
rho_R = 7.4;
v_R = M./rho_R;
T_R = getTfromPV(p_R, v_R);
h_R = getHfromVT(v_R, T_R);
c_R = getSOS(v_R, T_R);
u_R = 50;

iterations_fsolve_max = 100;
tol = 1e-6;
URV = 1; % specific volume underrelaxation factor

% Instantiate visualization parameters
% --------------------------------------------------------------------------------------------------
line_width = 1;
marker_size = 2;
line_colour = [0.75 0.75 0.75];
line_spec = '-';
fsolve_display_flag = 'iter'; % {'iter', 'off'}
xlim_min = -inf;
xlim_max = inf;

%% Define symbolic expressions
fprintf("Defining symbolic expressions...\n");

syms p_sym v_sym T_sym
% EOS and EOS parameters based on Abbott's generic cubic equation (1979)
switch type_eos
    case "IG"
        Theta_sym = 0;
    case "RK"
        Theta_sym = a*(1./T_sym).^0.5;
    case "SRK"
        Theta_sym = a*(1 + (0.48 + 1.574*omega - 0.176*omega^2)*(1 - (T_sym./T_c).^0.5)).^2;
    case "PR"
        Theta_sym = a*(1 + (0.37464 + 1.54226*omega - 0.2699*omega^2)*(1 - (T_sym./T_c).^0.5)).^2;
end
p_sym = R*T_sym/(v_sym-b)-Theta_sym/(v_sym^2+delta*v_sym+epsilon);

% Derivatives
dThetadT = diff(Theta_sym, T_sym);
d2ThetadT2 = diff(Theta_sym, T_sym, 2);
exp_dpdT = diff(p_sym, T_sym);
exp_d2pdT2 = diff(p_sym, T_sym, 2);
exp_dpdv = diff(p_sym, v_sym);

% Integrals
exp_intdpdT = real(int(exp_dpdT, v_sym)); % [J kmol^-1 K^-1]
exp_intvdpdv = real(int(v_sym*exp_dpdv, v_sym)); % [J kmol^-1]
exp_intd2pdT2 = real(int(exp_d2pdT2, v_sym)); % [J kmol^-1 K^-2]

% Specific heat capacities and enthalpies
T_mat = T_sym; % reset the length of T_mat (this default value is overridden)
for i = 1:length(A) 
    T_mat(i,1) = T_sym^(i-1); 
end

% Specific heat capacities and enthalpy
c_p_IG_TP = R*(A*T_mat); % verified
c_v_IG_TP = c_p_IG_TP - R; % verified
c_v = c_v_IG_TP + T_sym*exp_intd2pdT2; % verified
c_p = c_v - T_sym*exp_dpdT^2/exp_dpdv; % verified
delta_h_IG_TP = real((Theta_sym-T_sym*dThetadT)*log((sqrt(delta^2-4*epsilon)+2*v_sym+delta)/(sqrt(delta^2-4*epsilon)-2*v_sym-delta))/(sqrt(delta^2-4*epsilon)) + R*T_sym - p_sym*v_sym);

if type_eos == "IG" % if ideal gas eos selected, delta_h_IG evaluates to a NaN when it should be zero.
    delta_h_IG_TP = 0;
end

h_IG_TP = int(c_p_IG_TP, T_sym);
h_sym = h_IG_TP - delta_h_IG_TP;

% Speed of sound
c_sym = sqrt(-v_sym^2*c_p*exp_dpdv/(M*c_v));

%% Exact solver
fprintf("Starting exact solver...\n");
t_sim = 0;

i = 1;
u_star_L = u_L + 2*tol;
u_star_R = u_R;
p_star = (c_R*rho_R*p_L + c_L*rho_L*p_R + c_R*rho_R*c_L*rho_L*(u_L - u_R)) / (c_L*rho_L + c_R*rho_R); % Toro Eq 9.28
if type_eos ~= "IG"
    v_star_L = M/(rho_L + (p_star(i) - p_L)/c_L^2);
    v_star_R = M/(rho_R + (p_star(i) - p_R)/c_R^2);
end

while abs(u_star_L(end) - u_star_R(end)) > tol
    % Update the pressure
    if i == 2 % if second iteration, use mean Lagrangian wavespeed 
        if u_star_L(i-1) == u_L 
            C_L_bar = rho_L*c_L;
        else
            C_L_bar = abs(p_star(i-1) - p_L)/abs(u_star_L(i-1) - u_L);
        end
        if u_star_R(i-1) == u_R
            C_R_bar = rho_R*c_R;
        else
            C_R_bar = abs(p_star(i-1) - p_R)/abs(u_star_R(i-1) - u_R);
        end
        p_star(i) = (C_R_bar*p_L + C_L_bar*p_R + C_R_bar*C_L_bar*(u_L - u_R)) / (C_L_bar + C_R_bar);
        if type_eos ~= "IG"
            v_star_L(i) = M/(rho_L + (p_star(i) - p_L)/c_L^2);
            v_star_R(i) = M/(rho_R + (p_star(i) - p_R)/c_R^2);
        end
    elseif i >= 3 % use secant method
        p_star(i) = p_star(i-1) - (u_star_L(i-1) - u_star_R(i-1))*(p_star(i-1) - p_star(i-2))/...
            (u_star_L(i-1) - u_star_R(i-1) - u_star_L(i-2) + u_star_R(i-2));
        if type_eos ~= "IG"
            v_star_L(i) = v_star_L(i-1) - URV*(u_star_L(i-1) - u_star_R(i-1))*(v_star_L(i-1) - v_star_L(i-2))/...
                (u_star_L(i-1) - u_star_R(i-1) - u_star_L(i-2) + u_star_R(i-2));
            v_star_R(i) = v_star_R(i-1) - URV*(u_star_L(i-1) - u_star_R(i-1))*(v_star_R(i-1) - v_star_R(i-2))/...
                (u_star_L(i-1) - u_star_R(i-1) - u_star_L(i-2) + u_star_R(i-2));
        end
    end

    % Check the left wave
    if p_star(i) < p_L % left rarefaction
        if type_eos == "IG" 
            v_star_L(i) = v_L*(p_L/p_star(i))^(1/gamma);
            T_star_L = T_L*(p_star(i)/p_L)^((gamma-1)/gamma);
            c_star_L = sqrt(gamma*R*T_star_L/M);
            u_star_L(i) = 2/(gamma - 1)*(c_L - c_star_L) + u_L;
        else
            % OPTION 1:            
            syms T_sym % u_sym
            eqn_v_star = v_sym - v_L*(p_L/p_star(i))^(1/(c_p/c_v));
            eqn_p_star = p_star(i) - p_sym;
%             eqn_J_plus_L = p_star(i) + (M/v_star_L(i))*subs(c_sym, v_sym, v_star_L(i))*u_sym - p_L - (M/v_L)*c_L*u_L;
%             eqn_J_plus_L = u_sym + 2*subs(c_sym, v_sym, v_star_L(i))/(subs(c_p/c_v, v_sym, v_star_L(i)) - 1) - u_L - 2*c_L/(subs(c_p/c_v, [v_sym, T_sym], [v_L, T_L]) - 1);
%             eqn_J_plus_L = u_sym - u_L + 2*(subs(c_sym, v_sym, v_star_L(i)) - c_L)/(gamma - 1);
%             eqn_J_plus_L = u_sym - subs(c_sym, v_sym, v_star_L(i))*log(v_star_L(i)) - u_L + c_L*log(v_L);
            mat_ns = [eqn_v_star; eqn_p_star];
            
            f = matlabFunction(mat_ns); f_str = func2str(f);
            f_str = strrep(f_str, '@(T_sym,v_sym)', '@(x)'); 
            f_str = strrep(f_str, 'T_sym', 'x(1)');
            f_str = strrep(f_str, 'v_sym', 'x(2)');
            f = str2func(f_str);
            init = [0.5*(T_R + T_L); 0.5*(v_R + v_L)];
            S = real(fsolve(f, init, optimoptions('fsolve', 'Display', fsolve_display_flag, 'MaxFunctionEvaluations', iterations_fsolve_max)));
            T_star_L = S(1);
            v_star_L(i) = S(2);
          
%             % OPTION 2:
%             v_star_L(i) = v_L*(p_L/p_star(i))^(1/gamma);
%             T_star_L = getTfromPV(p_star(i), v_star_L(i));

            c_star_L = double(subs(c_sym, [v_sym, T_sym], [v_star_L(i), T_star_L]));
            u_star_L(i) = 2/(double(subs(c_p/c_v, [v_sym T_sym], [v_star_L(i) T_star_L])) - 1)*(c_L - c_star_L) + u_L;
        end
    else % left shock
        % Solve R-H equation and EOS for temperature and molar volume
        syms v_sym T_sym
        eqn_energy = h_L/M - h_sym/M - 0.5*(1/(rho_L*M/v_sym))*(p_L - p_star(i))*(M/v_sym + rho_L);
        eqn_eos = p_star(i) - p_sym;
        mat_ns = [eqn_energy; eqn_eos];
        f = matlabFunction(mat_ns); f_str = func2str(f);
        f_str = strrep(f_str, '@(T_sym,v_sym)', '@(x)'); 
        f_str = strrep(f_str, 'T_sym', 'x(1)'); 
        f_str = strrep(f_str, 'v_sym', 'x(2)');
        f = str2func(f_str);
        init = [0.5*(T_L + T_R); 0.5*(v_L + v_R)];
        S = real(fsolve(f, init, optimoptions('fsolve', 'Display', fsolve_display_flag, 'MaxFunctionEvaluations', iterations_fsolve_max)));
        T_star_L = S(1);
        v_star_L(i) = S(2); 

        % Solve for SOS, shock speed, and flow speed
        c_star_L = double(subs(c_sym, [v_sym, T_sym], [v_star_L(i), T_star_L]));
        W_L = u_L - sqrt(((M/v_star_L(i))/rho_L)*(p_star(i) - p_L)/(M/v_star_L(i) - rho_L));
        u_star_L(i) = W_L + sqrt((rho_L*v_star_L(i)/M)*(p_star(i) - p_L)/(M/v_star_L(i) - rho_L));
    end

    % Check the right wave
    if p_star(i) < p_R % right rarefaction
        if type_eos == "IG"
            v_star_R(i) = v_R*(p_R/p_star(i))^(1/gamma);
            T_star_R = T_R*(p_star(i)/p_R)^((gamma-1)/gamma);
            c_star_R = sqrt(gamma*R*T_star_R/M);
            u_star_R(i) = 2/(gamma - 1)*(c_star_R - c_R) + u_R;
        else
            syms u_sym T_sym
            v_star_R(i) = v_R*(p_R/p_star(i))^(1/gamma);
            eqn_p_star = p_star(i) - subs(p_sym, v_sym, v_star_R(i));
            eqn_J_minus_R = u_sym + subs(c_sym, v_sym, v_star_R(i))*log(v_star_R(i)) - u_R - c_R*log(v_R);
            mat_ns = [eqn_p_star; eqn_J_minus_R];
            
            f = matlabFunction(mat_ns); f_str = func2str(f);
            f_str = strrep(f_str, '@(T_sym,u_sym)', '@(x)'); 
            f_str = strrep(f_str, 'T_sym', 'x(1)');
            f_str = strrep(f_str, 'u_sym', 'x(2)');
            f = str2func(f_str);
            init = [0.5*(T_R + T_L); 1];
            S = real(fsolve(f, init, optimoptions('fsolve', 'Display', fsolve_display_flag, 'MaxFunctionEvaluations', iterations_fsolve_max)));
            T_star_R = S(1);
            u_star_R(i) = S(2);
            
            c_star_R = double(subs(c_sym, [v_sym, T_sym], [v_star_R(i), T_star_R]));
        end
    else % right shock
        % Solve R-H equation and EOS for temperature and molar volume
        syms v_sym T_sym
        eqn_energy = h_sym/M - h_R/M - 0.5*(1/(rho_R*M/v_sym))*(p_star(i) - p_R)*(M/v_sym + rho_R);
        eqn_eos = p_star(i) - p_sym;
        mat_ns = [eqn_energy; eqn_eos];
        f = matlabFunction(mat_ns); f_str = func2str(f);
        f_str = strrep(f_str, '@(T_sym,v_sym)', '@(x)'); 
        f_str = strrep(f_str, 'T_sym', 'x(1)'); 
        f_str = strrep(f_str, 'v_sym', 'x(2)');
        f = str2func(f_str);

        init = [0.5*(T_L + T_R); 0.5*(v_L + v_R)];
        S = real(fsolve(f, init, optimoptions('fsolve', 'Display', fsolve_display_flag, 'MaxFunctionEvaluations', iterations_fsolve_max)));
        T_star_R = S(1);
        v_star_R(i) = S(2); 

        % Solve for SOS, shock speed, and flow speed
        c_star_R = double(subs(c_sym, [v_sym, T_sym], [v_star_R(i), T_star_R]));
        W_R = u_R + sqrt(((M/v_star_R(i))/rho_R)*(p_star(i) - p_R)/(M/v_star_R(i) - rho_R));
        u_star_R(i) = W_R - sqrt((rho_R*v_star_R(i)/M)*(p_star(i) - p_R)/(M/v_star_R(i) - rho_R));
    end

    fprintf("%s\ti = %d\tp_star = %f\tu_star_L = %f\tu_star_R = %f...\n", type_eos, i, p_star(i), u_star_L(i), u_star_R(i));
    i = i + 1;
end

% Save final results
u_star_final = 0.5*(u_star_L(end) + u_star_R(end));
p_star_final = p_star(end);
v_star_L_final = v_star_L(end);
v_star_R_final = v_star_R(end);
T_star_L_final = T_star_L(end);
T_star_R_final = T_star_R(end);

%% Sample the results
fprintf("Sampling results...\n");

if p_star_final < p_L && p_star_final > p_R % RCS solution
    if type_eos == "IG"
        for j = 1:length(x) 
            if x(j) < (u_L - c_L)*t_final % Left constant region
                u(j) = u_L;
                p(j) = p_L;
                rho(j) = M/v_L;
                T(j) = T_L;
            elseif x(j) >= (u_L - c_L)*t_final && x(j) < (u_star_final - c_star_L)*t_final % Left expansion
                u(j) = (2/(gamma + 1)) * (c_L + x(j)/t_final) + (gamma - 1)/(gamma + 1)*u_L;
                p(j) = p_L * (1 + 0.5*(gamma - 1)*(u_L - u(j))/c_L)^(2*gamma/(gamma-1));
                rho(j) = (M/v_L) * (1 + 0.5*(gamma - 1)*(u_L - u(j))/c_L)^(2/(gamma-1));
                T(j) = T_L * (1 + 0.5*(gamma - 1)*(u_L - u(j))/c_L)^2;
            elseif x(j) >= (u_star_final - c_star_L)*t_final && x(j) < u_star_final*t_final % Left star state
                u(j) = u_star_final;
                p(j) = p_star_final;
                rho(j) = M/v_star_L_final;
                T(j) = T_star_L_final;
            elseif x(j) >= u_star_final*t_final && x(j) < W_R*t_final % Right star state
                u(j) = u_star_final;
                p(j) = p_star_final;
                rho(j) = M/v_star_R_final;
                T(j) = T_star_R_final;
            elseif x(j) >= W_R*t_final % Right constant region
                u(j) = u_R;
                p(j) = p_R;
                rho(j) = M/v_R;
                T(j) = T_R;
            end
            sos(j) = getSOS(M/rho(j), T(j));
            e(j) = getEfromVT(M/rho(j), T(j))/M;
        end
    else
        A_1L = log(v_L/v_star_L_final) / (u_L - c_L - u_star_final + c_star_L);
        A_2L = -0.5*log(v_L*v_star_L_final) + 0.5*log(v_L/v_star_L_final)*(u_L - c_L + u_star_final - c_star_L)/(u_L - c_L - u_star_final + c_star_L);
        A_3L = -log(p_L/p_star_final) / (u_L - c_L - u_star_final + c_star_L);
        A_4L = -0.5*log(p_L*p_star_final) + 0.5*log(p_L/p_star_final)*(u_L - c_L + u_star_final - c_star_L)/(u_L - c_L - u_star_final + c_star_L);
        A_5L = 0.5*(u_L + u_star_final) - 0.5*(u_L - u_star_final)*(u_L - c_L + u_star_final - c_star_L)/(u_L - c_L - u_star_final + c_star_L);
        A_6L = (u_L - u_star_final)/(u_L - c_L - u_star_final + c_star_L);

        for j = 1:length(x) 
            if x(j) < (u_L - c_L)*t_final % Left constant region
                u(j) = u_L;
                p(j) = p_L;
                rho(j) = M/v_L;
                T(j) = T_L;
            elseif x(j) >= (u_L - c_L)*t_final && x(j) < (u_star_final - c_star_L)*t_final % Left expansion
                u(j) = A_5L + A_6L*x(j)/t_final;
                p(j) = exp(-A_3L*x(j)/t_final - A_4L);
                rho(j) = M/exp(A_1L*x(j)/t_final - A_2L);
                f = matlabFunction(p(j) - subs(p_sym, v_sym, M/rho(j)));
                T(j) = fsolve(f, (T_star_L + T_L)/2, optimoptions('fsolve', 'Display', 'off', 'MaxFunctionEvaluations', iterations_fsolve_max));
            elseif x(j) >= (u_star_final - c_star_L)*t_final && x(j) < u_star_final*t_final % Left star state
                u(j) = u_star_final;
                p(j) = p_star_final;
                rho(j) = M/v_star_L_final;
                T(j) = T_star_L_final;
            elseif x(j) >= u_star_final*t_final && x(j) < W_R*t_final % Right star state
                u(j) = u_star_final;
                p(j) = p_star_final;
                rho(j) = M/v_star_R_final;
                T(j) = T_star_R_final;
            elseif x(j) >= W_R*t_final % Right constant region
                u(j) = u_R;
                p(j) = p_R;
                rho(j) = M/v_R;
                T(j) = T_R;
            end
            sos(j) = getSOS(M/rho(j), T(j));
            e(j) = getEfromVT(M/rho(j), T(j))/M;
        end
    end
elseif p_star_final > p_L && p_star_final < p_R % SCR solution
    if type_eos == "IG"
        for j = 1:length(x) 
            if x(j) < W_L*t_final % Left constant region
                u(j) = u_L;
                p(j) = p_L;
                rho(j) = M/v_L;
                T(j) = T_L;
            elseif x(j) >= W_L*t_final && x(j) < u_star_final*t_final % Left star state
                u(j) = u_star_final;
                p(j) = p_star_final;
                rho(j) = M/v_star_L_final;
                T(j) = T_star_L_final;
            elseif x(j) >= u_star_final*t_final && x(j) < (u_star_final + c_star_R)*t_final % Right star state
                u(j) = u_star_final;
                p(j) = p_star_final;
                rho(j) = M/v_star_R_final;
                T(j) = T_star_R_final;
            elseif x(j) >= (u_star_final + c_star_R)*t_final && x(j) < (u_R + c_R)*t_final % Right expansion
                u(j) = (2/(gamma + 1)) * (x(j)/t_final - c_R) + (gamma - 1)/(gamma + 1)*u_R;
                p(j) = p_R * (1 + 0.5*(gamma - 1)*(u(j) - u_R)/c_R)^(2*gamma/(gamma-1));
                rho(j) = (M/v_R) * (1 + 0.5*(gamma - 1)*(u(j) - u_R)/c_R)^(2/(gamma-1));
                T(j) = T_R * (1 + 0.5*(gamma - 1)*(u(j) - u_R)/c_R)^2;
            elseif x(j) >= (u_R + c_R)*t_final % Right constant region
                u(j) = u_R;
                p(j) = p_R;
                rho(j) = M/v_R;
                T(j) = T_R;
            end
            sos(j) = getSOS(M/rho(j), T(j));
            e(j) = getEfromVT(M/rho(j), T(j))/M;
        end
    else
        A_1R = log(v_R/v_star_R_final) / (u_R + c_R - u_star_final - c_star_R);
        A_2R = -0.5*log(v_R*v_star_R_final) + 0.5*log(v_R/v_star_R_final)*(u_R + c_R + u_star_final + c_star_R)/(u_R + c_R - u_star_final - c_star_R);
        A_3R = -log(p_R/p_star_final) / (u_R + c_R - u_star_final - c_star_R);
        A_4R = -0.5*log(p_R*p_star_final) + 0.5*log(p_R/p_star_final)*(u_R + c_R + u_star_final + c_star_R)/(u_R + c_R - u_star_final - c_star_R);
        A_5R = 0.5*(u_R + u_star_final) - 0.5*(u_R - u_star_final)*(u_R + c_R + u_star_final + c_star_R)/(u_R + c_R - u_star_final - c_star_R);
        A_6R = (u_R - u_star_final)/(u_R + c_R - u_star_final - c_star_R);

        for j = 1:length(x) 
            if x(j) < W_L*t_final % Left constant region
                u(j) = u_L;
                p(j) = p_L;
                rho(j) = M/v_L;
                T(j) = T_L;
            elseif x(j) >= W_L*t_final && x(j) < u_star_final*t_final % Left star state
                u(j) = u_star_final;
                p(j) = p_star_final;
                rho(j) = M/v_star_L_final;
                T(j) = T_star_L_final;
            elseif x(j) >= u_star_final*t_final && x(j) < (u_star_final + c_star_R)*t_final % Right star state
                u(j) = u_star_final;
                p(j) = p_star_final;
                rho(j) = M/v_star_R_final;
                T(j) = T_star_R_final;
            elseif x(j) >= (u_star_final + c_star_R)*t_final && x(j) < (u_R + c_R)*t_final % Right expansion
                u(j) = A_5R + A_6R*x(j)/t_final;
                p(j) = exp(-A_3R*x(j)/t_final - A_4R);
                rho(j) = M/exp(A_1R*x(j)/t_final - A_2R);
                f = matlabFunction(p(j) - subs(p_sym, v_sym, M/rho(j)));
                T(j) = fsolve(f, (T_star_R + T_R)/2, optimoptions('fsolve', 'Display', 'off', 'MaxFunctionEvaluations', iterations_fsolve_max));
            elseif x(j) >= (u_R + c_R)*t_final % Right constant region
                u(j) = u_R;
                p(j) = p_R;
                rho(j) = M/v_R;
                T(j) = T_R;
            end
            sos(j) = getSOS(M/rho(j), T(j));
            e(j) = getEfromVT(M/rho(j), T(j))/M;
        end
    end
else
    fprintf("ERROR: Unknown solution configuration! Try again.\n");
end

%% Plot results
fprintf("Plotting results...\n");
figure(1), set(gcf, 'Position', [0 0 1500 1200]);
           
subplot(2,3,1);
% plot(x, p_0/p_c, 'k', 'LineWidth', line_width); 
% hold on;
plot(x, p/p_c, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('p_r', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,2);
% plot(x, rho_0, 'k', 'LineWidth', line_width); 
% hold on;
plot(x, rho, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('\rho [kg/m^3]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,3);
% plot(x, T_0/T_c, 'k', 'LineWidth', line_width); 
% hold on;
plot(x, T/T_c, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('T_r', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,4);
% plot(x, u_0, 'k', 'LineWidth', line_width);
% hold on;
plot(x, u, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size);
hold off;
ylabel('u [m/s]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,5);
% plot(x, Ma_0, 'k', 'LineWidth', line_width);
% hold on;
plot(x, u./sos, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('Ma', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);

subplot(2,3,6);
% plot(x, e_0, 'k', 'LineWidth', line_width); 
% hold on;
plot(x, e, line_spec, 'Color', line_colour, 'LineWidth', line_width, 'MarkerSize', marker_size); 
hold off;
ylabel('e [J/kg]', 'FontSize', 15);
xlabel('x [m]', 'FontSize', 15);
xlim([xlim_min, xlim_max]);


%% Save results
fprintf("Saving results...\n");

out_file = sprintf("%s_%s_exact_t=%f_n=%d.xlsx", test_name, type_eos, t_final, n_cells);

writematrix("x", out_file, 'Range', 'A1');
writematrix(x.', out_file, 'Range', 'A2');

writematrix("p_r", out_file, 'Range', 'B1');
writematrix((p/p_c).', out_file, 'Range', 'B2');

writematrix("rho", out_file, 'Range', 'C1');
writematrix(rho.', out_file, 'Range', 'C2');

writematrix("T_r", out_file, 'Range', 'D1');
writematrix((T/T_c).', out_file, 'Range', 'D2');

writematrix("u", out_file, 'Range', 'E1');
writematrix(u.', out_file, 'Range', 'E2');

writematrix("Ma", out_file, 'Range', 'F1');
writematrix((u./sos).', out_file, 'Range', 'F2');

writematrix("e", out_file, 'Range', 'G1');
writematrix(e.', out_file, 'Range', 'G2');
